﻿using StatementsDB;
using StatementsDB.DataModel.Dbo;
using System;
using System.Collections.Generic;
using System.Runtime.Caching;

namespace AlphaBankStatements
{
    public static class ClientCache
    {
        public static List<Jobs> Jobs
        {
            get
            {
                return GetObjectFromCache("Jobs", () => { return new JobsDAL().GetAll(); });
            }
        }

        public static List<StmtCriteria> TargetLists
        {
            get
            {
                return GetObjectFromCache("TargetLists", () => { return new CriteriaDAL().GetAll(); });
            }
        }

        private static T GetObjectFromCache<T>(string cacheItemName, Func<T> objectSettingFunction, int? cacheTimeInMinutes = null)
        {
            ObjectCache cache = MemoryCache.Default;
            var cachedObject = (T)cache[cacheItemName];
            if (cachedObject == null)
            {
                var policy = new CacheItemPolicy();
                if (cacheTimeInMinutes.HasValue)
                {
                    policy.AbsoluteExpiration = DateTimeOffset.Now.AddMinutes(cacheTimeInMinutes.Value);
                }
                cachedObject = objectSettingFunction();
                cache.Set(cacheItemName, cachedObject, policy);
            }
            return cachedObject;
        }

        public static void InvalidateCache()
        {
            MemoryCache.Default.Dispose();
        }
    }
}
