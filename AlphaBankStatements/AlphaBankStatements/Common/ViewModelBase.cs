﻿using FirstFloor.ModernUI.Presentation;
using StatementsDB;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace AlphaBankStatements
{
    public abstract partial class ViewModelBase<TDBItem, TDALItem> : INotifyPropertyChanged
        where TDBItem : class
        where TDALItem : BaseDAL<TDBItem>
    {
        protected TDBItem _selectedItem;
        public virtual TDBItem SelectedItem
        {
            get
            {
                return _selectedItem;
            }
            set
            {
                _selectedItem = value;
                RaisePropertyChanged("SelectedItem");
                RaisePropertyChanged("SelectedItemID");
            }
        }

        private ObservableCollection<TDBItem> _items;
        public ObservableCollection<TDBItem> Items
        {
            get
            {
                if (_items == null)
                {
                    _items = new ObservableCollection<TDBItem>();
                }

                return _items;
            }

            set
            {
                _items = value;
                RaisePropertyChanged("Items");
                RaisePropertyChanged("ItemsCount");
            }
        }

        public int ItemsCount
        {
            get
            {
                return (_items != null) ? _items.Count : 0;
            }
        }

        public string SelectedItemID
        {
            get
            {
                var firstPropertyName = _selectedItem?.GetType().GetProperties().FirstOrDefault()?.Name ?? string.Empty;
                if (firstPropertyName.ToUpper().EndsWith("ID"))
                {
                    var id = 0;
                    int.TryParse(_selectedItem.GetType().GetProperty(firstPropertyName).GetValue(_selectedItem, null).ToString(), out id);
                    return id != 0 ? id.ToString() : string.Empty;
                }

                return string.Empty;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void RaisePropertyChanged(string propertyName)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public event ViewModelNotificationHandler OnNotify;
        public delegate void ViewModelNotificationHandler(object sender, ViewModelNotificationEventArgs e);

        protected ViewModelNotificationEventArgs RaiseViewModelNotification(string action, string parameter, object Entity)
        {
            var e = new ViewModelNotificationEventArgs
            {
                Action = action,
                Parameter = parameter,
                Entity = Entity
            };

            if (OnNotify != null)
            {
                try
                {
                    foreach (ViewModelNotificationHandler handler in OnNotify.GetInvocationList())
                    {
                        handler(this, e);
                        if (e.Cancel) break;
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }

            return e;
        }

        public virtual void Search()
        {
            Items = new ObservableCollection<TDBItem>(((TDALItem)Activator.CreateInstance(typeof(TDALItem))).GetAll());
        }

        protected virtual bool CanAddItem()
        {
            return true;
        }

        protected virtual bool CanEditItem()
        {
            return true;
        }

        protected virtual bool CanDeleteItem()
        {
            return true;
        }

        protected virtual bool CanExportCSV()
        {
            return true;
        }

        protected virtual bool CanFilterItems()
        {
            return true;
        }

        public virtual bool AddItem()
        {
            RaiseViewModelNotification("AddItemSuccess", null, SelectedItem);
            return true;
        }

        public virtual bool EditItem()
        {
            if (_selectedItem != null)
            {
                ((TDALItem)Activator.CreateInstance(typeof(TDALItem))).Update(SelectedItem);
                RaiseViewModelNotification("EditItemSuccess", null, SelectedItem);
                return true;
            }

            return false;
        }

        public virtual bool DeleteItem()
        {
            if (_selectedItem != null)
            {
                ((TDALItem)Activator.CreateInstance(typeof(TDALItem))).Delete(SelectedItem);
                Items.Remove(SelectedItem);
                RaiseViewModelNotification("DeleteItemSuccess", null, SelectedItem);
                return true;
            }
            return false;
        }

        public virtual bool ExportCSV()
        {

            var saveDialog = new SaveFileDialog();
            saveDialog.Filter = "CSV Files (*.csv) | *.csv";
            if (saveDialog.ShowDialog() == DialogResult.OK)
            {
                var csv = string.Empty;
                var itemslist = new List<string>();

                foreach (var item in Items)
                {
                    var type1 = item.GetType();
                    var properties = type1.GetProperties();
                    itemslist.Clear();
                    foreach (var property in properties)
                    {
                        var propertyName = property.Name;
                        itemslist.Add(property.GetValue(item, null).ToString());
                    }
                    csv = csv + String.Join(";", itemslist) + Environment.NewLine;

                }
                File.WriteAllText(saveDialog.FileName, csv.ToString());
            }
            RaiseViewModelNotification("ExportItemsSuccess", null, SelectedItem);
            return true;
        }

        public virtual bool FilterItems()
        {
            RaiseViewModelNotification("FilterItemsSuccess", null, SelectedItem);
            return true;
        }

        #region Relay Commands
        private RelayCommand _addItemCommand;
        public RelayCommand AddItemCommand
        {
            get
            {
                if (_addItemCommand == null)
                {
                    _addItemCommand = new RelayCommand(param => this.AddItem(), param => this.CanAddItem());
                }
                return _addItemCommand;
            }
        }

        private RelayCommand _editItemCommand;
        public RelayCommand EditItemCommand
        {
            get
            {
                if (_editItemCommand == null)
                {
                    _editItemCommand = new RelayCommand(param => this.EditItem(), param => this.CanEditItem());
                }
                return _editItemCommand;
            }
        }

        private RelayCommand _deleteItemCommand;
        public RelayCommand DeleteItemCommand
        {
            get
            {
                if (_deleteItemCommand == null)
                {
                    _deleteItemCommand = new RelayCommand(param => this.DeleteItem(), param => this.CanDeleteItem());
                }
                return _deleteItemCommand;
            }
        }

        private RelayCommand _exportCSVCommand;
        public RelayCommand ExportCSVCommand
        {
            get
            {
                if (_exportCSVCommand == null)
                {
                    _exportCSVCommand = new RelayCommand(param => this.ExportCSV(), param => this.CanExportCSV());
                }
                return _exportCSVCommand;
            }
        }

        private RelayCommand _filterItemsCommand;
        public RelayCommand FilterItemsCommand
        {
            get
            {
                if (_filterItemsCommand == null)
                {
                    _filterItemsCommand = new RelayCommand(param => this.FilterItems(), param => this.CanFilterItems());
                }
                return _filterItemsCommand;
            }
        }
        #endregion
    }
}
