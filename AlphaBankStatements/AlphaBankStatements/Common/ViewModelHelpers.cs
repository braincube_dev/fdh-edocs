﻿using System;
using System.ComponentModel;

namespace AlphaBankStatements
{
    public class ViewModelNotificationEventArgs : EventArgs
    {
        public ViewModelNotificationEventArgs()
        {
            this.Cancel = false;
        }
        public string Action { get; set; }
        public string Parameter { get; set; }
        public bool Cancel { get; set; }
        public object Entity { get; set; }

    }

    public class ComboItems : INotifyPropertyChanged
    {
        private int? _id;
        public int? ID
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
                RaisePropertyChanged("ID");
            }
        }

        private string _displayName;
        public string DisplayName
        {
            get
            {
                return _displayName;
            }
            set
            {
                _displayName = value;
                RaisePropertyChanged("DisplayName");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string propertyName)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
