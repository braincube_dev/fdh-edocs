﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace AlphaBankStatements.Pages.Images
{
    /// <summary>
    /// Interaction logic for CUDButtonsView.xaml
    /// </summary>
    /// 

    public partial class CUDButtonsView : UserControl
    {
        public static readonly DependencyProperty AddItemProperty =
        DependencyProperty.Register(
            "AddItemCommand",
            typeof(ICommand),
            typeof(CUDButtonsView),
            new UIPropertyMetadata(null));

        public static readonly DependencyProperty EditItemProperty =
        DependencyProperty.Register(
            "EditItemCommand",
            typeof(ICommand),
            typeof(CUDButtonsView),
            new UIPropertyMetadata(null));

        public static readonly DependencyProperty DeleteItemProperty =
        DependencyProperty.Register(
            "DeleteItemCommand",
            typeof(ICommand),
            typeof(CUDButtonsView),
            new UIPropertyMetadata(null));

        public ICommand AddItemCommand
        {
            get { return (ICommand)GetValue(AddItemProperty); }
            set { SetValue(AddItemProperty, value); }
        }

        public ICommand EditItemCommand
        {
            get { return (ICommand)GetValue(EditItemProperty); }
            set { SetValue(EditItemProperty, value); }
        }

        public ICommand DeleteItemCommand
        {
            get { return (ICommand)GetValue(DeleteItemProperty); }
            set { SetValue(DeleteItemProperty, value); }
        }

        public CUDButtonsView()
        {
            InitializeComponent();
        }
    }
}
