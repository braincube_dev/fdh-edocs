﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace AlphaBankStatements.Pages.Images
{
    /// <summary>
    /// Interaction logic for FilterExportButton.xaml
    /// </summary>
    /// 

    public partial class ExportButton : UserControl
    {
        public static readonly DependencyProperty ExportItemsProperty =
        DependencyProperty.Register(
            "ExportCSVCommand",
            typeof(ICommand),
            typeof(CUDButtonsView),
            new UIPropertyMetadata(null));

        public ICommand ExportCSVCommand
        {
            get { return (ICommand)GetValue(ExportItemsProperty); }
            set { SetValue(ExportItemsProperty, value); }
        }

        public ExportButton()
        {
            InitializeComponent();
        }
    }
}
