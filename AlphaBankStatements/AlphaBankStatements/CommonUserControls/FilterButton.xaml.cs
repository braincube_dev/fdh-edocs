﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace AlphaBankStatements.Pages.Images
{
    /// <summary>
    /// Interaction logic for FilterExportButton.xaml
    /// </summary>
    /// 

    public partial class FilterButtonView : UserControl
    {
        public static readonly DependencyProperty FilterItemsProperty =
        DependencyProperty.Register(
            "FilterItemsCommand",
            typeof(ICommand),
            typeof(CUDButtonsView),
            new UIPropertyMetadata(null));

        public ICommand FilterItemsCommand
        {
            get { return (ICommand)GetValue(FilterItemsProperty); }
            set { SetValue(FilterItemsProperty, value); }
        }

        public FilterButtonView()
        {
            InitializeComponent();
            cmbKeyCriteria.ItemsSource = ClientCache.Jobs.Select(a => new ComboItems { ID = a.JobID, DisplayName = a.JobName });
        }
    }
}
