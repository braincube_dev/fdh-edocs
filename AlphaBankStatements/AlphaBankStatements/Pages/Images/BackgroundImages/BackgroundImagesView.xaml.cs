﻿using System.Linq;
using System.Windows.Controls;

namespace AlphaBankStatements.Pages.Images
{
    /// <summary>
    /// Interaction logic for BackgroundImagesView.xaml
    /// </summary>
    public partial class BackgroundImagesView : UserControl
    {
        public BackgroundImagesViewModel ViewModel { get; set; }

        public BackgroundImagesView()
        {
            this.DataContext = ViewModel = new BackgroundImagesViewModel();
            InitializeComponent();
        }

        private void BackgroundImagesView_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            LoadCombos();
            FillDataGrid();
        }

        private void LoadCombos()
        {
            cmbJobName.ItemsSource = ClientCache.Jobs.Select(a => new ComboItems { ID = a.JobID, DisplayName = a.JobName });
        }

        private void FillDataGrid()
        {
            ViewModel.Search();
        }

        private void btnSetAll_SetProductToAll(object sender, System.Windows.RoutedEventArgs e)
        {
            txtProduct.Text = "XX";
        }
    }
}
