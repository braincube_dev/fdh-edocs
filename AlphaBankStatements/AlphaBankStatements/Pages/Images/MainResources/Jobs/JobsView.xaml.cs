﻿using StatementsDB.DataModel.Dbo;
using System.Windows.Controls;

namespace AlphaBankStatements.Pages.Images.MainResources
{
    /// <summary>
    /// Interaction logic for JobsView.xaml
    /// </summary>
    /// 
    public partial class JobsView : UserControl
    {
        public JobsViewModel ViewModel { get; set; }
        public JobsView()
        {
            this.DataContext = ViewModel = new JobsViewModel();
            InitializeComponent();
            FillDataGrid();
        }
        private void JobsView_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            
            FillDataGrid();
        }
        private void FillDataGrid()
        {
            ViewModel.Search();
        }
        private void DG_SelectItem(object sender, SelectionChangedEventArgs e)
        {
            ViewModel.SelectedItem = ((DataGrid)sender).SelectedItem as Jobs;
        }
    }
}
