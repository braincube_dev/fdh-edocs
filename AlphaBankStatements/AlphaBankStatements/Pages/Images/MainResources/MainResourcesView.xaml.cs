﻿using AlphaBankStatements.Pages.Images.MainResources;
using System.Windows.Controls;

namespace AlphaBankStatements.Pages.Images
{
    /// <summary>
    /// Interaction logic for MainResources.xaml
    /// </summary>
    public partial class MainResourcesView : UserControl
    {
        public MainResourcesView()
        {
            InitializeComponent();
            var JobUserControl = new JobsView();
            JobContentControl.Content = JobUserControl;
            var MarketingSelectionUserControl = new MarketingSelectionView();
            MarketingSelectionContentControl.Content = MarketingSelectionUserControl;
        }
    }
}
