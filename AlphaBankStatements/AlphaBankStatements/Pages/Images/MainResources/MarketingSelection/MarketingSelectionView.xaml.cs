﻿using StatementsDB.DataModel.Dbo;
using System.Windows.Controls;

namespace AlphaBankStatements.Pages.Images.MainResources
{
    /// <summary>
    /// Interaction logic for MarketingSelectionView.xaml
    /// </summary>
    public partial class MarketingSelectionView : UserControl
    {
        public MarketingSelectionViewModel ViewModel { get; set; }

        public MarketingSelectionView()
        {
            this.DataContext = ViewModel = new MarketingSelectionViewModel();
            InitializeComponent();            
            FillDataGrid();
        }
        private void MarketingSelectionView_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
           
            FillDataGrid();
        }
        private void FillDataGrid()
        {
            ViewModel.Search();
        }
        private void DG_SelectItem(object sender, SelectionChangedEventArgs e)
        {
            ViewModel.SelectedItem = ((DataGrid)sender).SelectedItem as StmtCriteria;
        }
    }
}
