﻿using AlphaBankStatements.Pages.Images.MarketingConditions;
using System.Linq;
using System.Windows.Controls;

namespace AlphaBankStatements.Pages.Images
{
    /// <summary>
    /// Interaction logic for MarketingConditions.xaml
    /// </summary>
    public partial class MarketingConditionsView : UserControl
    {
        public MarketingConditionsViewModel ViewModel { get; set; }

        public MarketingConditionsView()
        {
            this.DataContext = ViewModel = new MarketingConditionsViewModel();
            InitializeComponent();
            FillDataGrid();
        }
        private void MarketingConditionsView_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            LoadCombos();
            FillDataGrid();
        }
        private void LoadCombos()
        {
            jobcmb.ItemsSource = ClientCache.Jobs.Select(a => new ComboItems { ID = a.JobID, DisplayName = a.JobName });
        }
        private void FillDataGrid()
        {
            ViewModel.Search();
        }
       

    }
}
