﻿using System.Linq;
using System.Windows.Controls;

namespace AlphaBankStatements.Pages.Images
{
    /// <summary>
    /// Interaction logic for MarketingImages.xaml
    /// </summary>
    public partial class MarketingImagesView : UserControl
    {
        public MarketingImagesViewModel ViewModel { get; set; }

        public MarketingImagesView()
        {
            this.DataContext = ViewModel = new MarketingImagesViewModel();

            InitializeComponent();
            FillDataGrid();

        }
        private void MarketingImagesView_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            LoadCombos();
            FillDataGrid();
        }
        private void LoadCombos()
        {
            cmbtype.ItemsSource = ("IM");
        }

        private void FillDataGrid()
        {
            ViewModel.Search();
        }
       
    }
}
