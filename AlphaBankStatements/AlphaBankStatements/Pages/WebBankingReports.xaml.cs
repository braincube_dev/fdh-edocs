﻿using System.Windows.Controls;

namespace AlphaBankStatements.Pages
{
    /// <summary>
    /// Interaction logic for WebBankingReports.xaml
    /// </summary>
    public partial class WebBankingReports : UserControl
    {
        public WebBankingReports()
        {
            InitializeComponent();
        }
    }
}
