﻿using EdocsCore;
using FileLoaders;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;

namespace EDocsConsoleApllication
{
    class Program
    {
        /// <summary>
        /// Main creates and calls the engine that handles the process.
        /// </summary>
        static void Main(string[] args)
        {
            var engineToRun = ConfigurationManager.AppSettings["Loader"];
            var fileLoaderFolder = ConfigurationManager.AppSettings["FileLoaderFolder"];

            if (string.IsNullOrWhiteSpace(engineToRun) == false
                && string.Equals(engineToRun, "FileLoaderEngine", StringComparison.OrdinalIgnoreCase)
                && string.IsNullOrWhiteSpace(fileLoaderFolder) == false
                && Directory.Exists(fileLoaderFolder))
            {
                foreach (var file in new DirectoryInfo(fileLoaderFolder).GetFiles())
                {
                    var fileRecords = new FileLoaderEngine().GetRecords(file.FullName);
                }
                Console.ReadKey();
            }
            else
            {
                var engine = new ProcessEngine();
                var handlersMessages = engine.Start(args);

                DisplayMessages(handlersMessages);
            }

        }

        /// <summary>
        /// DisplayMessages shows to the user the output messages after the end of the processes.
        /// </summary>
        static void DisplayMessages(List<string> handlersMessages)
        {
            var logCounter = 0;
            foreach (var message in handlersMessages)
            {
                Console.Write($"{(++logCounter).ToString().PadLeft(3, '0')}: {message}\n");
            }

            Console.ReadKey();
        }
    }
}
