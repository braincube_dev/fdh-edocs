﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EdocsCore.Business;
using EdocsCore.ViewModels;
using Helpers;

namespace EDocsUnitTests.BussinessSenariosUnitTests
{
    [TestClass]
    public class CstfExtractCommand
    {
        [TestMethod]
        public void TestMethod1()
        {
            var inputData = new CstfExtractDataViewModel
            {
                CstfInputFile = @"C:\Temp\FilesToParse\CSTP002ABGAF20180824.00107.CSTF",
                CstfOutputDirectory = @"C:\Temp\OutputFiles",
                SchemeToProcess = (int)SchemeToProcessType.Bonus,
            };

            var test = new CstfExtractBL().ExtractData(inputData);
        }
    }
}
