﻿using EdocsCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace EDocsUnitTests
{
    [TestClass]
    public class CheckConfigUnitTests
    {
        [TestMethod]
        public void CheckIfAnyErrorTest()
        {
            var errors = new Dictionary<string, string>();
            foreach (var processName in GetProcessesNames())
            {
                var args = new string[] { processName };
                var engine = new ProcessEngine();
                var errorMessages = engine.Start(args)
                                            .Where(a => a.ToUpper().StartsWith("DONE") == false)
                                                ?? new List<string> { string.Empty, };
                if (errorMessages.Any())
                {
                    errors.Add(processName, $"{processName}:{string.Join(", ", errorMessages)}");
                }
            }

            var outputMessages = string.Empty;
            foreach (var item in errors)
            {
                outputMessages += $"{item.Value}, ";
            }

            Assert.IsFalse(errors.Any(), outputMessages);
        }

        private List<string> GetProcessesNames()
        {
            var settings = (ProcessesSettings)ConfigurationManager.GetSection("Section");
            return settings?.Processes?.GetProcessesNames() ?? new List<string>();
        }
    }
}
