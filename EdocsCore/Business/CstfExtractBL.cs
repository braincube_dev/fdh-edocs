﻿using EdocsCore.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Helpers;
using FileLoaders;
using FileLoaders.Loyalty;
using StatementsDB;
using StatementsDB.DataModel.Dbo;

namespace EdocsCore.Business
{
    /// <summary>
    /// CSTF file = Loyalty File
    /// </summary>
    public class CstfExtractBL
    {
        private Dictionary<Type, Action<CstfExtractDataViewModel>> ParseRecordStrategy => new Dictionary<Type, Action<CstfExtractDataViewModel>>
        {
            { typeof(LoyaltyHeaderRecord), (CstfExtractDataViewModel request) => HeaderAction(request)},
        };

        private void HeaderAction(CstfExtractDataViewModel request)
        {
            //new LoyaltyDatesDAL().Insert(
            //            new LoyaltyDates
            //            {
            //                SchemeID = request.SchemeToProcess,
            //                ProcDate = request.ImplementationDate.Value,
            //                DateEpistoli = request.LetterDate.Value,
            //                DatePtsBegin = request.PointsDateFrom.Value,
            //            });


            //InsertToLoayltyHeader
        }

        public CstfExtractDataViewModel ExtractData(CstfExtractDataViewModel request)
        {
            var CardList = new List<string>();
            var PersonalAddon = new List<string>();

            InitializeNullDates(request);
            var RecCnt = 0;
            var CustRead = 0;
            var CorpRead = 0;
            var CorpEstimated = 0;
            var CorpKept = 0;
            var CurrCorpEstimated = 0;
            var iCorpEstimated = 0;
            var CustKept = 0;
            var ExpRead = 0;
            var ExpEstimated = 0;
            var ExpKept = 0;
            var CurrExpEstimated = 0;
            var iExpEstimated = 0;
            var TotalPerCardRead = 0;
            var TotalPerCardKept = 0;
            var BankAdjustmentsRead = 0;
            var BankAdjustmentsKept = 0;
            var iMainCards = 0;
            var iAddonCards = 0;
            var iBussMain = 0;
            var iBussAddon = 0;
            
            var schemeToProcess = request.SchemeToProcess;
            
            var ForceStatementCards = $"{request.CstfOutputDirectory}//ForceStatement_{DateTime.Now.Date.ToString("yyyyMMdd")}_{((SchemeToProcessType)schemeToProcess).ToString()}.txt";
            var PersAddOnFile = $"{request.CstfOutputDirectory}//PersonalAddons{DateTime.Now.Date.ToString("yyyyMMdd")}_{((SchemeToProcessType)schemeToProcess).ToString()}.txt";
            var fileRecords = new FileLoaderEngine().GetRecords(request.CstfInputFile);

            foreach (var record in fileRecords)
            {
                if (ParseRecordStrategy.ContainsKey(record.GetType()))
                {
                    ParseRecordStrategy[record.GetType()](request);
                }
            }

            return request;
        }



        private void InitializeNullDates(CstfExtractDataViewModel request)
        {
            if (request.LetterDate == null
                || request.PointsDateFrom == null
                || request.PointsDateTo == null
                || request.ExpiredPointsDate == null
                || request.ToBeExpiredPointsDate == null)
            {
                request.Info.Add($"WRT-CSTF processor: Dates not entered in respective fields will be set to {DateTime.Now.ToString("dd/MM/yyyy")}");
            }

            request.LetterDate = request.LetterDate ?? DateTime.Now.Date;
            request.PointsDateFrom = request.PointsDateFrom ?? DateTime.Now.Date;
            request.PointsDateTo = request.PointsDateTo ?? DateTime.Now.Date;
            request.ExpiredPointsDate = request.ExpiredPointsDate ?? DateTime.Now.Date;
            request.ToBeExpiredPointsDate = request.ToBeExpiredPointsDate ?? DateTime.Now.Date;
            request.ImplementationDate = DateTime.Now.Date;
        }
    }
}
