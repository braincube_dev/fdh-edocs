﻿using Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text.RegularExpressions;

namespace EdocsCore
{
    public abstract class Handler
    {
        protected StepSetting stepSetting;
        protected Handler successor;

        public Regex bracketsRegex => new Regex(
                    @"\[              # Match an opening bracket.
                      (?>             # Then either match (possessively):
                       [^\[\]]+         #  any characters except brackets
                      |               # or
                       \[ (?<Depth>)  #  an opening paren (and increase the bracket counter)
                      |               # or
                       \] (?<-Depth>) #  a closing paren (and decrease the bracket counter).
                      )*              # Repeat as needed.
                     (?(Depth)(?!))   # Assert that the bracket counter is at zero.
                     \]               # Then match a closing bracket.",
                    RegexOptions.IgnorePatternWhitespace);

        protected List<HandlerLogItem> handlerLog;
        public List<HandlerLogItem> HandlerLog
        {
            get
            {
                if (handlerLog == null)
                {
                    handlerLog = new List<HandlerLogItem>();
                }

                return handlerLog;
            }

            set
            {
                handlerLog = value;
            }
        }

        protected FileStream WorkingFile { get; set; }

        public void SetSuccessor(Handler successor)
        {
            this.successor = successor;
        }

        public void SetStepSetting(StepSetting stepSetting)
        {
            this.stepSetting = stepSetting;
        }

        protected void SetFileToSuccessor(FileStream fileStream)
        {
            if (successor != null)
            {
                successor.WorkingFile = WorkingFile;
            }
        }

        protected void SetLogListToSuccessor()
        {
            if (successor != null)
            {
                successor.HandlerLog = HandlerLog;
            }
        }

        protected void LogError(Exception ex)
        {
            HandlerLog.Add(
                new HandlerError
                {
                    Message = ex.Message,
                    StepID = stepSetting.ID,
                    StepName = stepSetting.Name
                });
        }

        protected void LogInfo(string message)
        {
            HandlerLog.Add(
                new HandlerInfo
                {
                    Message = message,
                    StepID = stepSetting.ID,
                    StepName = stepSetting.Name
                });
        }

        protected void ContinueOnErrorBySettingChoice(Exception ex, bool forceStop = false)
        {
            if (stepSetting.ContinueOnError == false || forceStop)
            {
                throw ex;
            }
            else
            {
                LogError(ex);
            }
        }

        protected void SetDataToSuccessorAndHandleRequest(dynamic request)
        {
            SetFileToSuccessor(WorkingFile);
            SetLogListToSuccessor();
            successor?.HandleRequest(request);
        }

        protected void CreateFolder(string path)
        {
            if (string.IsNullOrWhiteSpace(path) == false
                && Directory.Exists(path) == false)
            {
                Directory.CreateDirectory(path);
            }
        }
        /// <summary>
        /// ApplyCopyFileStrategy selects the files to copy to the input folder,based on the strategy(prefix/suffix).
        /// </summary>
        protected virtual void ApplyCopyFileStrategy(Dictionary<string, dynamic> strategy, FileInfo[] filesToApply, CommonProperties common)
        {
            var commands = new Dictionary<string, Action<string, CommonProperties>>
            {
                { "RAISE", (instruction, properties) => { properties.IncreaseCounter(instruction); } },
            };

            foreach (var data in strategy)
            {
                var instructions = ((List<string>)data.Value.Instructions);

                var filesToTransfer = data.Key.Contains("[ENDING]") == false ?
                    filesToApply.Where(a => (bool)data.Value.UseSuffix ? a.Name.EndsWith(data.Key) : a.Name.StartsWith(data.Key)) :
                    filesToApply.Where(a => ValidateFilePrefixAndSuffix(data.Key, a.Name));

                foreach (var file in filesToTransfer)
                {
                    if (string.IsNullOrWhiteSpace(data.Value.Directory) == false)
                    {
                        file.CopyTo(((string)data.Value.Directory) + "\\" + file.Name);
                    }
                    instructions.ForEach(instruction =>
                    {
                        var command = instruction.Split('_').First().Replace("[", "").Replace("]", "");
                        if (commands.ContainsKey(command))
                        {
                            commands[command](instruction, common);
                        }
                    });
                }
            }
        }

        protected bool ValidateFilePrefixAndSuffix(string validator, string fileName)
        {
            var attributes = validator.Split(new string[] { "[ENDING]" }, StringSplitOptions.None);
            return attributes.Count() == 2 && fileName.StartsWith(attributes.First()) && fileName.EndsWith(attributes.Last());
        }
        /// <summary>
        /// ApplyCopyFileStrategy checks the type of the strategy (prefix/suffix).
        /// </summary>
        protected virtual Dictionary<string, dynamic> GetFileStrategy(string rawData)
        {
            var output = new Dictionary<string, dynamic>();

            var rows = rawData.Split(';');
            foreach (var keyValue in rows)
            {
                var data = keyValue;
                var instructions = new List<string>();
                foreach (Match match in bracketsRegex.Matches(data))
                {
                    var instr = match.ToString().ToUpper();
                    if (instr.Contains("[ENDING]") == false)
                    {
                        instructions.Add(instr);
                        data = data.Replace(match.ToString(), "");
                    }
                }
                var dictionaryValue =
                    new
                    {
                        Directory = data.Split('|').LastOrDefault() ?? string.Empty,
                        Instructions = instructions,
                        UseSuffix = instructions.Contains("[SUFFIX]"),
                    };

                if (output.ContainsKey(data.Split('|').First()) == false)
                {
                    output.Add(data.Split('|').First(), dictionaryValue);
                }
            }

            return output;
        }
        /// <summary>
        /// ApplyCopyFileStrategy creates a zip file and setups the name of the file and the path.
        /// </summary>
        protected void CreateZip(dynamic request)
        {
            var commonProperties = (CommonProperties)request.CommonProperties;
            var zipFullName = commonProperties.MapAttributesToValues(stepSetting.FileTarget);
            var zipName = zipFullName.Split('\\').Last();
            var zipPath = zipFullName.Replace(zipName, "");
            if (Directory.Exists(zipPath) == false)
            {
                Directory.CreateDirectory(zipPath);
            }
            using (var newFile = ZipFile.Open(zipFullName, (ZipArchiveMode.Create)))
                foreach (var inputDir in stepSetting.FoldersSource.Split(';'))
            {
                var searchPattern = inputDir.Split('\\').Last();
                var path = (string.IsNullOrWhiteSpace(searchPattern) == false) ? inputDir.Replace(searchPattern, "") : string.Empty;

                if (string.IsNullOrWhiteSpace(inputDir) || Directory.Exists(path) == false)
                {
                    continue;
                }

                var selectedFiles = Directory.GetFiles(path, searchPattern);
                
                
                    foreach (var fullFileName in selectedFiles)
                    {
                        newFile.CreateEntryFromFile(fullFileName, fullFileName.Split('\\').Last(), CompressionLevel.Optimal);
                    }
                

                if (stepSetting.AttachToEmail)
                {
                    if (commonProperties.AttachmentFiles == null)
                    {
                        commonProperties.AttachmentFiles = new Dictionary<string, string> { { zipFullName, zipFullName } };
                    }
                    else if (commonProperties.AttachmentFiles.ContainsKey(zipFullName) == false)
                    {
                        commonProperties.AttachmentFiles.Add(zipFullName, zipFullName);
                    }
                }
            }
        }

        protected void LoadToDb(string dbFolder)
        {
            throw new NotImplementedException();
        }

        public abstract void HandleRequest(dynamic request);
    }
}
