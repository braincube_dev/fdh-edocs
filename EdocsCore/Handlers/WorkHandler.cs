﻿using Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text.RegularExpressions;

namespace EdocsCore
{

    /// <summary>
    /// CheckLock opens the file if it is not locked by another process.
    /// </summary>
    class CheckLock : Handler
    {
        public override void HandleRequest(dynamic request)
        {
            try
            {
                var fileName = string.IsNullOrWhiteSpace(request.FileName) ? stepSetting.FileSource : request.FileName;
                WorkingFile = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.None);
                LogInfo($"Done: Locked file: {fileName}");
            }
            catch (Exception ex)
            {
                ContinueOnErrorBySettingChoice(ex);
                WorkingFile?.Close();
            }

            SetDataToSuccessorAndHandleRequest(request);
        }
    }

    /// <summary>
    /// MoveFiles copies the files of the input folder to another output/destination folder,if they exist.
    /// Then deletes the files from the input folder.
    /// </summary>
    class MoveFiles : Handler
    {
        public override void HandleRequest(dynamic request)
        {
            try
            {
                var commonProperties = ((CommonProperties)request.CommonProperties);
                var isEnabledDbMove = (string.IsNullOrWhiteSpace(stepSetting.SetDbFile) == false && Directory.Exists(stepSetting.SetDbFile) == false);

                var inputFiles = new List<FileInfo>();
                foreach (var inputDir in stepSetting.FoldersSource.Split(';'))
                {
                    if (string.IsNullOrWhiteSpace(inputDir) || Directory.Exists(inputDir) == false)
                    {
                        continue;
                    }

                    foreach (var file in new DirectoryInfo(inputDir).GetFiles())
                    {
                        inputFiles.Add(file);
                    }
                }

                if (inputFiles.Any() == false)
                {
                    WorkingFile?.Close();
                    throw new Exception($"Done: [Stopped] There are No Files in {stepSetting.FoldersSource}");
                }

                foreach (var outputFolder in stepSetting.FoldersTarget.Split(';'))
                {
                    if (string.IsNullOrWhiteSpace(outputFolder) || Directory.Exists(outputFolder) == false)
                    {
                        continue;
                    }

                    foreach (var file in inputFiles)
                    {
                        commonProperties.DBFile = stepSetting.SetDbFile;
                        file.CopyTo(outputFolder + "\\" + (stepSetting.AddTimeStamp ? DateTime.UtcNow.ToString("yyyyMMdd_HHmmss") : string.Empty) + file.Name);
                        if (isEnabledDbMove)
                        {
                            LoadToDb(stepSetting.SetDbFile);
                        }
                    }
                }

                var fileCounter = inputFiles.Count;
                inputFiles.ForEach(a => { a.Delete(); });

                LogInfo($"Done: {commonProperties.MapAttributesToValues(stepSetting.MessageToDisplay, fileCounter)}");
            }
            catch (Exception ex)
            {
                ContinueOnErrorBySettingChoice(ex, true);
            }

            SetDataToSuccessorAndHandleRequest(request);
        }
    }

    /// <summary>
    /// ZipAndMoveFiles zips into a .zip file,the contents of the input folder,and copies it to the output/destination folder.
    /// </summary>

    class ZipAndMoveFiles : Handler
    {
        public override void HandleRequest(dynamic request)
        {
            try
            {
                var backupDir = stepSetting.FoldersToCreate.Split(';').First() + "\\" + DateTime.UtcNow.Date.ToString("ddMMyy");
                if (Directory.Exists(backupDir) == false)
                {
                    Directory.CreateDirectory(backupDir);
                }

                var targetDir = stepSetting.FoldersTarget.Split(';').First();
                foreach (var file in new DirectoryInfo(targetDir).GetFiles())
                {
                    var dateModified = DateTime.UtcNow.ToString("ddMMyy_hhmm");
                    var zipFile = backupDir + "\\" + file.Name + "_" + dateModified + ".zip";
                    if (File.Exists(zipFile) == false)
                    {
                        using (var newFile = ZipFile.Open(zipFile, ZipArchiveMode.Update))
                        {
                            newFile.CreateEntryFromFile(file.FullName, file.Name, CompressionLevel.Optimal);
                        }
                    }
                }

                LogInfo($"Done: ZipAndMoveFiles");
            }
            catch (Exception ex)
            {
                ContinueOnErrorBySettingChoice(ex);
            }

            SetDataToSuccessorAndHandleRequest(request);
        }
    }
    /// <summary>
    /// CopyFiles copies the files from the input folder to the output/destination folder.
    /// </summary>
    class CopyFiles : Handler
    {

        public override void HandleRequest(dynamic request)
        {
            try
            {
                var inputFiles = new List<FileInfo>();
                foreach (var inputDir in stepSetting.FoldersSource.Split(';'))
                {
                    if (string.IsNullOrWhiteSpace(inputDir) || Directory.Exists(inputDir) == false)
                    {
                        continue;
                    }

                    foreach (var file in new DirectoryInfo(inputDir).GetFiles())
                    {
                        inputFiles.Add(file);
                    }
                }

                foreach (var outputFolder in stepSetting.FoldersTarget.Split(';'))
                {
                    if (string.IsNullOrWhiteSpace(outputFolder) || Directory.Exists(outputFolder) == false)
                    {
                        continue;
                    }

                    foreach (var file in inputFiles)
                    {
                        file.CopyTo(outputFolder + "\\" + file.Name);
                    }
                }

                var commonProperties = ((CommonProperties)request.CommonProperties);
                LogInfo($"Done: {commonProperties.MapAttributesToValues(stepSetting.MessageToDisplay, inputFiles.Count)}");
            }
            catch (Exception ex)
            {
                ContinueOnErrorBySettingChoice(ex);
            }

            SetDataToSuccessorAndHandleRequest(request);
        }
    }
    /// <summary>
    /// CopyToTargetAndCount copies the files from each input folder ,if they match with the correct strategy
    /// (example correct prefix/suffix),to the output/destination folder.
    /// (Optional)Increases the selected counter(s).
    /// </summary>

    class CopyToTargetAndCount : Handler
    {
        public override void HandleRequest(dynamic request)
        {
            try
            {
                var commonProperties = (CommonProperties)request.CommonProperties;

                var folders = stepSetting.FoldersSource.Split(';');
                var strategies = stepSetting.Strategies.Split('#');

                if (string.IsNullOrWhiteSpace(stepSetting.FoldersToCreate) == false)
                {
                    stepSetting.FoldersToCreate.Split(';')
                           .ToList().ForEach(a => { CreateFolder(a); });
                }

                if (folders.Count() != strategies.Count())
                {
                    ThrowException.StrategiesNotEqualToFolders();
                }

                for (var i = 0; i < folders.Count(); i++)
                {
                    var selectedFiles = new DirectoryInfo(Regex.Replace(folders[i], @"\t|\n|\r", "").Trim()).GetFiles();
                    var selectedStrategy = GetFileStrategy(Regex.Replace(strategies[i], @"\t|\n|\r", "").Trim());

                    ApplyCopyFileStrategy(selectedStrategy, selectedFiles, commonProperties);
                }

                LogInfo($"Done: CopyToTargetAndCount: {commonProperties.MapAttributesToValues(stepSetting.MessageToDisplay)}");
            }
            catch (Exception ex)
            {
                ContinueOnErrorBySettingChoice(ex);
            }

            SetDataToSuccessorAndHandleRequest(request);
        }
    }
    /// <summary>
    /// Backup Creates a .zip file with the content of an input folder and stores it in the correct /backup path.
    /// </summary>
    class Backup : Handler
    {
        public override void HandleRequest(dynamic request)
        {
            try
            {
                CreateZip(request);
                LogInfo("Done: Backuping file");
            }
            catch (Exception ex)
            {
                ContinueOnErrorBySettingChoice(ex);
            }

            SetDataToSuccessorAndHandleRequest(request);
        }
    }
    /// <summary>
    /// MainProcess.
    /// </summary>
    class MainProcess : Handler
    {
        public override void HandleRequest(dynamic request)
        {
            try
            {
                if (WorkingFile != null)
                {
                    LogInfo("Done: Processing file");
                }
            }
            catch (Exception ex)
            {
                ContinueOnErrorBySettingChoice(ex);
            }

            SetDataToSuccessorAndHandleRequest(request);
        }
    }
    /// <summary>
    /// ClearLock Unlocks the selected file,now it ready for another process to use it.
    /// </summary>
    class ClearLock : Handler
    {
        public override void HandleRequest(dynamic request)
        {
            try
            {
                if (WorkingFile != null)
                {
                    var fileName = WorkingFile.Name;
                    WorkingFile?.Close();
                    LogInfo($"Done: Clear lock of file: {fileName}");
                }
            }
            catch (Exception ex)
            {
                ContinueOnErrorBySettingChoice(ex);
            }

            SetDataToSuccessorAndHandleRequest(request);
        }
    }
    /// <summary>
    /// ClearFolders Deletes the files of the input folder.
    /// </summary>
    class ClearFolders : Handler
    {
        public override void HandleRequest(dynamic request)
        {
            try
            {
                var dirs = stepSetting.FoldersSource.Split(';');
                foreach (var dir in dirs)
                {
                    if (string.IsNullOrWhiteSpace(dir) || Directory.Exists(dir) == false)
                    {
                        continue;
                    }

                    foreach (var file in new DirectoryInfo(dir).GetFiles())
                    {
                        file.Delete();
                    }
                }

                LogInfo($"Done: Cleared Folders: {stepSetting.FoldersSource.Replace(";", ", ")}");
            }
            catch (Exception ex)
            {
                ContinueOnErrorBySettingChoice(ex);
            }

            SetDataToSuccessorAndHandleRequest(request);
        }
    }

    class GetDBConfigs : Handler
    {
        public override void HandleRequest(dynamic request)
        {
            try
            {
                LogInfo("Done: GetDBConfigs");
            }
            catch (Exception ex)
            {
                ContinueOnErrorBySettingChoice(ex);
            }

            SetDataToSuccessorAndHandleRequest(request);
        }
    }
    /// <summary>
    /// SendEmail Sends an Email.The subject and the attachments can be determined by the user.
    /// </summary>
    class SendEmail : Handler
    {
        public override void HandleRequest(dynamic request)
        {
            try
            {
                var commonProperties = ((CommonProperties)request.CommonProperties);
                var smptServer = new SmtpClient();
                var mail = new MailMessage();


                if (string.IsNullOrWhiteSpace(stepSetting.Attachment) == false && File.Exists(commonProperties.MapAttributesToValues(stepSetting.Attachment)))
                {
                    stepSetting.Attachment = commonProperties.MapAttributesToValues(stepSetting.Attachment);

                    if (commonProperties.AttachmentFiles != null)
                    {
                        commonProperties.AttachmentFiles.Add(stepSetting.Attachment, stepSetting.Attachment);
                    }
                    else
                    {
                        commonProperties.AttachmentFiles = new Dictionary<string, string>
                        {
                            { stepSetting.Attachment, stepSetting.Attachment },
                        };
                    }
                }

                smptServer.Host = stepSetting.SmtpServer.Split(':').First();

                if (stepSetting.SmtpServer.Split(':').Count() == 2)
                {
                    var port = 0;
                    int.TryParse(stepSetting.SmtpServer.Split(':').Last(), out port);
                    smptServer.Port = port;
                }
                mail.From = new MailAddress(stepSetting.From);
                stepSetting.To.Split(';')
                            .ToList()
                            .ForEach(address => mail.To.Add(new MailAddress(address)));

                mail.Subject = commonProperties.MapAttributesToValues(stepSetting.Subject);

                if (commonProperties.AttachmentFiles != null && commonProperties.AttachmentFiles.Any())
                {
                    commonProperties.AttachmentFiles.ToList().ForEach(attachment =>
                    {
                        mail.Attachments.Add(new Attachment(attachment.Key, MediaTypeNames.Application.Octet));
                    });
                }

                smptServer.Send(mail);

                mail.Attachments.Dispose();
                commonProperties.AttachmentFiles = null;

                LogInfo("Done: SendEmail");
            }
            catch (Exception ex)
            {
                ContinueOnErrorBySettingChoice(ex);
            }

            SetDataToSuccessorAndHandleRequest(request);
        }
    }
    /// <summary>
    /// CheckForEmptyFiles.
    /// </summary>
    class CheckForEmptyFiles : Handler
    {
        public override void HandleRequest(dynamic request)
        {
            try
            {
                var dirs = stepSetting.FoldersSource.Split(';');
                foreach (var dir in dirs)
                {
                    if (string.IsNullOrWhiteSpace(dir) || Directory.Exists(dir) == false)
                    {
                        continue;
                    }

                    foreach (var file in new DirectoryInfo(dir).GetFiles())
                    {
                        if (file.Length == 0)
                        {
                            File.Move(file.FullName, stepSetting.FoldersTarget + "\\" + file.Name);
                        }
                    }
                }

                LogInfo("Done: CheckForEmptyFiles");
            }
            catch (Exception ex)
            {
                ContinueOnErrorBySettingChoice(ex);
            }

            SetDataToSuccessorAndHandleRequest(request);
        }
    }
    /// <summary>
    /// MainProcessFolder.
    /// </summary>
    class MainProcessFolder : Handler
    {
        public override void HandleRequest(dynamic request)
        {
            try
            {
                LogInfo("Done: MainProcessFolder");
            }
            catch (Exception ex)
            {
                ContinueOnErrorBySettingChoice(ex);
            }

            SetDataToSuccessorAndHandleRequest(request);
        }
    }
    /// <summary>
    /// MainProcessFolderMerge.
    /// </summary>
    class MainProcessFolderMerge : Handler
    {
        public override void HandleRequest(dynamic request)
        {
            try
            {
                LogInfo("Done: MainProcessFolderMerge");
            }
            catch (Exception ex)
            {
                ContinueOnErrorBySettingChoice(ex);
            }

            SetDataToSuccessorAndHandleRequest(request);
        }
    }
    /// <summary>
    /// ProcessFolderValidator.
    /// </summary>
    class ProcessFolderValidator : Handler
    {
        public override void HandleRequest(dynamic request)
        {
            try
            {
                LogInfo("Done: ProcessFolderValidator");
            }
            catch (Exception ex)
            {
                ContinueOnErrorBySettingChoice(ex);
            }

            SetDataToSuccessorAndHandleRequest(request);
        }
    }
    /// <summary>
    /// CreateZipFile.
    /// </summary>
    class CreateZipFile : Handler
    {
        public override void HandleRequest(dynamic request)
        {
            try
            {
                CreateZip(request);
                LogInfo("Done: CreateZipFile");
            }
            catch (Exception ex)
            {
                ContinueOnErrorBySettingChoice(ex);
            }

            SetDataToSuccessorAndHandleRequest(request);
        }
    }
    /// <summary>
    /// LockFile Locks another file to be used by the process.
    /// </summary>
    class LockFile : Handler
    {
        public override void HandleRequest(dynamic request)
        {
            try
            {
                var commonProperties = ((CommonProperties)request.CommonProperties);
                var fileName = stepSetting.FileSource;

                if (string.IsNullOrWhiteSpace(fileName) == false
                    && File.Exists(fileName)
                    && commonProperties.LockedFiles.ContainsKey(fileName) == false)
                {
                    commonProperties.LockedFiles.Add(fileName, new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.None));
                }
                LogInfo($"Done: Locked file: {fileName}");
            }
            catch (Exception ex)
            {
                ContinueOnErrorBySettingChoice(ex);
            }

            SetDataToSuccessorAndHandleRequest(request);
        }
    }
    /// <summary>
    /// UnlockFile Unlocks the second file ,that was locked from the process.
    /// </summary>
    class UnlockFile : Handler
    {
        public override void HandleRequest(dynamic request)
        {
            try
            {
                var commonProperties = ((CommonProperties)request.CommonProperties);
                var fileName = stepSetting.FileSource;

                if (string.IsNullOrWhiteSpace(fileName) == false
                    && commonProperties.LockedFiles.ContainsKey(fileName))
                {
                    commonProperties.LockedFiles[fileName].Close();
                    commonProperties.LockedFiles.Remove(fileName);
                    LogInfo($"Done: Clear lock of file: {fileName}");
                }
            }
            catch (Exception ex)
            {
                ContinueOnErrorBySettingChoice(ex);
            }

            SetDataToSuccessorAndHandleRequest(request);
        }
    }
    /// <summary>
    /// CheckCountersEquality Checks if the input Counters are equal.
    /// </summary>
    class CheckCountersEquality : Handler
    {
        public override void HandleRequest(dynamic request)
        {
            bool forceStop = false;
            try
            {
                var commonProperties = ((CommonProperties)request.CommonProperties);
                var counters = stepSetting.Counters.ToUpper().Replace("[", "").Replace("]", "").Split(';');

                var values = new Dictionary<string, int>();
                foreach (var count in counters)
                {
                    var value = commonProperties.Counters.ContainsKey(count) ? commonProperties.Counters[count] : 0;
                    values.Add(count, value);
                }

                if (values.Count(a => a.Value == 0) > 0)
                {
                    forceStop = true;
                    var message = values.Where(a => a.Value == 0).Select(b => b.Key);
                    ThrowException.CountersAreZero(message);
                }

                if (values.Select(a => a.Value).Distinct().Count() != 1)
                {
                    forceStop = true;
                    ThrowException.CountersAreNotEqual();
                }
            }
            catch (Exception ex)
            {
                ContinueOnErrorBySettingChoice(ex, forceStop);
            }

            SetDataToSuccessorAndHandleRequest(request);
        }
    }
    /// <summary>
    /// MakeDatabaseStats .
    /// </summary>
    class MakeDatabaseStats : Handler
    {
        public override void HandleRequest(dynamic request)
        {
            try
            {
                var ini = string.IsNullOrWhiteSpace(stepSetting.FileSource) ? stepSetting.FileSource : null;
                var exe = string.IsNullOrWhiteSpace(stepSetting.ExeFile) ? stepSetting.ExeFile : null;

                //throw new NotImplementedException();
            }
            catch (Exception ex)
            {
                ContinueOnErrorBySettingChoice(ex);
            }
            SetDataToSuccessorAndHandleRequest(request);
        }
    }

    /// <summary>
    /// MoveFile Moves a specific file from an input folder to an output/destination folder.
    /// </summary>
    class MoveFile : Handler
    {
        public override void HandleRequest(dynamic request)
        {
            try
            {
                var commonProperties = ((CommonProperties)request.CommonProperties);
                var fileSourcePath = stepSetting.FileSource;
                var message = "File Failed";
                if (File.Exists(fileSourcePath))
                {
                    File.Copy(fileSourcePath, stepSetting.FileTarget);
                    message = stepSetting.MessageToDisplay;
                }

                LogInfo($"Done: {commonProperties.MapAttributesToValues(stepSetting.MessageToDisplay)}");
            }
            catch (Exception ex)
            {
                ContinueOnErrorBySettingChoice(ex);
            }
            SetDataToSuccessorAndHandleRequest(request);
        }
    }

    class CloseOnOpenFiles : Handler
    {
        public override void HandleRequest(dynamic request)
        {
            bool forceStop = false;

            try
            {
                var dirs = stepSetting.FoldersSource.Split(';');
                foreach (var dir in dirs)
                {
                    if (string.IsNullOrWhiteSpace(dir) || Directory.Exists(dir) == false)
                    {
                        continue;
                    }
                    foreach (var file in new DirectoryInfo(dir).GetFiles())
                    {
                        try
                        {
                            var f = new FileStream(file.FullName, FileMode.Open, FileAccess.Read, FileShare.None);
                            f.Close();
                        }
                        catch (Exception ex)
                        {
                            forceStop = true;
                            ThrowException.FileIsAlreadyOpen(file.Name);
                        }
                    }
                }

                LogInfo($"Done:Files in selected folders are closed");
            }
            catch (Exception ex)
            {
                ContinueOnErrorBySettingChoice(ex, forceStop);
            }
            SetDataToSuccessorAndHandleRequest(request);
        }
    }

}