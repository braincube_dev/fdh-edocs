﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace EdocsCore
{
    public class CommonProperties
    {
        public Dictionary<string, string> AttachmentFiles { get; set; }

        private Dictionary<string, FileStream> lockedFiles;
        public Dictionary<string, FileStream> LockedFiles
        {
            get
            {
                if (lockedFiles == null)
                {
                    lockedFiles = new Dictionary<string, FileStream>();
                }
                return lockedFiles;
            }
            set
            {
                lockedFiles = value;
            }
        }

        public string DBFile { get; set; }

        public Regex bracketsContainerRegex => new Regex(
                    @"\[              # Match an opening bracket.
                      (?>             # Then either match (possessively):
                       [^\[\]]+         #  any characters except brackets
                      |               # or
                       \[ (?<Depth>)  #  an opening bracket (and increase the bracket counter)
                      |               # or
                       \] (?<-Depth>) #  a closing bracket (and decrease the bracket counter).
                      )*              # Repeat as needed.
                     (?(Depth)(?!))   # Assert that the bracket counter is at zero.
                     \]               # Then match a closing bracket.",
                    RegexOptions.IgnorePatternWhitespace);

        public Regex counterCharsToRemove => new Regex(@"[\[\]]");

        #region GlobalCounters
        Dictionary<string, int> _counters;
        public Dictionary<string, int> Counters
        {
            get
            {
                if (_counters == null)
                {
                    _counters = new Dictionary<string, int>();
                }

                return _counters;
            }
        }

        public void IncreaseCounter(string inputName)
        {
            var counterName = counterCharsToRemove.Replace(inputName.ToUpper(), "").Replace("RAISE_", "");
            if (Counters.ContainsKey(counterName))
            {
                ++Counters[counterName];
            }
            else
            {
                Counters.Add(counterName, 1);
            }
        }
        #endregion

        public string MapAttributesToValues(string message, int? functionCounter = null)
        {
            var constants = new Dictionary<string, Func<string>>
            {
                { "[DATE]", () => { return DateTime.Now.ToString("ddMMyy"); } },
                { "[DATETIME]", () => { return DateTime.Now.ToString("yyyyMMdd_HHmmss"); } },
                { "[YEARMONTH]", () => { return DateTime.Now.ToString("yyyyMM"); } },
                { "[FUNCTIONCOUNTER]", () => { return functionCounter?.ToString() ?? "0"; } },
            };

            foreach (Match match in bracketsContainerRegex.Matches(message))
            {
                var attribute = match.ToString();
                if (constants.ContainsKey(attribute.ToUpper()))
                {
                    message = message.Replace(attribute.ToUpper(), constants[attribute.ToUpper()]());
                }
                else
                {
                    var selectedCounter = counterCharsToRemove.Replace(attribute, "").ToUpper();
                    var counterValue = Counters.ContainsKey(selectedCounter) ? Counters[selectedCounter].ToString() : "0";
                    message = message.Replace(attribute, counterValue);
                }
            }

            foreach (var map in constants)
            {
                message = message.Replace(map.Key, map.Value());
            }

            return message;
        }
    }
}
