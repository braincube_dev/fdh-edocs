﻿using Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;

namespace EdocsCore
{
    public class ProcessEngine
    {
        private Dictionary<string, string> _constants;

        private Dictionary<string, string> Constants
        {
            get
            {
                if (_constants == null)
                {
                    _constants = new Dictionary<string, string>();
                }

                return _constants;
            }
            set
            {
                _constants = value;
            }
        }
        /// <summary>
        /// Start calls the correct process and setups the settings for the process's usage.
        /// </summary>
        public List<string> Start(string[] args)
        {
            Dictionary<int, Handler> handlers = null;
            var handlersMessages = new List<string>();

            try
            {
                ValidateArguments(args);

                var applicationName = args.First();
                var fileToParse = args.Length >= 2 ? args[1] : string.Empty;
                var settings = GetProcessSetting(applicationName);
                InitializeConstants(settings);
                var stepsSettings = GetStepsSettings(settings);
                handlers = GetHandlers(stepsSettings);
                var mapSettingIds = GetMappedSettingIDs(handlers);

                for (var i = 1; i <= handlers.Count - 1; i++)
                {
                    if (i + 1 <= handlers.Count)
                    {
                        handlers[mapSettingIds[i]].SetSuccessor(handlers[mapSettingIds[i + 1]]);
                    }
                }

                handlers.First().Value.HandleRequest(new
                {
                    FileName = fileToParse,
                    CommonProperties = new CommonProperties(),
                });

                handlersMessages.AddRange(handlers?.FirstOrDefault().Value.HandlerLog.Select(a => a.Message) ?? new List<string>());
            }
            catch (Exception ex)
            {
                handlersMessages.AddRange(handlers?.FirstOrDefault().Value.HandlerLog.Select(a => a.Message) ?? new List<string>());
                handlersMessages.Add(ex.Message);
            }

            return handlersMessages;
        }

        private Dictionary<int, int> GetMappedSettingIDs(Dictionary<int, Handler> handlers)
        {
            var mapSettingIds = new Dictionary<int, int>();
            var ix = 0;
            foreach (var settingID in handlers.Select(a => a.Key))
            {
                mapSettingIds.Add(++ix, settingID);
            }

            return mapSettingIds;
        }
        /// <summary>
        /// GetHandlers checks if the step exists or is valid,and if it is,adds the step to the queue of steps.
        /// </summary>
        private Dictionary<int, Handler> GetHandlers(List<StepSetting> stepsSettings)
        {
            var handlers = new Dictionary<int, Handler>();
            foreach (var step in stepsSettings)
            {
                var type = Type.GetType($"EdocsCore.{step.Name}");
                if (type == null)
                {
                    ThrowException.StepToDoNotExists(step.Name);
                }
                if (handlers.ContainsKey(step.ID))
                {
                    ThrowException.StepIdAlreadyExists();
                }
                var handler = ((Handler)Activator.CreateInstance(type));
                handler.SetStepSetting(step);
                handlers.Add(step.ID, handler);
            }

            return handlers;
        }

        private void ValidateArguments(string[] args)
        {
            if (args.Length == 0)
            {
                throw new ArgumentException("Invalid argument lentgh");
            }
        }


        private void InitializeConstants(ProcessSettings settings)
        {
            foreach (Constant constant in settings.Constants)
            {
                if (Constants.ContainsKey(constant.Key) == false)
                {
                    Constants[constant.Key] = constant.Value.Trim();
                }
            }
        }

        private ProcessSettings GetProcessSetting(string processName)
        {
            var settings = (ProcessesSettings)ConfigurationManager.GetSection("Section");
            var selectedProcess = settings?.Processes?.GetProcessByName(processName);
            if (selectedProcess == null)
            {
                ThrowException.ProcessSettingsNotFound();
            }

            return selectedProcess;
        }


        private List<StepSetting> GetStepsSettings(ProcessSettings settings)
        {
            var stepsSettings = new List<StepSetting>();
            foreach (StepSetting step in settings.Steps)
            {
                if (step.Disabled == false)
                {
                    stepsSettings.Add(MapStepValuesFromConstants(step));
                }
            }
            return stepsSettings;
        }

        private StepSetting MapStepValuesFromConstants(StepSetting step)
        {
            var stepCopy = (StepSetting)step.Clone();
            var bracketsRegex = new Regex(
                    @"\{              # Match an opening bracket.
                      (?>             # Then either match (possessively):
                       [^{}]+         #  any characters except brackets
                      |               # or
                       \{ (?<Depth>)  #  an opening paren (and increase the bracket counter)
                      |               # or
                       \} (?<-Depth>) #  a closing paren (and decrease the bracket counter).
                      )*              # Repeat as needed.
                     (?(Depth)(?!))   # Assert that the bracket counter is at zero.
                     \}               # Then match a closing bracket.",
                    RegexOptions.IgnorePatternWhitespace);

            foreach (var property in stepCopy.GetType().GetProperties())
            {
                if (string.Equals(property.PropertyType.Name, "string", StringComparison.OrdinalIgnoreCase))
                {
                    var value = property.GetValue(stepCopy, null).ToString();
                    var matches = bracketsRegex.Matches(value);
                    if (matches.Count > 0)
                    {
                        foreach (Match match in matches)
                        {
                            var matchWithoutBrackets = match.ToString().Replace("{", "").Replace("}", "");
                            if (Constants.ContainsKey(matchWithoutBrackets))
                            {
                                value = value.Replace(match.ToString(), Constants[matchWithoutBrackets]);
                            }
                        }
                        property.SetValue(stepCopy, value, null);
                    }
                }
            }
            return stepCopy;
        }
    }
}
