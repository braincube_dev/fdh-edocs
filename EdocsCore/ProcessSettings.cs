﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace EdocsCore
{

    /// <summary>
    /// Containts Classes and methods for process definition and settings.
    /// </summary>
    public class ProcessesSettings : ConfigurationSection
    {
        [ConfigurationProperty("Processes", IsDefaultCollection = false)]
        [ConfigurationCollection(typeof(ProcessSettingsCollection), AddItemName = "Processes")]
        public ProcessSettingsCollection Processes
        {
            get
            {
                return (ProcessSettingsCollection)base["Processes"];
            }
        }
    }

    public class ProcessSettingsCollection : ConfigurationElementCollection
    {
        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.BasicMap;
            }
        }

        public ProcessSettings GetProcessByName(string name)
        {
            return this.OfType<ProcessSettings>().FirstOrDefault(item => string.Equals(item.Name, name, StringComparison.InvariantCultureIgnoreCase));
        }

        public List<string> GetProcessesNames()
        {
            return this.OfType<ProcessSettings>().Select(item => item.Name).ToList();
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new ProcessSettings();
        }

        protected override ConfigurationElement CreateNewElement(string elementName)
        {
            return new ProcessSettings(elementName);
        }

        protected override Object GetElementKey(ConfigurationElement element)
        {
            return ((ProcessSettings)element).Name;
        }

        protected override void BaseAdd(ConfigurationElement element)
        {
            BaseAdd(element, false);
        }

        protected override string ElementName
        {
            get { return "Process"; }
        }
    }

    public class ProcessSettings : ConfigurationElement
    {
        public ProcessSettings()
        {
        }

        public ProcessSettings(string key)
        {
            Name = key;
        }

        [ConfigurationProperty("Name", DefaultValue = "Default", IsRequired = true, IsKey = false)]
        [StringValidator(InvalidCharacters = " ~!@#$%^&*()[]{}/;'\"|\\", MinLength = 1, MaxLength = 60)]
        public string Name
        {
            get
            {
                return (string)this["Name"];
            }
            set
            {
                this["Name"] = value;
            }
        }

        [ConfigurationProperty("Steps", IsDefaultCollection = false)]
        [ConfigurationCollection(typeof(StepSettingsCollection), AddItemName = "Steps")]
        public StepSettingsCollection Steps
        {
            get
            {
                return (StepSettingsCollection)base["Steps"];
            }
        }

        [ConfigurationProperty("Constants", IsDefaultCollection = false)]
        [ConfigurationCollection(typeof(ConstantsCollection), AddItemName = "Constants")]
        public ConstantsCollection Constants
        {
            get
            {
                return (ConstantsCollection)base["Constants"];
            }
        }
    }

    public class ConstantsCollection : ConfigurationElementCollection
    {
        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.BasicMap;
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new Constant();
        }

        protected override ConfigurationElement CreateNewElement(string elementName)
        {
            return new Constant(elementName);
        }

        protected override Object GetElementKey(ConfigurationElement element)
        {
            return ((Constant)element).Key;
        }

        protected override void BaseAdd(ConfigurationElement element)
        {
            BaseAdd(element, false);
        }

        protected override string ElementName
        {
            get { return "Constant"; }
        }
    }

    public class Constant : ConfigurationElement, ICloneable
    {
        public Constant()
        {
        }

        public Constant(string key)
        {
            Key = key;
        }

        [ConfigurationProperty("Key", DefaultValue = "Default", IsRequired = true, IsKey = true)]
        [StringValidator(InvalidCharacters = " ~!@#$%^&*()[]{}/;'\"|\\", MinLength = 1)]
        public string Key
        {
            get
            {
                return (string)this["Key"];
            }
            set
            {
                this["Key"] = value;
            }
        }

        [ConfigurationProperty("Value", DefaultValue = "Default", IsRequired = true, IsKey = false)]
        public string Value
        {
            get
            {
                return (string)this["Value"];
            }
            set
            {
                this["Value"] = value;
            }
        }

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }
    }

    public class StepSettingsCollection : ConfigurationElementCollection
    {
        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.BasicMap;
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new StepSetting();
        }

        protected override ConfigurationElement CreateNewElement(string elementName)
        {
            return new StepSetting(elementName);
        }

        protected override Object GetElementKey(ConfigurationElement element)
        {
            return ((StepSetting)element).ID;
        }

        protected override void BaseAdd(ConfigurationElement element)
        {
            BaseAdd(element, false);
        }

        protected override string ElementName
        {
            get { return "Step"; }
        }
    }

    public class StepSetting : ConfigurationElement, ICloneable
    {
        public StepSetting()
        {
        }

        public StepSetting(string name)
        {
            Name = name;
        }

        #region Common
        [ConfigurationProperty("Name", DefaultValue = "Default", IsRequired = true, IsKey = false)]
        [StringValidator(InvalidCharacters = " ~!@#$%^&*()[]{}/;'\"|\\", MinLength = 1, MaxLength = 60)]
        public string Name
        {
            get
            {
                return (string)this["Name"];
            }
            set
            {
                this["Name"] = value;
            }
        }

        [ConfigurationProperty("ID", IsRequired = true, IsKey = true)]
        public int ID
        {
            get
            {
                return (int)this["ID"];
            }
            set
            {
                this["ID"] = value;
            }
        }

        [ConfigurationProperty("ContinueOnError")]
        public bool ContinueOnError
        {
            get
            {
                return (bool)this["ContinueOnError"];
            }
            set
            {
                this["ContinueOnError"] = value;
            }
        }

        [ConfigurationProperty("Disabled")]
        public bool Disabled
        {
            get
            {
                return (bool)this["Disabled"];
            }
            set
            {
                this["Disabled"] = value;
            }
        }
        #endregion

        #region GetDBConfigs
        [ConfigurationProperty("ConnectionString")]
        public string ConnectionString
        {
            get
            {
                return (string)this["ConnectionString"];
            }
            set
            {
                this["ConnectionString"] = value;
            }
        }
        #endregion

        #region Send Email
        private string _smtpServer;
        [ConfigurationProperty("SmtpServer")]
        public string SmtpServer
        {
            get
            {
                return _smtpServer ?? (string)this["SmtpServer"];
            }
            set
            {
                _smtpServer = value;
            }
        }

        private string _from;
        [ConfigurationProperty("From")]
        public string From
        {
            get
            {
                return _from ?? (string)this["From"];
            }
            set
            {
                _from = value;
            }
        }

        private string _To;
        [ConfigurationProperty("To")]
        public string To
        {
            get
            {
                return _To ?? (string)this["To"];
            }
            set
            {
                _To = value;
            }
        }



        private string _subject;
        [ConfigurationProperty("Subject")]
        public string Subject
        {
            get
            {
                return _subject ?? (string)this["Subject"];
            }
            set
            {
                _subject = value;
            }
        }

        private string _attachment;
        [ConfigurationProperty("Attachment")]
        public string Attachment
        {
            get
            {
                return _attachment ?? (string)this["Attachment"];
            }
            set
            {
                _attachment = value;
            }
        }
        #endregion

        #region CopyToTargetAndCount

        private string _strategies;
        [ConfigurationProperty("Strategies")]
        public string Strategies
        {
            get
            {
                return _strategies ?? (string)this["Strategies"];
            }
            set
            {
                _strategies = value;
            }
        }

        private string _foldersToCreate;
        [ConfigurationProperty("FoldersToCreate")]
        public string FoldersToCreate
        {
            get
            {
                return _foldersToCreate ?? (string)this["FoldersToCreate"];
            }
            set
            {
                _foldersToCreate = value;
            }
        }
        #endregion

        private string _messageToDisplay;
        [ConfigurationProperty("MessageToDisplay")]
        public string MessageToDisplay
        {
            get
            {
                return _messageToDisplay ?? (string)this["MessageToDisplay"];
            }
            set
            {
                _messageToDisplay = value;
            }
        }

        private string _fileSource;
        [ConfigurationProperty("FileSource")]
        public string FileSource
        {
            get
            {
                return _fileSource ?? (string)this["FileSource"];
            }
            set
            {
                _fileSource = value;
            }
        }

        private string _fileTarget;
        [ConfigurationProperty("FileTarget")]
        public string FileTarget
        {
            get
            {
                return _fileTarget ?? (string)this["FileTarget"];
            }
            set
            {
                _fileTarget = value;
            }
        }

        private string _foldersSource;
        [ConfigurationProperty("FoldersSource")]
        public string FoldersSource
        {
            get
            {
                return _foldersSource ?? (string)this["FoldersSource"];
            }
            set
            {
                _foldersSource = value;
            }
        }

        private string _foldersTarget;
        [ConfigurationProperty("FoldersTarget")]
        public string FoldersTarget
        {
            get
            {
                return _foldersTarget ?? (string)this["FoldersTarget"];
            }
            set
            {
                _foldersTarget = value;
            }
        }

        private bool? _attachToEmail;
        [ConfigurationProperty("AttachToEmail")]
        public bool AttachToEmail
        {
            get
            {
                return _attachToEmail ?? (bool)this["AttachToEmail"];
            }
            set
            {
                _attachToEmail = value;
            }
        }

        private string _setDbFile;
        [ConfigurationProperty("SetDbFile")]
        public string SetDbFile
        {
            get
            {
                return _setDbFile ?? (string)this["SetDbFile"];
            }
            set
            {
                _setDbFile = value;
            }
        }

        private string _exeFile;
        [ConfigurationProperty("ExeFile")]
        public string ExeFile
        {
            get
            {
                return _exeFile ?? (string)this["ExeFile"];
            }
            set
            {
                _exeFile = value;
            }
        }

        private string _counters;
        [ConfigurationProperty("Counters")]
        public string Counters
        {
            get
            {
                return _counters ?? (string)this["Counters"];
            }
            set
            {
                _counters = value;
            }
        }

        private bool? _addTimeStamp;
        [ConfigurationProperty("AddTimeStamp")]
        public bool AddTimeStamp
        {
            get
            {
                return _addTimeStamp ?? (bool)this["AddTimeStamp"]; 
            }
            set
            {
                _addTimeStamp = value;
            }
        }

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}