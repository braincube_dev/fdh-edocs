﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdocsCore.ViewModels
{
    public class CstfExtractDataViewModel
    {
        public string CstfInputFile { get; set; }

        public string CstfOutputDirectory { get; set; }

        public int SchemeToProcess { get; set; }

        public DateTime? ImplementationDate { get; set; }

        public DateTime? LetterDate { get; set; }

        public DateTime? PointsDateFrom { get; set; }

        public DateTime? PointsDateTo { get; set; }

        public DateTime? ExpiredPointsDate { get; set; }

        public DateTime? ToBeExpiredPointsDate { get; set; }

        public bool ExtractForceSatementFile { get; set; }

        public bool ExtractPersonalAddonFile { get; set; }

        private List<string> info;
        public List<string> Info
        {
            get
            {
                if (info == null)
                {
                    info = new List<string>();
                }

                return info;
            }
            set
            {
                info = value;
            }
        }
    }
}
