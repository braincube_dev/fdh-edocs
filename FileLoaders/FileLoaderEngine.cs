﻿using FileHelpers;
using FileLoaders.Loyalty;
using FileLoaders.RecordClasses;
using FileLoaders.RecordClasses.Billing.AlphaBonusMerge;
using Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FileLoaders
{
    public class RecordTypesInfo
    {
        public Type[] RecordTypes { get; set; }
        public RecordTypeSelector Selector { get; set; }

        public Encoding SelectedEncoding => Encoding.GetEncoding("utf-8");

        public RecordTypesInfo()
        {
            RecordTypes = null;
            Selector = null;
        }

        public RecordTypesInfo(Type[] recordTypes)
        {
            RecordTypes = recordTypes;
            Selector = null;
        }

        public RecordTypesInfo(Type[] recordTypes, RecordTypeSelector selector)
        {
            RecordTypes = recordTypes;
            Selector = selector;
        }
    }

    public partial class FileLoaderEngine
    {
        private Dictionary<string, RecordTypesInfo> strategyNameTypes => new Dictionary<string, RecordTypesInfo>()
        {
            {"WRT_Table_CorporateNames", new RecordTypesInfo(new Type[] { typeof(CorporateNamesRecord) }) },
            {"BonusImagesCred-Amex-CredC-DINERS", new RecordTypesInfo(new Type[] { typeof(BonusImagesRecord) }) },
            {"MenuProducts", new RecordTypesInfo(new Type[] { typeof(MenuProductsRecord) }) },
            {"CSTP", new RecordTypesInfo(
                            new Type[]
                            {
                                typeof(LoyaltyHeaderRecord),
                                typeof(LoyaltyCardRecord),
                                typeof(LoyaltyCorporateDataRecord),
                                typeof(LoyaltyExtraPointsDataRecord),
                                typeof(LoyaltyTotalPerCardRecord),
                                typeof(LoyaltyBankAdjustmentsRecord),
                                typeof(LoyaltyFooterRecord)
                            },
                            LoyaltyCustomSelector)
            } ,
            {"AMEX_BONUS", new RecordTypesInfo(
                            new Type[]
                            {
                                typeof(Add1Record),
                                typeof(Add2Record),
                                typeof(Add3Record),
                                typeof(AddsRecord),
                                typeof(HeadRecord),
                                typeof(MessageLineRecord),
                                typeof(Msg1Record),
                                typeof(Msg2Record),
                                typeof(MsgnRecord),
                                typeof(Pts1Record),
                                typeof(Pts2Record),
                                typeof(Pts3Record),
                                typeof(Pts4Record),
                                typeof(PtsbRecord),
                                typeof(PtsmRecord),
                                typeof(Rpt1Record),
                                typeof(Rpt2Record),
                                typeof(Spc1Record),
                                typeof(Spp1Record),
                                typeof(StarRecord),
                                typeof(Sum1Record),
                                typeof(Sum2Record),
                                typeof(Sum3Record),
                                typeof(Sum4Record),
                                typeof(Sum5Record),
                                typeof(Sum6Record),
                                typeof(TracRecord),
                                typeof(TranRecord),
                                typeof(Var1Record),
                                typeof(Var2Record),
                                typeof(ZisRecord),
                            },
                            AlphaBonusMergeCustomSelector)
            } ,
        };

        /// <summary>
        /// Parses and returns file records by request.
        /// Acceptable file types
        /// 1. Excel files
        /// 2. One type record files
        /// 3. Multiple type record files
        /// </summary>
        public IEnumerable<object> GetRecords(string fileName)
        {
            if (fileIsExcel(fileName))
            {
                return new ExcelRecordParser().Parse(fileName);
            }
            else
            {
                var fileRecordInfo = GetRecordTypesInfo(fileName);
                if (fileRecordInfo.RecordTypes.Count() > 1)
                {
                    return new MultiRecordParser().Parse(fileName, fileRecordInfo);
                }
                else
                {
                    return new SingleRecordParser().Parse(fileName, fileRecordInfo);
                }
            }
        }
        
        private bool fileIsExcel(string fileName)
        {
            return fileName.EndsWith(".xlsx") || fileName.EndsWith(".xls");
        }

        private RecordTypesInfo GetRecordTypesInfo(string fileName)
        {
            foreach (var key in strategyNameTypes.Keys)
            {
                if (fileName.Contains(key))
                {
                    return strategyNameTypes[key];
                }
            }

            ThrowException.FileNotSupported();
            return null;
        }
    }
}
