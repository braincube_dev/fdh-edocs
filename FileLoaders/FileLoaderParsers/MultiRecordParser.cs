﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace FileLoaders
{
    class MultiRecordParser
    {
        /// <summary>
        /// MultiEngine parses the input file(with FILEHELPERS) and stores the content in the correct class objects.
        /// The file contains at least two different types of records.
        /// </summary>
        public IEnumerable<object> Parse(string fileName, RecordTypesInfo fileTypeInfo)
        {
            using (TextReader reader = new StreamReader(fileName))
            {
                var engine = new MultiRecordEngine(fileTypeInfo.RecordTypes);
                engine.RecordSelector = fileTypeInfo.Selector;
                engine.Encoding = fileTypeInfo.SelectedEncoding;

                engine.BeginReadStream(reader);

                object record = null;
                
                while ((record = engine.ReadNext()) != null)
                {
                    yield return record;
                }
            }
        }
    }
}
