﻿using FileLoaders.Loyalty;
using FileLoaders.RecordClasses.Billing.AlphaBonusMerge;
using System;
using System.Collections.Generic;

namespace FileLoaders
{
    public partial class FileLoaderEngine
    {
        /// <summary>
        /// LoyaltyCustomSelector return the type of the selected record.
        /// </summary>
        public Type LoyaltyCustomSelector(FileHelpers.MultiRecordEngine engine, string recordString)
        {
            if (string.IsNullOrWhiteSpace(recordString))
            {
                return null;
            }

            var strategyRecordType = new Dictionary<char, Type>
            {
                { '0', typeof(LoyaltyHeaderRecord)},
                { '1', typeof(LoyaltyCardRecord)},
                { '2', typeof(LoyaltyCorporateDataRecord)},
                { '3', typeof(LoyaltyExtraPointsDataRecord)},
                { '4', typeof(LoyaltyTotalPerCardRecord)},
                { '5', typeof(LoyaltyBankAdjustmentsRecord)},
                { '9', typeof(LoyaltyFooterRecord)},
            };

            if (strategyRecordType.ContainsKey(recordString[8]))
            {
                return strategyRecordType[recordString[8]];
            }

            return null;
        }
        /// <summary>
        /// AlphaBonusMergeCustomSelector return the type of the selected record.
        /// </summary>
        public Type AlphaBonusMergeCustomSelector(FileHelpers.MultiRecordEngine engine, string recordString)
        {
            if (string.IsNullOrWhiteSpace(recordString))
            {
                return null;
            }

            var strategyRecordType = new Dictionary<string, Type>
            {
                { "ADD1", typeof(Add1Record)},
                { "ADD2", typeof(Add2Record)},
                { "ADD3", typeof(Add3Record)},
                { "ADDS", typeof(AddsRecord)},
                { "HEAD", typeof(HeadRecord)},
                { "MSGD", typeof(MessageLineRecord)},
                { "MSG1", typeof(Msg1Record)},
                { "MSG2", typeof(Msg2Record)},
                { "MSGN", typeof(MsgnRecord)},
                { "PTS1", typeof(Pts1Record)},
                { "PTS2", typeof(Pts2Record)},
                { "PTS3", typeof(Pts3Record)},
                { "PTS4", typeof(Pts4Record)},
                { "PTSB", typeof(PtsbRecord)},
                { "PTSM", typeof(PtsmRecord)},
                { "RPT1", typeof(Rpt1Record)},
                { "RPT2", typeof(Rpt2Record)},
                { "SPC1", typeof(Spc1Record)},
                { "SPP1", typeof(Spp1Record)},
                { "STAR", typeof(StarRecord)},
                { "SUM1", typeof(Sum1Record)},
                { "SUM2", typeof(Sum2Record)},
                { "SUM3", typeof(Sum3Record)},
                { "SUM4", typeof(Sum4Record)},
                { "SUM5", typeof(Sum5Record)},
                { "SUM6", typeof(Sum6Record)},
                { "TRAC", typeof(TracRecord)},
                { "TRAN", typeof(TranRecord)},
                { "VAR1", typeof(Var1Record)},
                { "VAR2", typeof(Var2Record)},
                { "ZIS ", typeof(ZisRecord)},
            };
            var record = recordString.Substring(60, 4);
            if (strategyRecordType.ContainsKey(record))
            {
                return strategyRecordType[record];
            }

            return null;
        }
    }
}
