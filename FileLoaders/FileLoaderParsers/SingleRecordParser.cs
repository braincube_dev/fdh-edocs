﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace FileLoaders
{
    public class SingleRecordParser
    {
        /// <summary>
        /// SingleEngine parses the input file(with FILEHELPERS)and stores the content in the correct class object.
        /// </summary>
        public IEnumerable<object> Parse(string fileName, RecordTypesInfo fileTypeInfo)
        {
            using (TextReader reader = new StreamReader(fileName))
            {
                var engine = new FileHelperAsyncEngine(fileTypeInfo.RecordTypes.FirstOrDefault());
                engine.Encoding = fileTypeInfo.SelectedEncoding;

                engine.BeginReadStream(reader);

                object record = null;

                while ((record = engine.ReadNext()) != null)
                {
                    yield return record;
                }
            }
        }
    }
}
