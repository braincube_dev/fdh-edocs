﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileLoaders.RecordClasses.Billing.AlphaBonusMerge
{
    [FixedLengthRecord()]

    class Add1Record
    {
        [FieldFixedLength(7)]
        public string SerialNo;

        [FieldFixedLength(2)]
        public string ProductCodeKey;

        [FieldFixedLength(2)]
        public string RetainFlag;

        [FieldFixedLength(1)]
        public string StuffFlag;

        [FieldFixedLength(4)]
        public string BreakCode;

        [FieldFixedLength(20)]
        public string AddressKey;

        [FieldFixedLength(16)]
        public string AccountNo;

        [FieldFixedLength(4)]
        public string StatementNo;

        [FieldFixedLength(4)]
        public string LineNo;

        [FieldFixedLength(4)]
        public string LineID;

        [FieldFixedLength(2)]
        public string ProductCode;

        [FieldFixedLength(3)]
        public string AddID;

        [FieldFixedLength(16)]
        public string AddAccountNo;

        [FieldFixedLength(20)]
        public string AddPersSurname;

        [FieldFixedLength(20)]
        public string AddPersName;

        [FieldFixedLength(1)]
        public string AddSex;

        [FieldFixedLength(16)]
        public string AddSpendTotal;

        [FieldFixedLength(30)]
        public string AddProsfName;

        [FieldFixedLength(30)]
        public string AddProsfSort;

        [FieldFixedLength(20)]
        public string AddAitSurname;

        [FieldFixedLength(20)]
        public string AddAitName;

        [FieldFixedLength(258)]
        public string Trailer;
    }
}
