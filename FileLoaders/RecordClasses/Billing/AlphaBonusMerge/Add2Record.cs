﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileLoaders.RecordClasses.Billing.AlphaBonusMerge
{
    [FixedLengthRecord()]

    class Add2Record
    {
        [FieldFixedLength(7)]
        public string SerialNo;

        [FieldFixedLength(2)]
        public string ProductCodeKey;

        [FieldFixedLength(2)]
        public string RetainFlag;

        [FieldFixedLength(1)]
        public string StuffFlag;

        [FieldFixedLength(4)]
        public string BreakCode;

        [FieldFixedLength(20)]
        public string AddressKey;

        [FieldFixedLength(16)]
        public string AccountNo;

        [FieldFixedLength(4)]
        public string StatementNo;

        [FieldFixedLength(4)]
        public string LineNo;

        [FieldFixedLength(4)]
        public string LineID;

        [FieldFixedLength(2)]
        public string ProductCode;

        [FieldFixedLength(16)]
        public string AddSpendTotalALT;

        [FieldFixedLength(16)]
        public string AddSpendAllow;

        [FieldFixedLength(16)]
        public string AddSpendAllowALT;

        [FieldFixedLength(16)]
        public string AddSpendingLimit;

        [FieldFixedLength(16)]
        public string AddSpendingLimitALT;

        [FieldFixedLength(5)]
        public string AddMsg1;

        [FieldFixedLength(5)]
        public string AddMsg2;

        [FieldFixedLength(5)]
        public string AddMsg3;

        [FieldFixedLength(5)]
        public string AddMsg4;

        [FieldFixedLength(5)]
        public string AddMsg5;

        [FieldFixedLength(5)]
        public string AddMsg6;

        [FieldFixedLength(5)]
        public string AddMsg7;

        [FieldFixedLength(5)]
        public string AddMsg8;

        [FieldFixedLength(10)]
        public string AddCustomerCode;

        [FieldFixedLength(1)]
        public string AddAccountStatus1;

        [FieldFixedLength(1)]
        public string AddAccountStatus2;

        [FieldFixedLength(20)]
        public string AddPersLSurname;

        [FieldFixedLength(20)]
        public string AddPersLName;

        [FieldFixedLength(20)]
        public string AddEmplAdrCompany;

        [FieldFixedLength(242)]
        public string Trailer;
    }
}
