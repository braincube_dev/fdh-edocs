﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace FileLoaders.RecordClasses.Billing.AlphaBonusMerge
{
    [FixedLengthRecord()]
    class Add3Record
    {
        [FieldFixedLength(7)]
        public string SerialNo;

        [FieldFixedLength(2)]
        public string ProductCodeKey;

        [FieldFixedLength(2)]
        public string RetainFlag;

        [FieldFixedLength(1)]
        public string StuffFlag;

        [FieldFixedLength(4)]
        public string BreakCode;

        [FieldFixedLength(20)]
        public string AddressKey;

        [FieldFixedLength(16)]
        public string AccountNo;

        [FieldFixedLength(4)]
        public string StatementNo;

        [FieldFixedLength(4)]
        public string LineNo;

        [FieldFixedLength(4)]
        public string LineID;

        [FieldFixedLength(2)]
        public string ProductCode;

        [FieldFixedLength(30)]
        public string AddEmplAdrAddress;

        [FieldFixedLength(20)]
        public string AddEmplAdrDetail;

        [FieldFixedLength(6)]
        public string AddEmplAdrPoBox;

        [FieldFixedLength(20)]
        public string AddEmplAdrTown;

        [FieldFixedLength(5)]
        public string AddEmplAdrPostCode;

        [FieldFixedLength(20)]
        public string AddEmplAdrCountry;

        [FieldFixedLength(20)]
        public string AddFather;

        [FieldFixedLength(20)]
        public string AddFatherL;

        [FieldFixedLength(293)]
        public string Trailer;
    }
}
