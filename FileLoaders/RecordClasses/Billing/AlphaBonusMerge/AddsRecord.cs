﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace FileLoaders.RecordClasses.Billing.AlphaBonusMerge
{
    [FixedLengthRecord()]

    class AddsRecord
    {
        [FieldFixedLength(7)]
        public string SerialNo;

        [FieldFixedLength(2)]
        public string ProductCodeKey;

        [FieldFixedLength(2)]
        public string RetainFlag;

        [FieldFixedLength(1)]
        public string StuffFlag;

        [FieldFixedLength(4)]
        public string BreakCode;

        [FieldFixedLength(20)]
        public string AddressKey;

        [FieldFixedLength(16)]
        public string AccountNo;

        [FieldFixedLength(4)]
        public string StatementNo;

        [FieldFixedLength(4)]
        public string LineNo;

        [FieldFixedLength(4)]
        public string LineID;

        [FieldFixedLength(2)]
        public string ProductCode;

        [FieldFixedLength(20)]
        public string PlstSurname1;

        [FieldFixedLength(20)]
        public string PlstName1;

        [FieldFixedLength(10)]
        public string PlstFather1;

        [FieldFixedLength(20)]
        public string PlstSurname2;

        [FieldFixedLength(20)]
        public string PlstName2;

        [FieldFixedLength(10)]
        public string PlstFather2;

        [FieldFixedLength(20)]
        public string ChequeBcd;

        [FieldFixedLength(10)]
        public string Filler;

        [FieldFixedLength(16)]
        public string AddAccountNo1;

        [FieldFixedLength(16)]
        public string AddAccountNo2;

        [FieldFixedLength(272)]
        public string Trailer;

    }
}
