﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileLoaders.RecordClasses.Billing.AlphaBonusMerge
{
    [FixedLengthRecord()]
    class HeadRecord
    {
        [FieldFixedLength(7)]
        public string SerialNo;

        [FieldFixedLength(2)]
        public string ProductCodeKey;

        [FieldFixedLength(2)]
        public string RetainFlag;

        [FieldFixedLength(1)]
        public string StuffFlag;

        [FieldFixedLength(4)]
        public string BreakCode;

        [FieldFixedLength(20)]
        public string AddressKey;

        [FieldFixedLength(16)]
        public string AccountNo;

        [FieldFixedLength(4)]
        public string StatementNo;

        [FieldFixedLength(4)]
        public string LineNo;

        [FieldFixedLength(4)]
        public string LineID;

        [FieldFixedLength(2)]
        public string ProductCode;

        [FieldFixedLength(25)]
        public string HeadLatinTitle;

        [FieldFixedLength(4)]
        public string HeadBankID;

        [FieldFixedLength(4)]
        public string HeadIssueDateYYYY;

        [FieldFixedLength(2)]
        public string HeadIssueDateMM;

        [FieldFixedLength(2)]
        public string HeadIssueDateDD;

        [FieldFixedLength(397)]
        public string Trailer;
    }
}
