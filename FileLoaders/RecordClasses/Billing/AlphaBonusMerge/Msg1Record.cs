﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace FileLoaders.RecordClasses.Billing.AlphaBonusMerge
{
    [FixedLengthRecord()]
    class Msg1Record
    {
        [FieldFixedLength(7)]
        public string SerialNo;

        [FieldFixedLength(2)]
        public string ProductCodeKey;

        [FieldFixedLength(2)]
        public string RetainFlag;

        [FieldFixedLength(1)]
        public string StuffFlag;

        [FieldFixedLength(4)]
        public string BreakCode;

        [FieldFixedLength(20)]
        public string AddressKey;

        [FieldFixedLength(16)]
        public string AccountNo;

        [FieldFixedLength(4)]
        public string StatementNo;

        [FieldFixedLength(4)]
        public string LineNo;

        [FieldFixedLength(4)]
        public string LineID;

        [FieldFixedLength(2)]
        public string ProductCode;

        [FieldFixedLength(3)]
        public string MsgID;

        [FieldFixedLength(16)]
        public string MsgTotalPay;

        [FieldFixedLength(16)]
        public string MsgTotalPayALT;

        [FieldFixedLength(16)]
        public string MsgTotalSale;

        [FieldFixedLength(16)]
        public string MsgTotalSaleALT;

        [FieldFixedLength(16)]
        public string MsgFrozenInterest;

        [FieldFixedLength(16)]
        public string MsgFrozenInterestALT;

        [FieldFixedLength(16)]
        public string MsgLocalCadvAllow;

        [FieldFixedLength(16)]
        public string MsgLocalCadvAllowALT;

        [FieldFixedLength(16)]
        public string MsgInalCadvAllow;

        [FieldFixedLength(16)]
        public string MsgInalCadvAllowALT;

        [FieldFixedLength(16)]
        public string MsgWoExpences;

        [FieldFixedLength(16)]
        public string MsgWoExpensesALT;

        [FieldFixedLength(16)]
        public string MsgWoPrevCapital;

        [FieldFixedLength(4)]
        public string PrevStmntsDateYYYY;

        [FieldFixedLength(2)]
        public string PrevStmntsDateMM;

        [FieldFixedLength(2)]
        public string PrevStmntsDateDD;

        [FieldFixedLength(10)]
        public string EmployerCode;

        [FieldFixedLength(205)]
        public string Trailer;
    }
}
