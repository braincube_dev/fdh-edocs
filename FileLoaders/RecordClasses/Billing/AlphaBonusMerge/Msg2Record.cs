﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileLoaders.RecordClasses.Billing.AlphaBonusMerge
{
    [FixedLengthRecord()]

    class Msg2Record
    {
        [FieldFixedLength(7)]
        public string SerialNo;

        [FieldFixedLength(2)]
        public string ProductCodeKey;

        [FieldFixedLength(2)]
        public string RetainFlag;

        [FieldFixedLength(1)]
        public string StuffFlag;

        [FieldFixedLength(4)]
        public string BreakCode;

        [FieldFixedLength(20)]
        public string AddressKey;

        [FieldFixedLength(16)]
        public string AccountNo;

        [FieldFixedLength(4)]
        public string StatementNo;

        [FieldFixedLength(4)]
        public string LineNo;

        [FieldFixedLength(4)]
        public string LineID;

        [FieldFixedLength(2)]
        public string ProductCode;

        [FieldFixedLength(16)]
        public string MsgWoPrevCapitalALT;

        [FieldFixedLength(11)]
        public string MsgEurRate;

        [FieldFixedLength(7)]
        public string MsgPoints;

        [FieldFixedLength(33)]
        public string MsgLine1;

        [FieldFixedLength(33)]
        public string MsgLine2;

        [FieldFixedLength(16)]
        public string MsgMinPayOld;

        [FieldFixedLength(16)]
        public string MsgMinPayOldALT;

        [FieldFixedLength(16)]
        public string MsgTotalCash;

        [FieldFixedLength(16)]
        public string MsgTotalCashAlt;

        [FieldFixedLength(16)]
        public string MsgDlnqAmt;

        [FieldFixedLength(16)]
        public string MsgDlnqAmtALT;

        [FieldFixedLength(16)]
        public string MsgNoIrstAmt;

        [FieldFixedLength(222)]
        public string Trailer;

    }
}
