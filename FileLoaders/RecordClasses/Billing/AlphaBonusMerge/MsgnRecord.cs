﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileLoaders.RecordClasses.Billing.AlphaBonusMerge
{
    [FixedLengthRecord()]

    class MsgnRecord
    {
        [FieldFixedLength(7)]
        public string SerialNo;

        [FieldFixedLength(2)]
        public string ProductCodeKey;

        [FieldFixedLength(2)]
        public string RetainFlag;

        [FieldFixedLength(1)]
        public string StuffFlag;

        [FieldFixedLength(4)]
        public string BreakCode;

        [FieldFixedLength(20)]
        public string AddressKey;

        [FieldFixedLength(16)]
        public string AccountNo;

        [FieldFixedLength(4)]
        public string StatementNo;

        [FieldFixedLength(4)]
        public string LineNo;

        [FieldFixedLength(4)]
        public string LineID;

        [FieldFixedLength(2)]
        public string ProductCode;

        [FieldFixedLength(2)]
        public string MessageMsgnProductCode;

        [FieldFixedLength(5)]
        public string MessageMsgn1;

        [FieldFixedLength(5)]
        public string MessageMsgn2;

        [FieldFixedLength(5)]
        public string MessageMsgn3;

        [FieldFixedLength(5)]
        public string MessageMsgn4;

        [FieldFixedLength(5)]
        public string MessageMsgn5;

        [FieldFixedLength(5)]
        public string MessageMsgn6;

        [FieldFixedLength(5)]
        public string MessageMsgn7;

        [FieldFixedLength(5)]
        public string MessageMsgn8;

        [FieldFixedLength(8)]
        public string PressPeriodFrom;

        [FieldFixedLength(4)]
        public string PressPeriodFromYYYY;

        [FieldFixedLength(2)]
        public string PressPeriodFromMM;

        [FieldFixedLength(2)]
        public string PressPeriodDD;

        [FieldFixedLength(8)]
        public string PressPeriodTo;

        [FieldFixedLength(4)]
        public string PressPeriodToYYYY;

        [FieldFixedLength(2)]
        public string PressPeriodToMM;

        [FieldFixedLength(22)]
        public string PressPeriodToDD;

        [FieldFixedLength(368)]
        public string Trailer;
    }
}
