﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileLoaders.RecordClasses.Billing.AlphaBonusMerge
{
    [FixedLengthRecord()]

    class Pts2Record
    {
        [FieldFixedLength(7)]
        public string SerialNo;

        [FieldFixedLength(2)]
        public string ProductCodeKey;

        [FieldFixedLength(2)]
        public string RetainFlag;

        [FieldFixedLength(1)]
        public string StuffFlag;

        [FieldFixedLength(4)]
        public string BreakCode;

        [FieldFixedLength(20)]
        public string AddressKey;

        [FieldFixedLength(16)]
        public string AccountNo;

        [FieldFixedLength(4)]
        public string StatementNo;

        [FieldFixedLength(4)]
        public string LineNo;

        [FieldFixedLength(4)]
        public string LineID;

        [FieldFixedLength(2)]
        public string ProductCode;

        [FieldFixedLength(16)]
        public string RewPtsLost;

        [FieldFixedLength(16)]
        public string RewPtsEnd;

        [FieldFixedLength(16)]
        public string RewPtsExpire;

        [FieldFixedLength(4)]
        public string RewPtsExpireDateYYYY;

        [FieldFixedLength(2)]
        public string RewPtsExpireDateMM;

        [FieldFixedLength(2)]
        public string RewPtsExpireDateDD;

        [FieldFixedLength(10)]
        public string RewPtsChequeNo;

        [FieldFixedLength(10)]
        public string RewPtsLotNoFrom;

        [FieldFixedLength(10)]
        public string RewPtsLotNoTo;

        [FieldFixedLength(16)]
        public string RewPtsInMgm;

        [FieldFixedLength(16)]
        public string RewPtsInWelcome;

        [FieldFixedLength(16)]
        public string RewPtsInEvent1;

        [FieldFixedLength(16)]
        public string RewPtsInEvent2;

        [FieldFixedLength(16)]
        public string RewPtsInEvent3;

        [FieldFixedLength(16)]
        public string RewPtsLsContrib;

        [FieldFixedLength(16)]
        public string RewPtsTotalContrib;

        [FieldFixedLength(8)]
        public string RewPtsCumPtsTrxReq;

        [FieldFixedLength(228)]
        public string Trailer;
    }
}
