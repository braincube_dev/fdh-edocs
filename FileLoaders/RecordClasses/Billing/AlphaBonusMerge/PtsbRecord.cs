﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileLoaders.RecordClasses.Billing.AlphaBonusMerge
{
    [FixedLengthRecord()]

    class PtsbRecord
    {
        [FieldFixedLength(7)]
        public string SerialNo;

        [FieldFixedLength(2)]
        public string ProductCodeKey;

        [FieldFixedLength(2)]
        public string RetainFlag;

        [FieldFixedLength(1)]
        public string StuffFlag;

        [FieldFixedLength(4)]
        public string BreakCode;

        [FieldFixedLength(20)]
        public string AddressKey;

        [FieldFixedLength(16)]
        public string AccountNo;

        [FieldFixedLength(4)]
        public string StatementNo;

        [FieldFixedLength(4)]
        public string LineNo;

        [FieldFixedLength(4)]
        public string LineID;

        [FieldFixedLength(2)]
        public string ProductCode;

        [FieldFixedLength(8)]
        public string BonusPtsDate;

        [FieldFixedLength(16)]
        public string TotalPointsMain;

        [FieldFixedLength(16)]
        public string OnCardPointsMain;

        [FieldFixedLength(16)]
        public string OffCardPointsMain;

        [FieldFixedLength(16)]
        public string TotalPointsAddon1;

        [FieldFixedLength(16)]
        public string TotalPointsAddon2;

        [FieldFixedLength(16)]
        public string BonusAccount1;

        [FieldFixedLength(16)]
        public string BonusAccount2;

        [FieldFixedLength(16)]
        public string BonusAccount3;
            
        [FieldFixedLength(16)]
        public string BonusAccount4;

        [FieldFixedLength(16)]
        public string BonusAccount5;

        [FieldFixedLength(16)]
        public string BonusAccount6;

        [FieldFixedLength(250)]
        public string Trailer;
    }
}
