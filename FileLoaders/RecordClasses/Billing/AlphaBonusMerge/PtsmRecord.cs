﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileLoaders.RecordClasses.Billing.AlphaBonusMerge
{
    [FixedLengthRecord()]

    class PtsmRecord
    {
        [FieldFixedLength(7)]
        public string SerialNo;

        [FieldFixedLength(2)]
        public string ProductCodeKey;

        [FieldFixedLength(2)]
        public string RetainFlag;

        [FieldFixedLength(1)]
        public string StuffFlag;

        [FieldFixedLength(4)]
        public string BreakCode;

        [FieldFixedLength(20)]
        public string AddressKey;

        [FieldFixedLength(16)]
        public string AccountNo;

        [FieldFixedLength(4)]
        public string StatementNo;

        [FieldFixedLength(4)]
        public string LineNo;

        [FieldFixedLength(4)]
        public string LineID;

        [FieldFixedLength(2)]
        public string ProductCode;

        [FieldFixedLength(16)]
        public string RewPtsOutIssue1;

        [FieldFixedLength(16)]
        public string RewPtsOutIssue2;

        [FieldFixedLength(16)]
        public string RewPtsOutIssue3;

        [FieldFixedLength(16)]
        public string RewPtsTotalContrib1;

        [FieldFixedLength(16)]
        public string RewPtsTotalContrib2;

        [FieldFixedLength(16)]
        public string RewPtsTotalContrib3;

        [FieldFixedLength(338)]
        public string Trailer;

    }
}
