﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileLoaders.RecordClasses.Billing.AlphaBonusMerge
{
    [FixedLengthRecord()]

    class Rpt2Record
    {
        [FieldFixedLength(7)]
        public string SerialNo;

        [FieldFixedLength(2)]
        public string ProductCodeKey;

        [FieldFixedLength(2)]
        public string RetainFlag;

        [FieldFixedLength(1)]
        public string StuffFlag;

        [FieldFixedLength(4)]
        public string BreakCode;

        [FieldFixedLength(20)]
        public string AddressKey;

        [FieldFixedLength(16)]
        public string AccountNo;

        [FieldFixedLength(4)]
        public string StatementNo;

        [FieldFixedLength(4)]
        public string LineNo;

        [FieldFixedLength(4)]
        public string LineID;

        [FieldFixedLength(2)]
        public string ProductCode;

        [FieldFixedLength(16)]
        public string RewPts1Expire;

        [FieldFixedLength(4)]
        public string RewPts1ExpireDateYYYY;

        [FieldFixedLength(2)]
        public string RewPts1ExpireDateMM;

        [FieldFixedLength(2)]
        public string RewPts1ExpireDateDD;

        [FieldFixedLength(10)]
        public string RewPts1ChequeNo;

        [FieldFixedLength(16)]
        public string RewPts1InMgm;

        [FieldFixedLength(16)]
        public string RewPts1InWelcome;

        [FieldFixedLength(16)]
        public string RewPts1InEvent1;

        [FieldFixedLength(16)]
        public string RewPts1InEvent2;

        [FieldFixedLength(16)]
        public string RewPts1InEvent3;

        [FieldFixedLength(16)]
        public string RewPts1LsContrib;

        [FieldFixedLength(16)]
        public string RewPts1TotalContrib;

        [FieldFixedLength(288)]
        public string Trailer;

    }
}
