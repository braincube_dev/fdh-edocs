﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileLoaders.RecordClasses.Billing.AlphaBonusMerge
{
    [FixedLengthRecord()]

    class Spc1Record
    {
        [FieldFixedLength(7)]
        public string SerialNo;

        [FieldFixedLength(2)]
        public string ProductCodeKey;

        [FieldFixedLength(2)]
        public string RetainFlag;

        [FieldFixedLength(1)]
        public string StuffFlag;

        [FieldFixedLength(4)]
        public string BreakCode;

        [FieldFixedLength(20)]
        public string AddressKey;

        [FieldFixedLength(16)]
        public string AccountNo;

        [FieldFixedLength(4)]
        public string StatementNo;

        [FieldFixedLength(4)]
        public string LineNo;

        [FieldFixedLength(4)]
        public string LineID;

        [FieldFixedLength(2)]
        public string ProductCode;

        [FieldFixedLength(16)]
        public string AccumPointsAmt1;

        [FieldFixedLength(16)]
        public string AccumPointsAmt2;

        [FieldFixedLength(16)]
        public string AccumPointsAmt3;

        [FieldFixedLength(16)]
        public string AccumPointsAmt4;

        [FieldFixedLength(16)]
        public string AccumPointsAmt5;

        [FieldFixedLength(16)]
        public string AccumPointsAmt6;

        [FieldFixedLength(16)]
        public string AccumPointsAmt7;

        [FieldFixedLength(16)]
        public string AccumPointsAmt8;

        [FieldFixedLength(16)]
        public string AccumPointsAmt9;

        [FieldFixedLength(16)]
        public string AccumPointsAmt10;

        [FieldFixedLength(16)]
        public string AccumPointsAmt11;

        [FieldFixedLength(16)]
        public string AccumPointsAmt12;

        [FieldFixedLength(16)]
        public string AccumPointsAmt13;

        [FieldFixedLength(16)]
        public string RewPtsTotalOutIssue;

        [FieldFixedLength(210)]
        public string Trailers;


    }
}
