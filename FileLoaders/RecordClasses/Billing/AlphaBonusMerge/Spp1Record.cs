﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileLoaders.RecordClasses.Billing.AlphaBonusMerge
{
    [FixedLengthRecord()]

    class Spp1Record
    {
        [FieldFixedLength(7)]
        public string SerialNo;

        [FieldFixedLength(2)]
        public string ProductCodeKey;

        [FieldFixedLength(2)]
        public string RetainFlag;

        [FieldFixedLength(1)]
        public string StuffFlag;

        [FieldFixedLength(4)]
        public string BreakCode;

        [FieldFixedLength(20)]
        public string AddressKey;

        [FieldFixedLength(16)]
        public string AccountNo;

        [FieldFixedLength(4)]
        public string StatementNo;

        [FieldFixedLength(4)]
        public string LineNo;

        [FieldFixedLength(4)]
        public string LineID;

        [FieldFixedLength(2)]
        public string ProductCode;

        [FieldFixedLength(3)]
        public string SppID;

        [FieldFixedLength(16)]
        public string SppAccountNo;

        [FieldFixedLength(16)]
        public string SppTrxAmt;

        [FieldFixedLength(16)]
        public string SppTrxAmtALT;

        [FieldFixedLength(5)]
        public string SppInstReqCnt;

        [FieldFixedLength(16)]
        public string SppInstMonthAmt;

        [FieldFixedLength(16)]
        public string SppInstMonthAmtALT;

        [FieldFixedLength(5)]
        public string SppInstPaidCnt;

        [FieldFixedLength(5)]
        public string SppInstToPayCnt;

        [FieldFixedLength(336)]
        public string Trailer;
    }
}
