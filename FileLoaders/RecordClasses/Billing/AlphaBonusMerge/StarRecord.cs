﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileLoaders.RecordClasses.Billing.AlphaBonusMerge
{
    [FixedLengthRecord()]

    class StarRecord
    {
        [FieldFixedLength(7)]
        public string SerialNo;

        [FieldFixedLength(2)]
        public string ProductCodeKey;

        [FieldFixedLength(2)]
        public string RetainFlag;

        [FieldFixedLength(1)]
        public string StuffFlag;

        [FieldFixedLength(4)]
        public string BreakCode;

        [FieldFixedLength(20)]
        public string AddressKey;

        [FieldFixedLength(16)]
        public string AccountNo;

        [FieldFixedLength(4)]
        public string StatementNo;

        [FieldFixedLength(4)]
        public string LineNo;

        [FieldFixedLength(4)]
        public string LineID;

        [FieldFixedLength(2)]
        public string ProductCode;

        [FieldFixedLength(12)]
        public string StarRewardNo;

        [FieldFixedLength(8)]
        public string StarDateFrom;

        [FieldFixedLength(8)]
        public string StarDateTo;

        [FieldFixedLength(16)]
        public string StarPtsStart;

        [FieldFixedLength(14)]
        public string StarPtsNew;

        [FieldFixedLength(14)]
        public string StarPtsRedeem;

        [FieldFixedLength(14)]
        public string StarPtsApply;

        [FieldFixedLength(14)]
        public string StarPtsAdj;

        [FieldFixedLength(14)]
        public string StarPtsEnd;

        [FieldFixedLength(2)]
        public string StarNoOfMsg;

        [FieldFixedLength(5)]
        public string StarMsg1No;

        [FieldFixedLength(5)]
        public string StarMsg2No;

        [FieldFixedLength(5)]
        public string StarMsg3No;

        [FieldFixedLength(5)]
        public string StarMsg4No;

        [FieldFixedLength(5)]
        public string StarMsg5No;

        [FieldFixedLength(283)]
        public string Trailer;
    }
}
