﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileLoaders.RecordClasses.Billing.AlphaBonusMerge
{
    [FixedLengthRecord()]
    class Sum1Record
    {
        [FieldFixedLength(7)]
        public string   SerialNo;

        [FieldFixedLength(2)]
        public string ProductCodeKey;

        [FieldFixedLength(2)]
        public string RetainFlag;
        
        [FieldFixedLength(1)]
        public string StuffFlag;

        [FieldFixedLength(4)]
        public string BreakCode;

        [FieldFixedLength(20)]
        public string AddressKey;

        [FieldFixedLength(16)]
        public string AccountNo;

        [FieldFixedLength(4)]
        public string StatementNo;

        [FieldFixedLength(4)]
        public string LineNo;

        [FieldFixedLength(4)]
        public string LineID;

        [FieldFixedLength(2)]
        public string ProductCode;

        [FieldFixedLength(20)]
        public string Pers_Surname;

        [FieldFixedLength(20)]
        public string Pers_Name;

        [FieldFixedLength(20)]
        public string Pers_Father;

        [FieldFixedLength(1)]
        public string Pers_Sex;

        [FieldFixedLength(20)]
        public string Name_Title;

        [FieldFixedLength(30)]
        public string Adr_Employer;

        [FieldFixedLength(6)]
        public string Adr_PoBox;

        [FieldFixedLength(20)]
        public string Adr_Detail;

        [FieldFixedLength(30)]
        public string Adr_Address;

        [FieldFixedLength(20)]
        public string Adr_Town;

        [FieldFixedLength(5)]
        public string Adr_Post_Code;

        [FieldFixedLength(15)]
        public string Adr_Country;

        [FieldFixedLength(10)]
        public string Adr_PhoneNo;

        [FieldFixedLength(1)]
        public string Stmtn_Print_Flag;

        [FieldFixedLength(1)]
        public string Annual_Fee_Discount_Flag;

        [FieldFixedLength(4)]
        public string BirthDateYEAR;

        [FieldFixedLength(2)]
        public string BirthDateMONTH;

        [FieldFixedLength(2)]
        public string BirthDateDAY;

        [FieldFixedLength(4)]
        public string IssOccupationCode;

        [FieldFixedLength(1)]
        public string StmntStatementAddress;

        [FieldFixedLength(40)]
        public string StmntExtAdd1;

        [FieldFixedLength(40)]
        public string StmntExtAdd2;

        [FieldFixedLength(40)]
        public string StmntExtAdd3;

        [FieldFixedLength(40)]
        public string StmntExtAdd4;

        [FieldFixedLength(1)]
        public string StmntExtAddFlag;

        [FieldFixedLength(6)]
        public string StmntContrSalInt;

        [FieldFixedLength(6)]
        public string StmntContrSalCash;

        [FieldFixedLength(29)]
        public string Trailer;
    }
}
