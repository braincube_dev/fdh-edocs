﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileLoaders.RecordClasses.Billing.AlphaBonusMerge
{
    [FixedLengthRecord()]
    class Sum2Record
    {
        [FieldFixedLength(7)]
        public string SerialNo;

        [FieldFixedLength(2)]
        public string ProductCodeKey;

        [FieldFixedLength(2)]
        public string RetainFlag;

        [FieldFixedLength(1)]
        public string StuffFlag;

        [FieldFixedLength(4)]
        public string BreakCode;

        [FieldFixedLength(20)]
        public string AddressKey;

        [FieldFixedLength(16)]
        public string AccountNo;

        [FieldFixedLength(4)]
        public string StatementNo;

        [FieldFixedLength(4)]
        public string LineNo;

        [FieldFixedLength(4)]
        public string LineID;

        [FieldFixedLength(2)]
        public string ProductCode;

        [FieldFixedLength(1)]
        public string Insert1;

        [FieldFixedLength(1)]
        public string Insert2;

        [FieldFixedLength(1)]
        public string Insert3;

        [FieldFixedLength(1)]
        public string Insert4;

        [FieldFixedLength(5)]
        public string InterestRate;

        [FieldFixedLength(5)]
        public string OverInterestRate;

        [FieldFixedLength(5)]
        public string CadvInterestRate;

        [FieldFixedLength(5)]
        public string XferInterestRate;

        [FieldFixedLength(4)]
        public string IssueDateYEAR;

        [FieldFixedLength(2)]
        public string IssueDateMONTH;

        [FieldFixedLength(2)]
        public string IssueDateDAY;

        [FieldFixedLength(4)]
        public string PayDateYEAR;

        [FieldFixedLength(2)]
        public string PayDateMONTH;

        [FieldFixedLength(2)]
        public string PayDateDAY;

        [FieldFixedLength(16)]
        public string OpeningBalance;

        [FieldFixedLength(16)]
        public string OpeningBalanceAlt;

        [FieldFixedLength(16)]
        public string ClosingBalance;

        [FieldFixedLength(16)]
        public string ClosingBalanceAlt;

        [FieldFixedLength(16)]
        public string MinPayDue;

        [FieldFixedLength(16)]
        public string MinPayDueALT;

        [FieldFixedLength(16)]
        public string MinPayDueOLD;

        [FieldFixedLength(16)]
        public string MinPayDueOLDALT;

        [FieldFixedLength(16)]
        public string OldCreditLimit;

        [FieldFixedLength(16)]
        public string OldCreditLimitALT;

        [FieldFixedLength(16)]
        public string InstallAMT;

        [FieldFixedLength(16)]
        public string PsCreditLimit;

        [FieldFixedLength(2)]
        public string Bucket;

        [FieldFixedLength(1)]
        public string LegalFlag;

        [FieldFixedLength(2)]
        public string BlackListCode;

        [FieldFixedLength(5)]
        public string XCADVInterestRate;

        [FieldFixedLength(5)]
        public string XSaleInterestRate;

        [FieldFixedLength(187)]
        public string Trailer;

    }
}
