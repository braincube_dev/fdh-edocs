﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace FileLoaders.RecordClasses.Billing.AlphaBonusMerge
{
    [FixedLengthRecord()]
    class Sum3Record
    {
        [FieldFixedLength(7)]
        public string SerialNo;

        [FieldFixedLength(2)]
        public string ProductCodeKey;

        [FieldFixedLength(2)]
        public string RetainFlag;

        [FieldFixedLength(1)]
        public string StuffFlag;

        [FieldFixedLength(4)]
        public string BreakCode;

        [FieldFixedLength(20)]
        public string AddressKey;

        [FieldFixedLength(16)]
        public string AccountNo;

        [FieldFixedLength(4)]
        public string StatementNo;

        [FieldFixedLength(4)]
        public string LineNo;

        [FieldFixedLength(4)]
        public string LineID;

        [FieldFixedLength(2)]
        public string ProductCode;

        [FieldFixedLength(16)]
        public string InstallAmtALT;

        [FieldFixedLength(16)]
        public string CreditLimit;

        [FieldFixedLength(16)]
        public string CreditLimitALT;

        [FieldFixedLength(16)]
        public string SpendingLimit;

        [FieldFixedLength(16)]
        public string SpendingLimitALT;

        [FieldFixedLength(16)]
        public string DollarBalance;

        [FieldFixedLength(16)]
        public string PsMinPayDue;

        [FieldFixedLength(16)]
        public string PsMinPayDueALT;

        [FieldFixedLength(16)]
        public string FrozenInterest;

        [FieldFixedLength(16)]
        public string FrozenInterestALT;

        [FieldFixedLength(2)]
        public string IntAlgorithm;

        [FieldFixedLength(1)]
        public string Flag;

        [FieldFixedLength(4)]
        public string RenewalDateYYYY;

        [FieldFixedLength(2)]
        public string RenewalDateMM;

        [FieldFixedLength(2)]
        public string RenewalDateDD;

        [FieldFixedLength(16)]
        public string NewCreditLimit;

        [FieldFixedLength(16)]
        public string NewCreditLimitALT;

        [FieldFixedLength(5)]
        public string Msg1Number;

        [FieldFixedLength(5)]
        public string Msg2Number;

        [FieldFixedLength(5)]
        public string Msg3Number;

        [FieldFixedLength(216)]
        public string Trailer;

    }
}
