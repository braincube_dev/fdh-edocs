﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileLoaders.RecordClasses.Billing.AlphaBonusMerge
{
    [FixedLengthRecord()]

    class Sum4Record
    {
        [FieldFixedLength(7)]
        public string SerialNo;

        [FieldFixedLength(2)]
        public string ProductCodeKey;

        [FieldFixedLength(2)]
        public string RetainFlag;

        [FieldFixedLength(1)]
        public string StuffFlag;

        [FieldFixedLength(4)]
        public string BreakCode;

        [FieldFixedLength(20)]
        public string AddressKey;

        [FieldFixedLength(16)]
        public string AccountNo;

        [FieldFixedLength(4)]
        public string StatementNo;

        [FieldFixedLength(4)]
        public string LineNo;

        [FieldFixedLength(4)]
        public string LineID;

        [FieldFixedLength(2)]
        public string ProductCode;

        [FieldFixedLength(5)]
        public string Msg4Number;

        [FieldFixedLength(5)]
        public string Msg5Number;

        [FieldFixedLength(5)]
        public string Msg6Number;

        [FieldFixedLength(5)]
        public string Msg7Number;

        [FieldFixedLength(5)]
        public string Msg8Number;

        [FieldFixedLength(1)]
        public string ZapFlag;

        [FieldFixedLength(1)]
        public string CardFlag;

        [FieldFixedLength(1)]
        public string StandingOrder;

        [FieldFixedLength(5)]
        public string BranchCode;

        [FieldFixedLength(5)]
        public string PlstNoOfCards;

        [FieldFixedLength(1)]
        public string CutxIndicator;

        [FieldFixedLength(7)]
        public string SourceCode;

        [FieldFixedLength(5)]
        public string SourcePos;

        [FieldFixedLength(1)]
        public string AcountStatus1;

        [FieldFixedLength(1)]
        public string AccountStatus2;

        [FieldFixedLength(5)]
        public string PCurrency;

        [FieldFixedLength(1)]
        public string CardType;

        [FieldFixedLength(16)]
        public string TotalDebits;

        [FieldFixedLength(16)]
        public string TotalCredits;

        [FieldFixedLength(16)]
        public string ExtraAmount;

        [FieldFixedLength(16)]
        public string TotalDebitsALT;

        [FieldFixedLength(16)]
        public string TotalCreditsALT;

        [FieldFixedLength(16)]
        public string ExtraAmountALT;

        [FieldFixedLength(18)]
        public string BankAccountNo;

        [FieldFixedLength(1)]
        public string LanguageFlag;

        [FieldFixedLength(26)]
        public string EmbsMainName;

        [FieldFixedLength(4)]
        public string ExpirationDateYYYY;

        [FieldFixedLength(2)]
        public string ExpirationDateMM;

        [FieldFixedLength(10)]
        public string CustomerCode;

        [FieldFixedLength(5)]
        public string ProgramSelection;

        [FieldFixedLength(10)]
        public string IntlCustomerCode;

        [FieldFixedLength(203)]
        public string Trailer;
    }
}
