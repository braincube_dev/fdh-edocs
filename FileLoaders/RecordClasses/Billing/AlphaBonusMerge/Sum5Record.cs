﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileLoaders.RecordClasses.Billing.AlphaBonusMerge
{
    [FixedLengthRecord()]

    class Sum5Record
    {
        [FieldFixedLength(7)]
        public string SerialNo;

        [FieldFixedLength(2)]
        public string ProductCodeKey;

        [FieldFixedLength(2)]
        public string RetainFlag;

        [FieldFixedLength(1)]
        public string StuffFlag;

        [FieldFixedLength(4)]
        public string BreakCode;

        [FieldFixedLength(20)]
        public string AddressKey;

        [FieldFixedLength(16)]
        public string AccountNo;

        [FieldFixedLength(4)]
        public string StatementNo;

        [FieldFixedLength(4)]
        public string LineNo;

        [FieldFixedLength(4)]
        public string LineID;

        [FieldFixedLength(2)]
        public string ProductCode;

        [FieldFixedLength(16)]
        public string CashBalance;

        [FieldFixedLength(16)]
        public string CashBalanceALT;

        [FieldFixedLength(1)]
        public string PrintAllowFlag;

        [FieldFixedLength(2)]
        public string Segment;

        [FieldFixedLength(20)]
        public string AitSurname;

        [FieldFixedLength(10)]
        public string Filler;

        [FieldFixedLength(20)]
        public string OldAccount;

        [FieldFixedLength(15)]
        public string ProductLit;

        [FieldFixedLength(90)]
        public string MsgTableO;

        [FieldFixedLength(20)]
        public string AitName;

        [FieldFixedLength(15)]
        public string ReferenceNo;

        [FieldFixedLength(4)]
        public string OpenedDateYYYY;

        [FieldFixedLength(2)]
        public string OpenedDateMM;

        [FieldFixedLength(2)]
        public string OpenedDateDD;

        [FieldFixedLength(10)]
        public string FormName;

        [FieldFixedLength(1)]
        public string FormType;

        [FieldFixedLength(1)]
        public string FormPage;

        [FieldFixedLength(189)]
        public string Trailer;
    }
}
