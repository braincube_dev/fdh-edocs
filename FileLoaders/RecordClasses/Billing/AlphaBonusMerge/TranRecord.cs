﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileLoaders.RecordClasses.Billing.AlphaBonusMerge
{
    [FixedLengthRecord()]

    class TranRecord
    {
        [FieldFixedLength(7)]
        public string SerialNo;

        [FieldFixedLength(2)]
        public string ProductCodeKey;

        [FieldFixedLength(2)]
        public string RetainFlag;

        [FieldFixedLength(1)]
        public string StuffFlag;

        [FieldFixedLength(4)]
        public string BreakCode;

        [FieldFixedLength(20)]
        public string AddressKey;

        [FieldFixedLength(16)]
        public string AccountNo;

        [FieldFixedLength(4)]
        public string StatementNo;

        [FieldFixedLength(4)]
        public string LineNo;

        [FieldFixedLength(4)]
        public string LineID;

        [FieldFixedLength(2)]
        public string ProductCode;

        [FieldFixedLength(4)]
        public string TrxDateYYYY;

        [FieldFixedLength(2)]
        public string TrxDateMM;

        [FieldFixedLength(2)]
        public string TrxDateDD;

        [FieldFixedLength(4)]
        public string ProcDateYYYY;

        [FieldFixedLength(2)]
        public string ProcDateMM;

        [FieldFixedLength(2)]
        public string ProcDateDD;

        [FieldFixedLength(8)]
        public string TrxDeltaRef;

        [FieldFixedLength(36)]
        public string TrxDescription;

        [FieldFixedLength(16)]
        public string TrxAmount;

        [FieldFixedLength(16)]
        public string TrxAmountALT;

        [FieldFixedLength(19)]
        public string TrxForeignAmt;

        [FieldFixedLength(4)]
        public string TrxCurrencyCode;

        [FieldFixedLength(2)]
        public string TrxInfo;

        [FieldFixedLength(3)]
        public string TrxCode;

        [FieldFixedLength(4)]
        public string TrxMerchantCode;

        [FieldFixedLength(7)]
        public string AddonSuffix;

        [FieldFixedLength(6)]
        public string Reward;

        [FieldFixedLength(6)]
        public string Reward1;

        [FieldFixedLength(11)]
        public string RewardValue;

        [FieldFixedLength(30)]
        public string TrxDescription2;

        [FieldFixedLength(16)]
        public string TrxFlexAmt;

        [FieldFixedLength(2)]
        public string TrxFlexInst1;

        [FieldFixedLength(2)]
        public string TrxFlexInst2;

        [FieldFixedLength(2)]
        public string TrxFlexID;

        [FieldFixedLength(5)]
        public string TrxDescInst;

        [FieldFixedLength(13)]
        public string TrxPrcntAmt;

        [FieldFixedLength(13)]
        public string TrxPrcnt;

        [FieldFixedLength(13)]
        public string TotalCashBack;

        [FieldFixedLength(6)]
        public string TrxExchangeRate;

        [FieldFixedLength(3)]
        public string TrxExchangeLit;

        [FieldFixedLength(3)]
        public string MidCurrency;

        [FieldFixedLength(3)]
        public string MidCurrencyLit;

        [FieldFixedLength(3)]
        public string BillingCurrencyLit;

        [FieldFixedLength(18)]
        public string ExchangeRate;

        [FieldFixedLength(18)]
        public string Temp1ExchRate;

        [FieldFixedLength(18)]
        public string Temp2ExchRate;

        [FieldFixedLength(112)]
        public string Trailer;

    }
}
