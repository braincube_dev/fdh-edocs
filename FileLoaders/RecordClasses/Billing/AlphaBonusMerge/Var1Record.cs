﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileLoaders.RecordClasses.Billing.AlphaBonusMerge
{
    [FixedLengthRecord()]

    class Var1Record
    {
        [FieldFixedLength(7)]
        public string SerialNo;

        [FieldFixedLength(2)]
        public string ProductCodeKey;

        [FieldFixedLength(2)]
        public string RetainFlag;

        [FieldFixedLength(1)]
        public string StuffFlag;

        [FieldFixedLength(4)]
        public string BreakCode;

        [FieldFixedLength(20)]
        public string AddressKey;

        [FieldFixedLength(16)]
        public string AccountNo;

        [FieldFixedLength(4)]
        public string StatementNo;

        [FieldFixedLength(4)]
        public string LineNo;

        [FieldFixedLength(4)]
        public string LineID;

        [FieldFixedLength(2)]
        public string ProductCode;

        [FieldFixedLength(10)]
        public string Var1;

        [FieldFixedLength(10)]
        public string Var2;

        [FieldFixedLength(10)]
        public string Var3;

        [FieldFixedLength(10)]
        public string Var4;

        [FieldFixedLength(10)]
        public string Var5;

        [FieldFixedLength(10)]
        public string Var6;

        [FieldFixedLength(10)]
        public string Var7;

        [FieldFixedLength(10)]
        public string Var8;

        [FieldFixedLength(10)]
        public string Var9;

        [FieldFixedLength(10)]
        public string Var10;

        [FieldFixedLength(10)]
        public string Var11;

        [FieldFixedLength(10)]
        public string Var12;

        [FieldFixedLength(10)]
        public string Var13;

        [FieldFixedLength(10)]
        public string Var14;

        [FieldFixedLength(10)]
        public string Var15;

        [FieldFixedLength(10)]
        public string Var16;

        [FieldFixedLength(10)]
        public string Var17;

        [FieldFixedLength(10)]
        public string Var18;

        [FieldFixedLength(10)]
        public string Var19;

        [FieldFixedLength(10)]
        public string Var20;

        [FieldFixedLength(234)]
        public string Trailer;
    }
}
