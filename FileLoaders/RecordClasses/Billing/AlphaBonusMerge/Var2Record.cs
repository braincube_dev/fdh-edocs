﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileLoaders.RecordClasses.Billing.AlphaBonusMerge
{
    [FixedLengthRecord()]

    class Var2Record
    {
        [FieldFixedLength(7)]
        public string SerialNo;

        [FieldFixedLength(2)]
        public string ProductCodeKey;

        [FieldFixedLength(2)]
        public string RetainFlag;

        [FieldFixedLength(1)]
        public string StuffFlag;

        [FieldFixedLength(4)]
        public string BreakCode;

        [FieldFixedLength(20)]
        public string AddressKey;

        [FieldFixedLength(16)]
        public string AccountNo;

        [FieldFixedLength(4)]
        public string StatementNo;

        [FieldFixedLength(4)]
        public string LineNo;

        [FieldFixedLength(4)]
        public string LineID;

        [FieldFixedLength(2)]
        public string ProductCode;

        [FieldFixedLength(10)]
        public string Var21;

        [FieldFixedLength(10)]
        public string Var22;

        [FieldFixedLength(10)]
        public string Var23;

        [FieldFixedLength(10)]
        public string Var24;

        [FieldFixedLength(10)]
        public string Var25;

        [FieldFixedLength(10)]
        public string Var26;

        [FieldFixedLength(10)]
        public string Var27;

        [FieldFixedLength(10)]
        public string Var28;

        [FieldFixedLength(10)]
        public string Var29;

        [FieldFixedLength(10)]
        public string Var30;

        [FieldFixedLength(10)]
        public string Var31;

        [FieldFixedLength(10)]
        public string Var32;

        [FieldFixedLength(10)]
        public string Var33;

        [FieldFixedLength(10)]
        public string Var34;

        [FieldFixedLength(10)]
        public string Var35;

        [FieldFixedLength(10)]
        public string Var36;

        [FieldFixedLength(10)]
        public string Var37;

        [FieldFixedLength(10)]
        public string Var38;

        [FieldFixedLength(10)]
        public string Var39;

        [FieldFixedLength(10)]
        public string Var40;

        [FieldFixedLength(234)]
        public string Trailer;

    }
}
