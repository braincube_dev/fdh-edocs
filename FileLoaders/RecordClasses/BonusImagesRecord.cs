﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileLoaders.RecordClasses
{
    [DelimitedRecord(";")]

    class BonusImagesRecord
    {
        public string JobID;

        public string ImageType;

        public string ImageName;

        public string EmptyTrailer;
    }
}
