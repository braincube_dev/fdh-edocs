﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileLoaders.RecordClasses
{
    [DelimitedRecord(";")]

    class CorporateNamesRecord
    {
        public string CorporateCode;

        public string CorporateName;

        public string Weight;
    }
}
