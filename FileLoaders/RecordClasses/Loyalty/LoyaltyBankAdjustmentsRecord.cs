﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileLoaders.Loyalty
{
    [FixedLengthRecord()]
    public class LoyaltyBankAdjustmentsRecord
    {
        [FieldFixedLength(8)]
        public string RecordNumber;

        [FieldFixedLength(1)]
        public string RecordType;

        [FieldFixedLength(2)]
        public string BkAdjustmentRecordNumber;

        [FieldFixedLength(19)]
        public string PAN;

        [FieldFixedLength(10)]
        public string PointsEarned;

        [FieldFixedLength(8)]
        public string CorporateNumber;

        [FieldFixedLength(40)]
        public string CorporateName;

        [FieldFixedLength(113)]
        public string Trailer;
    }
}
