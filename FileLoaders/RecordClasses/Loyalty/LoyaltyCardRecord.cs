﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileLoaders.Loyalty
{
    [FixedLengthRecord()]
    public class LoyaltyCardRecord
    {
        [FieldFixedLength(8)]
        public string RecordNumber;

        [FieldFixedLength(1)]
        public string RecordType;

        [FieldFixedLength(19)]
        public string PAN;

        [FieldFixedLength(3)]
        public string TemplateNumber;

        [FieldFixedLength(3)]
        public string LetterID;

        [FieldFixedLength(4)]
        public string InsertType;

        [FieldFixedLength(11)]
        public string PrevTpts;

        [FieldFixedLength(10)]
        public string TEpts;

        [FieldFixedLength(10)]
        public string TRpts;

        [FieldFixedLength(11)]
        public string NewTpts;

        [FieldFixedLength(10)]
        public string ExtraPoints;

        [FieldFixedLength(10)]
        public string PointsExpired;

        [FieldFixedLength(10)]
        public string PointsToBeExpired;

        [FieldFixedLength(11)]
        public string BonusPtsAdjustment;

        [FieldFixedLength(32)]
        public string ListOffers;

        [FieldFixedLength(3)]
        public string CorporateData;

        [FieldFixedLength(2)]
        public string ExtraPointsData;

        [FieldFixedLength(21)]
        public string XLSaccountID;

        [FieldFixedLength(14)]
        public string Trailer;
    }
}
