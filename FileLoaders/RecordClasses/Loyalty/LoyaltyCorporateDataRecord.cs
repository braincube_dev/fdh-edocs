﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileLoaders.Loyalty
{
    [FixedLengthRecord()]
    public class LoyaltyCorporateDataRecord
    {
        [FieldFixedLength(8)]
        public string RecordNumber;

        [FieldFixedLength(1)]
        public string RecordType;

        [FieldFixedLength(3)]
        public string CorporateRecordNumber;

        [FieldFixedLength(8)]
        public string CorporateNumber;

        [FieldFixedLength(40)]
        public string CorporateName;

        [FieldFixedLength(10)]
        public string PostringsEarned;

        [FieldFixedLength(123)]
        public string Trailer;
    }
}
