﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileLoaders.Loyalty
{
    [FixedLengthRecord()]
    public class LoyaltyExtraPointsDataRecord
    {
        [FieldFixedLength(8)]
        public string RecordNumber;

        [FieldFixedLength(1)]
        public string RecordType;

        [FieldFixedLength(2)]
        public string ExtraRecordNumber;

        [FieldFixedLength(24)]
        public string CampaignName;

        [FieldFixedLength(10)]
        public string ExtraPoints;

        [FieldFixedLength(148)]
        public string Trailer;

    }
}
