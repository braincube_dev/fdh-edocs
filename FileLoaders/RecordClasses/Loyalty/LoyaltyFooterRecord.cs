﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileLoaders.Loyalty
{
    [FixedLengthRecord()]
    public class LoyaltyFooterRecord
    {
        [FieldFixedLength(8)]
        public string RecordNumber;

        [FieldFixedLength(1)]
        public string RecordType;

        [FieldFixedLength(8)]
        public string RecordCount;

        [FieldFixedLength(176)]
        public string Trailer;
    }
}
