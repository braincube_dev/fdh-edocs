﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileLoaders.Loyalty
{
    [FixedLengthRecord()]
    public class LoyaltyHeaderRecord
    {
        [FieldFixedLength(8)]
        public string RecordNumber;

        [FieldFixedLength(1)]
        public string RecordType;

        [FieldFixedLength(1)]
        public string Version;

        [FieldFixedLength(14)]
        [FieldConverter(ConverterKind.Date, "yyyyMMddHHmmss")]
        public DateTime DateTime; 

        [FieldFixedLength(5)]
        public string FileType;

        [FieldFixedLength(5)]
        public int FSN;

        [FieldFixedLength(7)]
        public string IssuerFileID;

        [FieldFixedLength(20)]
        public string StatementFileLinkReference;

        [FieldFixedLength(132)]
        public string trailer;

       
    }
}
