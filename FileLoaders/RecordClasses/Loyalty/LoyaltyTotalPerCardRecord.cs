﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileLoaders.Loyalty
{
    [FixedLengthRecord()]

    public class LoyaltyTotalPerCardRecord
    {
        [FieldFixedLength(8)]
        public string RecordNumber;

        [FieldFixedLength(1)]
        public string RecordType;

        [FieldFixedLength(2)]
        public string TotalPerCardRecordNumber;

        [FieldFixedLength(19)]
        public string PAN;

        [FieldFixedLength(21)]
        public string UAID;

        [FieldFixedLength(10)]
        public string PointsEarned;

        [FieldFixedLength(10)]
        public string PointsRedeemed;

        [FieldFixedLength(1)]
        public string AccountActive;

        [FieldFixedLength(2)]
        public string NumberOfBankAdjustmentRecord;

        [FieldFixedLength(119)]
        public string Trailer;

    }
}
