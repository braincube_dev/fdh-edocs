﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileLoaders.RecordClasses
{
    [DelimitedRecord(";")]

    class MenuProductsRecord
    {
        public string Menu;

        public string MenuID;

        public string ProductDescription;

        public string ProductCode;

        public string EmptyTrailer;

    }
}
