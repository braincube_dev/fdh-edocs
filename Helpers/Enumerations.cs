﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Helpers
{
    public enum SchemeToProcessType
    {
        Bonus = 1,
        Affinity = 2,
        Diners = 3,
    }
}
