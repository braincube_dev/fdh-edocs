﻿namespace Helpers
{
    public class HandlerLogItem
    {
        public string Message { get; set; }
        public int StepID { get; set; }
        public string StepName { get; set; }
    }

    public class HandlerInfo : HandlerLogItem
    {

    }

    public class HandlerWarning : HandlerLogItem
    {

    }

    public class HandlerError : HandlerLogItem
    {

    }
}
