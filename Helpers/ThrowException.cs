﻿using System;
using System.Collections.Generic;

namespace Helpers
{
    public static class ThrowException
    {
        public static void Throw(string exceptionCode)
        {
            throw new ApplicationException(exceptionCode);
        }

        public static void ProcessSettingsNotFound()
        {
            Throw("PROCESS_SETTING_NOT_FOUND");
        }

        public static void StepToDoNotExists(string stepName)
        {
            Throw($"REQUESTED_STEP_JOBTODO_NOT_EXIST_{stepName}");
        }

        public static void StepIdAlreadyExists()
        {
            Throw("STEP_SETTING_ID_ALREADY_EXISTS");
        }

        public static void FileNotExist()
        {
            Throw("FILE_NOT_EXISTS");
        }

        public static void FileIsAlreadyOpen(string fileName)
        {
            Throw($"FILE_IS_OPEN_{fileName}");
        }

        public static void FileNotSupported()
        {
            Throw("NOT_SUPPORTED");
        }

        public static void DirectoryNotExist()
        {
            Throw("DIRECTORY_NOT_EXISTS");
        }

        public static void StrategiesNotEqualToFolders()
        {
            Throw("STRATEGIES_NOT_EQUAL_TO_FOLDERS");
        }

        public static void CountersAreZero(IEnumerable<string> counters)
        {
            Throw($"COUNTERS_ARE_ZERO {string.Join(", ", counters)}");
        }

        public static void CountersAreNotEqual()
        {
            Throw($"COUNTERS_ARE_NOT_EQUAL");
        }
    }
}
