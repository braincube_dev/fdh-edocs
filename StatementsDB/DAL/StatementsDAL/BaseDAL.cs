﻿using DapperExtensions;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace StatementsDB
{
    abstract public class BaseDAL<T> where T : class
    {
        string connectionString;
        protected BaseDAL()
        {
            connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
        }

        public int Insert(T obj)
        {
            using (IDbConnection _db = new SqlConnection(connectionString))
            {
                return _db.Insert(obj);
            }
        }

        public bool Update(T obj)
        {
            using (IDbConnection _db = new SqlConnection(connectionString))
            {
                return _db.Update(obj);
            }
        }

        public bool Delete(T obj)
        {
            using (IDbConnection _db = new SqlConnection(connectionString))
            {
                return _db.Delete(obj);
            }
        }

        public T GetByID(int id)
        {
            using (IDbConnection _db = new SqlConnection(connectionString))
            {
                return _db.Get<T>(id);
            }
        }

        public List<T> GetAll()
        {
            using (IDbConnection _db = new SqlConnection(connectionString))
            {
                return _db.GetList<T>()?.ToList() ?? new List<T>();
            }
        }

        public List<T> GetListByPredicate(PredicateGroup group = null)
        {
            using (IDbConnection _db = new SqlConnection(connectionString))
            {
                return group == null ? _db.GetList<T>()?.ToList() ?? new List<T>() : _db.GetList<T>(group)?.ToList() ?? new List<T>();
            }
        }
    }
}
