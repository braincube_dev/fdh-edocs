IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'ML01A')
BEGIN
	DROP TABLE [EDOC].[ML01A]; PRINT 'ML01A DELETED'
END

--ML01A--
CREATE TABLE [EDOC].[ML01A](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
        [FIELDH] [nvarchar](255),
        [FIELDI] [nvarchar](255),
        [FIELDJ] [nvarchar](255),
        [FIELDK] [nvarchar](255),
        [FIELDL] [nvarchar](255),
        [FIELDM] [nvarchar](255),
        [FIELDN] [nvarchar](255),
        [FIELDO] [nvarchar](255),
        [FIELDP] [nvarchar](255),
        [FIELDQ] [nvarchar](255),
        [FIELDR] [nvarchar](255),
        [FIELDS] [nvarchar](255),
        [FIELDT] [nvarchar](255),
        [FIELDU] [nvarchar](255),
        [FIELDV] [nvarchar](255),
        [FIELDW] [nvarchar](255),
        [FIELDX] [nvarchar](255),
CONSTRAINT [PK_ML01A] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [ML01A] CREATED'
-----END ML01A--

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'ML01B')
BEGIN
	DROP TABLE [EDOC].[ML01B]; PRINT 'ML01B DELETED'
END

--ML01B--
CREATE TABLE [EDOC].[ML01B](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
CONSTRAINT [PK_ML01B] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [ML01B] CREATED'
-----END ML01B--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'ML02A')
BEGIN
	DROP TABLE [EDOC].[ML02A]; PRINT 'ML02A DELETED'
END

--ML02A--
CREATE TABLE [EDOC].[ML02A](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
        [FIELDH] [nvarchar](255),
        [FIELDI] [nvarchar](255),
        [FIELDJ] [nvarchar](255),
        [FIELDK] [nvarchar](255),
        [FIELDL] [nvarchar](255),
        [FIELDM] [nvarchar](255),
        [FIELDN] [nvarchar](255),
        [FIELDO] [nvarchar](255),
        [FIELDP] [nvarchar](255),
CONSTRAINT [PK_ML02A] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [ML02A] CREATED'
-----END ML02A--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'ML02A')
BEGIN
	DROP TABLE [EDOC].[ML03A]; PRINT 'ML03A DELETED'
END

--ML03A--
CREATE TABLE [EDOC].[ML03A](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
        [FIELDH] [nvarchar](255),
        [FIELDI] [nvarchar](255),
        [FIELDJ] [nvarchar](255),
        [FIELDK] [nvarchar](255),
        [FIELDL] [nvarchar](255),
        [FIELDM] [nvarchar](255),
        [FIELDN] [nvarchar](255),
        [FIELDO] [nvarchar](255),
        [FIELDP] [nvarchar](255),
        [FIELDQ] [nvarchar](255),
        [FIELDR] [nvarchar](255),
        [FIELDS] [nvarchar](255),
        [FIELDT] [nvarchar](255),
        [FIELDU] [nvarchar](255),
        [FIELDV] [nvarchar](255),
        [FIELDW] [nvarchar](255),
        [FIELDX] [nvarchar](255),
		[FIELDY] [nvarchar](255),
        [FIELDZ] [nvarchar](255),
        [FIELDAA] [nvarchar](255),
        [FIELDAB] [nvarchar](255),
        [FIELDAC] [nvarchar](255),
        [FIELDAE] [nvarchar](255),
        [FIELDAF] [nvarchar](255),
		[FIELDAG] [nvarchar](255),
        [FIELDAH] [nvarchar](255),
CONSTRAINT [PK_ML03A] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [ML02A] CREATED'
-----END ML03A--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'ML03B')
BEGIN
	DROP TABLE [EDOC].[ML03B]; PRINT 'ML03B DELETED'
END

--ML03B--
CREATE TABLE [EDOC].[ML03B](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
CONSTRAINT [PK_ML03B] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [ML03B] CREATED'
-----END ML03B--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'ML04A')
BEGIN
	DROP TABLE [EDOC].[ML04A]; PRINT 'ML04A DELETED'
END

--ML04A--
CREATE TABLE [EDOC].[ML04A](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
        [FIELDH] [nvarchar](255),
        [FIELDI] [nvarchar](255),
        [FIELDJ] [nvarchar](255),
        [FIELDK] [nvarchar](255),
        [FIELDL] [nvarchar](255),
        [FIELDM] [nvarchar](255),
        [FIELDN] [nvarchar](255),
        [FIELDO] [nvarchar](255),
        [FIELDP] [nvarchar](255),
        [FIELDQ] [nvarchar](255),
        [FIELDR] [nvarchar](255),
        [FIELDS] [nvarchar](255),
        [FIELDT] [nvarchar](255),
        [FIELDU] [nvarchar](255),
        [FIELDV] [nvarchar](255),
        [FIELDW] [nvarchar](255),
        [FIELDX] [nvarchar](255),
CONSTRAINT [PK_ML04A] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [ML04A] CREATED'
-----END ML04A--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'ML04B')
BEGIN
	DROP TABLE [EDOC].[ML04B]; PRINT 'ML04B DELETED'
END

--ML04B--
CREATE TABLE [EDOC].[ML04B](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
CONSTRAINT [PK_ML04B] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [ML04B] CREATED'
-----END ML04B--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'ML05A')
BEGIN
	DROP TABLE [EDOC].[ML05A]; PRINT 'ML05A DELETED'
END

--ML05A--
CREATE TABLE [EDOC].[ML05A](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
        [FIELDH] [nvarchar](255),
        [FIELDI] [nvarchar](255),
        [FIELDJ] [nvarchar](255),
        [FIELDK] [nvarchar](255),
        [FIELDL] [nvarchar](255),
        [FIELDM] [nvarchar](255),
        [FIELDN] [nvarchar](255),
        [FIELDO] [nvarchar](255),
        [FIELDP] [nvarchar](255),
        [FIELDQ] [nvarchar](255),
        [FIELDR] [nvarchar](255),
        [FIELDS] [nvarchar](255),
        [FIELDT] [nvarchar](255),
        [FIELDU] [nvarchar](255),
        [FIELDV] [nvarchar](255),
        [FIELDW] [nvarchar](255),
        [FIELDX] [nvarchar](255),
		[FIELDY] [nvarchar](255),
CONSTRAINT [PK_ML05A] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [ML05A] CREATED'
-----END ML05A--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'ML05B')
BEGIN
	DROP TABLE [EDOC].[ML05B]; PRINT 'ML05B DELETED'
END

--ML05B--
CREATE TABLE [EDOC].[ML05B](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),        
CONSTRAINT [PK_ML05B] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [ML05B] CREATED'
-----END ML05B--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'SB01A')
BEGIN
	DROP TABLE [EDOC].[SB01A]; PRINT 'SB01A DELETED'
END

--SB01A--
CREATE TABLE [EDOC].[SB01A](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
        [FIELDH] [nvarchar](255),
        [FIELDI] [nvarchar](255),
        [FIELDJ] [nvarchar](255),
        [FIELDK] [nvarchar](255),
        [FIELDL] [nvarchar](255),
        [FIELDM] [nvarchar](255),
        [FIELDN] [nvarchar](255),
        [FIELDO] [nvarchar](255),
        [FIELDP] [nvarchar](255),
        [FIELDQ] [nvarchar](255),
        [FIELDR] [nvarchar](255),
        [FIELDS] [nvarchar](255),
        [FIELDT] [nvarchar](255),
        [FIELDU] [nvarchar](255),
        [FIELDV] [nvarchar](255),
        [FIELDW] [nvarchar](255),
        [FIELDX] [nvarchar](255),
		[FIELDY] [nvarchar](255),
        [FIELDZ] [nvarchar](255),
        [FIELDAA] [nvarchar](255),
        [FIELDAB] [nvarchar](255),
        [FIELDAC] [nvarchar](255),
		[FIELDAD] [nvarchar](255),
        [FIELDAE] [nvarchar](255),
CONSTRAINT [PK_SB01A] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [SB01A] CREATED'
-----END SB01A--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'SB01B')
BEGIN
	DROP TABLE [EDOC].[SB01B]; PRINT 'SB01B DELETED'
END

--SB01B--
CREATE TABLE [EDOC].[SB01B](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),        
CONSTRAINT [PK_SB01B] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [SB01B] CREATED'
-----END SB01B--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'SB02A')
BEGIN
	DROP TABLE [EDOC].[SB02A]; PRINT 'SB02A DELETED'
END

--SB02A--
CREATE TABLE [EDOC].[SB02A](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
        [FIELDH] [nvarchar](255),
        [FIELDI] [nvarchar](255),
        [FIELDJ] [nvarchar](255),
        [FIELDK] [nvarchar](255),
        [FIELDL] [nvarchar](255),
        [FIELDM] [nvarchar](255),
        [FIELDN] [nvarchar](255),
        [FIELDO] [nvarchar](255),
        [FIELDP] [nvarchar](255),
        [FIELDQ] [nvarchar](255),
        [FIELDR] [nvarchar](255),
        [FIELDS] [nvarchar](255),
        [FIELDT] [nvarchar](255),
        [FIELDU] [nvarchar](255),
        [FIELDV] [nvarchar](255),
        [FIELDW] [nvarchar](255),
        [FIELDX] [nvarchar](255),
        [FIELDY] [nvarchar](255),
        [FIELDZ] [nvarchar](255),
        [FIELDAA] [nvarchar](255),
        [FIELDAB] [nvarchar](255),
        [FIELDAC] [nvarchar](255),
CONSTRAINT [PK_SB02A] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [SB02A] CREATED'
-----END SB02A--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'SB02B')
BEGIN
	DROP TABLE [EDOC].[SB02B]; PRINT 'SB02B DELETED'
END

--SB02B--
CREATE TABLE [EDOC].[SB02B](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
CONSTRAINT [PK_SB02B] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [SB02B] CREATED'
-----END SB02B--

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'SB03A')
BEGIN
	DROP TABLE [EDOC].[SB03A]; PRINT 'SB03A DELETED'
END

--SB03A--
CREATE TABLE [EDOC].[SB03A](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
        [FIELDH] [nvarchar](255),
        [FIELDI] [nvarchar](255),
        [FIELDJ] [nvarchar](255),
        [FIELDK] [nvarchar](255),
        [FIELDL] [nvarchar](255),
        [FIELDM] [nvarchar](255),
        [FIELDN] [nvarchar](255),
        [FIELDO] [nvarchar](255),
        [FIELDP] [nvarchar](255),
        [FIELDQ] [nvarchar](255),
        [FIELDR] [nvarchar](255),
        [FIELDS] [nvarchar](255),
        [FIELDT] [nvarchar](255),
        [FIELDU] [nvarchar](255),
        [FIELDV] [nvarchar](255),
        [FIELDW] [nvarchar](255),
        [FIELDX] [nvarchar](255),
        [FIELDY] [nvarchar](255),
        [FIELDZ] [nvarchar](255),
        [FIELDAA] [nvarchar](255),
CONSTRAINT [PK_SB03A] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [SB03A] CREATED'
-----END SB03A--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'SB03B')
BEGIN
	DROP TABLE [EDOC].[SB03B]; PRINT 'SB03B DELETED'
END

--SB03B--
CREATE TABLE [EDOC].[SB03B](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),

CONSTRAINT [PK_SB03B] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [SB03B] CREATED'
-----END SB03B--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'SB04A')
BEGIN
	DROP TABLE [EDOC].[SB04A]; PRINT 'SB04A DELETED'
END

--SB04A--
CREATE TABLE [EDOC].[SB04A](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
        [FIELDH] [nvarchar](255),
        [FIELDI] [nvarchar](255),
        [FIELDJ] [nvarchar](255),
        [FIELDK] [nvarchar](255),
        [FIELDL] [nvarchar](255),
        [FIELDM] [nvarchar](255),
        [FIELDN] [nvarchar](255),
        [FIELDO] [nvarchar](255),
        [FIELDP] [nvarchar](255),
        [FIELDQ] [nvarchar](255),
        [FIELDR] [nvarchar](255),
        [FIELDS] [nvarchar](255),
        [FIELDT] [nvarchar](255),
        [FIELDU] [nvarchar](255),
        [FIELDV] [nvarchar](255),
        [FIELDW] [nvarchar](255),
        [FIELDX] [nvarchar](255),
        [FIELDY] [nvarchar](255),
        [FIELDZ] [nvarchar](255),
        [FIELDAA] [nvarchar](255),
        [FIELDAB] [nvarchar](255),
        [FIELDAC] [nvarchar](255),
		[FIELDAD] [nvarchar](255),
        [FIELDAE] [nvarchar](255),
		[FIELDAF] [nvarchar](255),
        [FIELDAG] [nvarchar](255),
        [FIELDAH] [nvarchar](255),
		[FIELDAI] [nvarchar](255),
        [FIELDAJ] [nvarchar](255),
CONSTRAINT [PK_SB04A] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [SB04A] CREATED'
-----END SB04A--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'SB04B')
BEGIN
	DROP TABLE [EDOC].[SB04B]; PRINT 'SB04B DELETED'
END

--SB04B--
CREATE TABLE [EDOC].[SB04B](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
CONSTRAINT [PK_SB04B] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [SB04B] CREATED'
-----END SB04B--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'SB05A')
BEGIN
	DROP TABLE [EDOC].[SB05A]; PRINT 'SB05A DELETED'
END

--SB05A--
CREATE TABLE [EDOC].[SB05A](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
        [FIELDH] [nvarchar](255),
        [FIELDI] [nvarchar](255),
        [FIELDJ] [nvarchar](255),
        [FIELDK] [nvarchar](255),
        [FIELDL] [nvarchar](255),
        [FIELDM] [nvarchar](255),
        [FIELDN] [nvarchar](255),
        [FIELDO] [nvarchar](255),
        [FIELDP] [nvarchar](255),
        [FIELDQ] [nvarchar](255),
        [FIELDR] [nvarchar](255),
        [FIELDS] [nvarchar](255),
        [FIELDT] [nvarchar](255),
        [FIELDU] [nvarchar](255),
        [FIELDV] [nvarchar](255),
        [FIELDW] [nvarchar](255),
        [FIELDX] [nvarchar](255),
        [FIELDY] [nvarchar](255),
        [FIELDZ] [nvarchar](255),
        [FIELDAA] [nvarchar](255),
        [FIELDAB] [nvarchar](255),
        [FIELDAC] [nvarchar](255),
		[FIELDAD] [nvarchar](255),
        [FIELDAE] [nvarchar](255),
		[FIELDAF] [nvarchar](255),
		[FIELDAG] [nvarchar](255),
        [FIELDAH] [nvarchar](255),
CONSTRAINT [PK_SB05A] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [SB05A] CREATED'
-----END SB05A--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'SB05B')
BEGIN
	DROP TABLE [EDOC].[SB05B]; PRINT 'SB05B DELETED'
END

--SB05B--
CREATE TABLE [EDOC].[SB05B](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
CONSTRAINT [PK_SB05B] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [SB05B] CREATED'
-----END SB05B--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'SB06A')
BEGIN
	DROP TABLE [EDOC].[SB06A]; PRINT 'SB06A DELETED'
END

--SB06A--
CREATE TABLE [EDOC].[SB06A](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
        [FIELDH] [nvarchar](255),
        [FIELDI] [nvarchar](255),
        [FIELDJ] [nvarchar](255),
        [FIELDK] [nvarchar](255),
        [FIELDL] [nvarchar](255),
        [FIELDM] [nvarchar](255),
        [FIELDN] [nvarchar](255),
        [FIELDO] [nvarchar](255),
        [FIELDP] [nvarchar](255),
        [FIELDQ] [nvarchar](255),
        [FIELDR] [nvarchar](255),
        [FIELDS] [nvarchar](255),
        [FIELDT] [nvarchar](255),
        [FIELDU] [nvarchar](255),
        [FIELDV] [nvarchar](255),
        [FIELDW] [nvarchar](255),
        [FIELDX] [nvarchar](255),
        [FIELDY] [nvarchar](255),
        [FIELDZ] [nvarchar](255),
        [FIELDAA] [nvarchar](255),
        [FIELDAB] [nvarchar](255),
        [FIELDAC] [nvarchar](255),
		[FIELDAD] [nvarchar](255),
        [FIELDAE] [nvarchar](255),
		[FIELDAF] [nvarchar](255),
CONSTRAINT [PK_SB06A] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [SB06A] CREATED'
-----END SB06A--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'SB06B')
BEGIN
	DROP TABLE [EDOC].[SB06B]; PRINT 'SB06B DELETED'
END

--SB06B--
CREATE TABLE [EDOC].[SB06B](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
CONSTRAINT [PK_SB06B] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [SB06B] CREATED'
-----END SB06B--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'CMS01')
BEGIN
	DROP TABLE [EDOC].[CMS01]; PRINT 'CMS01 DELETED'
END

--CMS01--
CREATE TABLE [EDOC].[CMS01](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
        [FIELDH] [nvarchar](255),
        [FIELDI] [nvarchar](255),
        [FIELDJ] [nvarchar](255),
        [FIELDK] [nvarchar](255),
        [FIELDL] [nvarchar](255),
        [FIELDM] [nvarchar](255),
CONSTRAINT [PK_CMS01] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [CMS01] CREATED'
-----END CMS01--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'CMS02')
BEGIN
	DROP TABLE [EDOC].[CMS02]; PRINT 'CMS02 DELETED'
END

--CMS02--
CREATE TABLE [EDOC].[CMS02](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
        [FIELDH] [nvarchar](255),
        [FIELDI] [nvarchar](255),
        [FIELDJ] [nvarchar](255),
        [FIELDK] [nvarchar](255),
        [FIELDL] [nvarchar](255),
CONSTRAINT [PK_CMS02] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [CMS02] CREATED'
-----END CMS02--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'CMS03')
BEGIN
	DROP TABLE [EDOC].[CMS03]; PRINT 'CMS03 DELETED'
END

--CMS03--
CREATE TABLE [EDOC].[CMS03](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
        [FIELDH] [nvarchar](255),
        [FIELDI] [nvarchar](255),
        [FIELDJ] [nvarchar](255),
        [FIELDK] [nvarchar](255),
        [FIELDL] [nvarchar](255),
        [FIELDM] [nvarchar](255),
        [FIELDN] [nvarchar](255),
        [FIELDO] [nvarchar](255),
        [FIELDP] [nvarchar](255),
        [FIELDQ] [nvarchar](255),
CONSTRAINT [PK_CMS03] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [CMS03] CREATED'
-----END CMS03--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'CMS04')
BEGIN
	DROP TABLE [EDOC].[CMS04]; PRINT 'CMS04 DELETED'
END

--CMS04--
CREATE TABLE [EDOC].[CMS04](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
        [FIELDH] [nvarchar](255),
        [FIELDI] [nvarchar](255),
        [FIELDJ] [nvarchar](255),
        [FIELDK] [nvarchar](255),
        [FIELDL] [nvarchar](255),
        [FIELDM] [nvarchar](255),
        [FIELDN] [nvarchar](255),
CONSTRAINT [PK_CMS04] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [CMS04] CREATED'
-----END CMS04--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'CMS05')
BEGIN
	DROP TABLE [EDOC].[CMS05]; PRINT 'CMS05 DELETED'
END

--CMS05--
CREATE TABLE [EDOC].[CMS05](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
        [FIELDH] [nvarchar](255),
        [FIELDI] [nvarchar](255),
        [FIELDJ] [nvarchar](255),
        [FIELDK] [nvarchar](255),
        [FIELDL] [nvarchar](255),
CONSTRAINT [PK_CMS05] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [CMS05] CREATED'
-----END CMS05--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LMS01A')
BEGIN
	DROP TABLE [EDOC].[LMS01A]; PRINT 'LMS01A DELETED'
END

--LMS01A--
CREATE TABLE [EDOC].[LMS01A](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
        [FIELDH] [nvarchar](255),
        [FIELDI] [nvarchar](255),
        [FIELDJ] [nvarchar](255),
        [FIELDK] [nvarchar](255),
        [FIELDL] [nvarchar](255),
        [FIELDM] [nvarchar](255),
        [FIELDN] [nvarchar](255),
CONSTRAINT [PK_LMS01A] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [LMS01A] CREATED'
-----END LMS01A--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LMS01B')
BEGIN
	DROP TABLE [EDOC].[LMS01B]; PRINT 'LMS01B DELETED'
END

--LMS01B--
CREATE TABLE [EDOC].[LMS01B](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
CONSTRAINT [PK_LMS01B] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [LMS01B] CREATED'
-----END LMS01B--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LMS02A')
BEGIN
	DROP TABLE [EDOC].[LMS02A]; PRINT 'LMS02A DELETED'
END

--LMS02A--
CREATE TABLE [EDOC].[LMS02A](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
        [FIELDH] [nvarchar](255),
        [FIELDI] [nvarchar](255),
        [FIELDJ] [nvarchar](255),
        [FIELDK] [nvarchar](255),
        [FIELDL] [nvarchar](255),
        [FIELDM] [nvarchar](255),
        [FIELDN] [nvarchar](255),
        [FIELDO] [nvarchar](255),
CONSTRAINT [PK_LMS02A] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [LMS02A] CREATED'
-----END LMS02A--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LMS02B')
BEGIN
	DROP TABLE [EDOC].[LMS02B]; PRINT 'LMS02B DELETED'
END

--LMS02B--
CREATE TABLE [EDOC].[LMS02B](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
CONSTRAINT [PK_LMS02B] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [LMS02B] CREATED'
-----END LMS02B--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LMS03A')
BEGIN
	DROP TABLE [EDOC].[LMS03A]; PRINT 'LMS03A DELETED'
END

--LMS03A--
CREATE TABLE [EDOC].[LMS03A](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
        [FIELDH] [nvarchar](255),
        [FIELDI] [nvarchar](255),
        [FIELDJ] [nvarchar](255),
        [FIELDK] [nvarchar](255),
        [FIELDL] [nvarchar](255),
        [FIELDM] [nvarchar](255),
        [FIELDN] [nvarchar](255),
CONSTRAINT [PK_LMS03A] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [LMS03A] CREATED'
-----END LMS03A--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LMS03B')
BEGIN
	DROP TABLE [EDOC].[LMS03B]; PRINT 'LMS03B DELETED'
END

--LMS03B--
CREATE TABLE [EDOC].[LMS03B](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
CONSTRAINT [PK_LMS03B] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [LMS03B] CREATED'
-----END LMS03B--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LMS04A')
BEGIN
	DROP TABLE [EDOC].[LMS04A]; PRINT 'LMS04A DELETED'
END

--LMS04A--
CREATE TABLE [EDOC].[LMS04A](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
        [FIELDH] [nvarchar](255),
        [FIELDI] [nvarchar](255),
        [FIELDJ] [nvarchar](255),
        [FIELDK] [nvarchar](255),
        [FIELDL] [nvarchar](255),
        [FIELDM] [nvarchar](255),
        [FIELDN] [nvarchar](255),
        [FIELDO] [nvarchar](255),
        [FIELDP] [nvarchar](255),
CONSTRAINT [PK_LMS04A] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [LMS04A] CREATED'
-----END LMS04A--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LMS04B')
BEGIN
	DROP TABLE [EDOC].[LMS04B]; PRINT 'LMS04B DELETED'
END

--LMS04B--
CREATE TABLE [EDOC].[LMS04B](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
CONSTRAINT [PK_LMS04B] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [LMS04B] CREATED'
-----END LMS04B--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LMS05A')
BEGIN
	DROP TABLE [EDOC].[LMS05A]; PRINT 'LMS05A DELETED'
END

--LMS05A--
CREATE TABLE [EDOC].[LMS05A](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
        [FIELDH] [nvarchar](255),
        [FIELDI] [nvarchar](255),
        [FIELDJ] [nvarchar](255),
        [FIELDK] [nvarchar](255),
        [FIELDL] [nvarchar](255),
        [FIELDM] [nvarchar](255),
        [FIELDN] [nvarchar](255),
CONSTRAINT [PK_LMS05A] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [LMS05A] CREATED'
-----END LMS05A--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LMS05B')
BEGIN
	DROP TABLE [EDOC].[LMS05B]; PRINT 'LMS05B DELETED'
END

--LMS05B--
CREATE TABLE [EDOC].[LMS05B](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
CONSTRAINT [PK_LMS05B] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [LMS05B] CREATED'
-----END LMS05B--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LMS06A')
BEGIN
	DROP TABLE [EDOC].[LMS06A]; PRINT 'LMS06A DELETED'
END

--LMS06A--
CREATE TABLE [EDOC].[LMS06A](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
        [FIELDH] [nvarchar](255),
        [FIELDI] [nvarchar](255),
        [FIELDJ] [nvarchar](255),
        [FIELDK] [nvarchar](255),
        [FIELDL] [nvarchar](255),
        [FIELDM] [nvarchar](255),
        [FIELDN] [nvarchar](255),
        [FIELDO] [nvarchar](255),
CONSTRAINT [PK_LMS06A] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [LMS06A] CREATED'
-----END LMS06A--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LMS06B')
BEGIN
	DROP TABLE [EDOC].[LMS06B]; PRINT 'LMS06B DELETED'
END

--LMS06B--
CREATE TABLE [EDOC].[LMS06B](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
CONSTRAINT [PK_LMS06B] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [LMS06B] CREATED'
-----END LMS06B--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LMS07A')
BEGIN
	DROP TABLE [EDOC].[LMS07A]; PRINT 'LMS07A DELETED'
END

--LMS07A--
CREATE TABLE [EDOC].[LMS07A](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
        [FIELDH] [nvarchar](255),
        [FIELDI] [nvarchar](255),
        [FIELDJ] [nvarchar](255),
        [FIELDK] [nvarchar](255),
        [FIELDL] [nvarchar](255),
        [FIELDM] [nvarchar](255),
CONSTRAINT [PK_LMS07A] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [LMS07A] CREATED'
-----END LMS07A--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LMS07B')
BEGIN
	DROP TABLE [EDOC].[LMS07B]; PRINT 'LMS07B DELETED'
END

--LMS07B--
CREATE TABLE [EDOC].[LMS07B](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
CONSTRAINT [PK_LMS07B] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [LMS07B] CREATED'
-----END LMS07B--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LMS08A')
BEGIN
	DROP TABLE [EDOC].[LMS08A]; PRINT 'LMS08A DELETED'
END

--LMS08A--
CREATE TABLE [EDOC].[LMS08A](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
        [FIELDH] [nvarchar](255),
        [FIELDI] [nvarchar](255),
        [FIELDJ] [nvarchar](255),
        [FIELDK] [nvarchar](255),
        [FIELDL] [nvarchar](255),
        [FIELDM] [nvarchar](255),
        [FIELDN] [nvarchar](255),
        [FIELDO] [nvarchar](255),
CONSTRAINT [PK_LMS08A] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [LMS08A] CREATED'
-----END LMS08A--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LMS08B')
BEGIN
	DROP TABLE [EDOC].[LMS08B]; PRINT 'LMS08B DELETED'
END

--LMS08B--
CREATE TABLE [EDOC].[LMS08B](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
CONSTRAINT [PK_LMS08B] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [LMS08B] CREATED'
-----END LMS08B--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LMS09A')
BEGIN
	DROP TABLE [EDOC].[LMS09A]; PRINT 'LMS09A DELETED'
END

--LMS09A--
CREATE TABLE [EDOC].[LMS09A](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
        [FIELDH] [nvarchar](255),
        [FIELDI] [nvarchar](255),
        [FIELDJ] [nvarchar](255),
        [FIELDK] [nvarchar](255),
        [FIELDL] [nvarchar](255),
        [FIELDM] [nvarchar](255),
CONSTRAINT [PK_LMS09A] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [LMS09A] CREATED'
-----END LMS09A--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LMS09B')
BEGIN
	DROP TABLE [EDOC].[LMS09B]; PRINT 'LMS09B DELETED'
END
--LMS09B--
CREATE TABLE [EDOC].LMS09B(
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
CONSTRAINT [PK_LMS09B] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [LMS09B] CREATED'
-----END LMS09B--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LMS10A')
BEGIN
	DROP TABLE [EDOC].[LMS10A]; PRINT 'LMS10A DELETED'
END

--LMS10A--
CREATE TABLE [EDOC].[LMS10A](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
        [FIELDH] [nvarchar](255),
        [FIELDI] [nvarchar](255),
        [FIELDJ] [nvarchar](255),
        [FIELDK] [nvarchar](255),
        [FIELDL] [nvarchar](255),
        [FIELDM] [nvarchar](255),
        [FIELDN] [nvarchar](255),
        [FIELDO] [nvarchar](255),
CONSTRAINT [PK_LMS10A] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [LMS10A] CREATED'
-----END LMS10A--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LMS10B')
BEGIN
	DROP TABLE [EDOC].[LMS10B]; PRINT 'LMS10B DELETED'
END

--LMS10B--
CREATE TABLE [EDOC].[LMS10B](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
CONSTRAINT [PK_LMS10B] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [LMS10B] CREATED'
-----END LMS10B--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LMS11A')
BEGIN
	DROP TABLE [EDOC].[LMS11A]; PRINT 'LMS10B DELETED'
END

--LMS11A--
CREATE TABLE [EDOC].[LMS11A](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
        [FIELDH] [nvarchar](255),
        [FIELDI] [nvarchar](255),
        [FIELDJ] [nvarchar](255),
        [FIELDK] [nvarchar](255),
        [FIELDL] [nvarchar](255),
        [FIELDM] [nvarchar](255),
CONSTRAINT [PK_LMS11A] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [LMS11A] CREATED'
-----END LMS11A--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LMS11B')
BEGIN
	DROP TABLE [EDOC].[LMS11B]; PRINT 'LMS11B DELETED'
END

--LMS11B--
CREATE TABLE [EDOC].[LMS11B](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
CONSTRAINT [PK_LMS11B] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [LMS11B] CREATED'
-----END LMS11B--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LMS12A')
BEGIN
	DROP TABLE [EDOC].[LMS12A]; PRINT 'LMS12A DELETED'
END

--LMS12A--
CREATE TABLE [EDOC].[LMS12A](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
        [FIELDH] [nvarchar](255),
        [FIELDI] [nvarchar](255),
        [FIELDJ] [nvarchar](255),
        [FIELDK] [nvarchar](255),
        [FIELDL] [nvarchar](255),
        [FIELDM] [nvarchar](255),
        [FIELDN] [nvarchar](255),
        [FIELDO] [nvarchar](255),
CONSTRAINT [PK_LMS12A] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [LMS12A] CREATED'
-----END LMS12A--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LMS12B')
BEGIN
	DROP TABLE [EDOC].[LMS12B]; PRINT 'LMS12B DELETED'
END

--LMS12B--
CREATE TABLE [EDOC].[LMS12B](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
CONSTRAINT [PK_LMS12B] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [LMS12B] CREATED'
-----END LMS12B--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LMS13A')
BEGIN
	DROP TABLE [EDOC].[LMS13A]; PRINT 'LMS13A DELETED'
END

--LMS13A--
CREATE TABLE [EDOC].[LMS13A](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
        [FIELDH] [nvarchar](255),
        [FIELDI] [nvarchar](255),
        [FIELDJ] [nvarchar](255),
        [FIELDK] [nvarchar](255),
        [FIELDL] [nvarchar](255),
        [FIELDM] [nvarchar](255),
        [FIELDN] [nvarchar](255),
        [FIELDO] [nvarchar](255),
        [FIELDP] [nvarchar](255),
        [FIELDQ] [nvarchar](255),
CONSTRAINT [PK_LMS13A] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [LMS13A] CREATED'
-----END LMS13A--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LMS13B')
BEGIN
	DROP TABLE [EDOC].[LMS13B]; PRINT 'LMS13B DELETED'
END
--LMS13B--
CREATE TABLE [EDOC].LMS13B(
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
CONSTRAINT [PK_LMS13B] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [LMS13B] CREATED'
-----END LMS13B--

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LMS14A')
BEGIN
	DROP TABLE [EDOC].[LMS14A]; PRINT 'LMS14A DELETED'
END

--LMS14A--
CREATE TABLE [EDOC].[LMS14A](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
        [FIELDH] [nvarchar](255),
        [FIELDI] [nvarchar](255),
        [FIELDJ] [nvarchar](255),
        [FIELDK] [nvarchar](255),
        [FIELDL] [nvarchar](255),
        [FIELDM] [nvarchar](255),
        [FIELDN] [nvarchar](255),
        [FIELDO] [nvarchar](255),
        [FIELDP] [nvarchar](255),
        [FIELDQ] [nvarchar](255),
        [FIELDR] [nvarchar](255),
CONSTRAINT [PK_LMS14A] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [LMS14A] CREATED'
-----END LMS14A--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LMS14B')
BEGIN
	DROP TABLE [EDOC].[LMS14B]; PRINT 'LMS14B DELETED'
END

--LMS14B--
CREATE TABLE [EDOC].[LMS14B](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
CONSTRAINT [PK_LMS14B] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [LMS14B] CREATED'
-----END LMS14B--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LMS15A')
BEGIN
	DROP TABLE [EDOC].[LMS15A]; PRINT 'LMS15A DELETED'
END

--LMS15A--
CREATE TABLE [EDOC].[LMS15A](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
        [FIELDH] [nvarchar](255),
        [FIELDI] [nvarchar](255),
        [FIELDJ] [nvarchar](255),
        [FIELDK] [nvarchar](255),
        [FIELDL] [nvarchar](255),
        [FIELDM] [nvarchar](255),
        [FIELDN] [nvarchar](255),
CONSTRAINT [PK_LMS15A] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [LMS15A] CREATED'
-----END LMS15A--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LMS16A')
BEGIN
	DROP TABLE [EDOC].[LMS16A]; PRINT 'LMS16A DELETED'
END

--LMS16A--
CREATE TABLE [EDOC].[LMS16A](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
        [FIELDH] [nvarchar](255),
        [FIELDI] [nvarchar](255),
        [FIELDJ] [nvarchar](255),
        [FIELDK] [nvarchar](255),
        [FIELDL] [nvarchar](255),
CONSTRAINT [PK_LMS16A] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [LMS16A] CREATED'
-----END LMS16A--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LMS16B')
BEGIN
	DROP TABLE [EDOC].[LMS16B]; PRINT 'LMS16B DELETED'
END

--LMS16B--
CREATE TABLE [EDOC].[LMS16B](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
        [FIELDH] [nvarchar](255),
CONSTRAINT [PK_LMS16B] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [LMS16B] CREATED'
-----END LMS16B--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LMS17A')
BEGIN
	DROP TABLE [EDOC].[LMS17A]; PRINT 'LMS17A DELETED'
END
--LMS17A--
CREATE TABLE [EDOC].[LMS17A](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
        [FIELDH] [nvarchar](255),
        [FIELDI] [nvarchar](255),
        [FIELDJ] [nvarchar](255),
        [FIELDK] [nvarchar](255),
        [FIELDL] [nvarchar](255),
        [FIELDM] [nvarchar](255),
        [FIELDN] [nvarchar](255),
        [FIELDO] [nvarchar](255),
        [FIELDP] [nvarchar](255),
        [FIELDQ] [nvarchar](255),
        [FIELDR] [nvarchar](255),
        [FIELDS] [nvarchar](255),
        [FIELDT] [nvarchar](255),
        [FIELDU] [nvarchar](255),
        [FIELDV] [nvarchar](255),
        [FIELDW] [nvarchar](255),
        [FIELDX] [nvarchar](255),
        [FIELDY] [nvarchar](255),
        [FIELDZ] [nvarchar](255),
CONSTRAINT [PK_LMS17A] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [LMS17A] CREATED'
-----END LMS17A--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LMS17B')
BEGIN
	DROP TABLE [EDOC].[LMS17B]; PRINT 'LMS17B DELETED'
END
--LMS17B--
CREATE TABLE [EDOC].[LMS17B](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
CONSTRAINT [PK_LMS17B] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [LMS17B] CREATED'
-----END LMS17B--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LMS18A')
BEGIN
	DROP TABLE [EDOC].[LMS18A]; PRINT 'LMS18A DELETED'
END
--LMS18A--
CREATE TABLE [EDOC].[LMS18A](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
        [FIELDH] [nvarchar](255),
        [FIELDI] [nvarchar](255),
        [FIELDJ] [nvarchar](255),
        [FIELDK] [nvarchar](255),
        [FIELDL] [nvarchar](255),
        [FIELDM] [nvarchar](255),
        [FIELDN] [nvarchar](255),
        [FIELDO] [nvarchar](255),
        [FIELDP] [nvarchar](255),
        [FIELDQ] [nvarchar](255),
        [FIELDR] [nvarchar](255),
        [FIELDS] [nvarchar](255),
        [FIELDT] [nvarchar](255),
        [FIELDU] [nvarchar](255),
        [FIELDV] [nvarchar](255),
        [FIELDW] [nvarchar](255),
        [FIELDX] [nvarchar](255),
        [FIELDY] [nvarchar](255),
CONSTRAINT [PK_LMS18A] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [LMS18A] CREATED'
-----END LMS18A--
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LMS18B')
BEGIN
	DROP TABLE [EDOC].[LMS18B]; PRINT 'LMS18B DELETED'
END
--LMS18B--
CREATE TABLE [EDOC].[LMS18B](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
        [ImportDate] [DATETIME],
        [FIELDA] [nvarchar](255),
        [FIELDB] [nvarchar](255),
        [FIELDC] [nvarchar](255),
        [FIELDD] [nvarchar](255),
        [FIELDE] [nvarchar](255),
        [FIELDF] [nvarchar](255),
        [FIELDG] [nvarchar](255),
CONSTRAINT [PK_LMS18B] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [LMS18B] CREATED'
-----END LMS18B--