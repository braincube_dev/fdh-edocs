-------------------------DROP Details-------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'AttDebAtmTrxDetails')
BEGIN
	DROP TABLE [EDOC].[AttDebAtmTrxDetails]
	PRINT 'Details DELETED'
END
-------------------------DROP DetailsHeader-------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'AttDebAtmTrxDetailsHeader')
BEGIN
	DROP TABLE [EDOC].[AttDebAtmTrxDetailsHeader]
	PRINT 'DetailsHeader DELETED'
END
-------------------------DROP AlphaBankAlbaniaVPSV51-------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'AlphaBankAlbaniaVPSV51')
BEGIN
	DROP TABLE [EDOC].[AlphaBankAlbaniaVPSV51]
	PRINT 'Details DELETED'
END

-------------------------DROP AcbdebBusinessPointDetails-------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'AcbdebBusinessPointDetails')
BEGIN
	DROP TABLE [EDOC].[AcbdebBusinessPointDetails]
	PRINT 'AcbdebBusinessPointDetails DELETED'
END

-------------------------DROP AcbdebBusinessPointHeader-------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'AcbdebBusinessPointHeader')
BEGIN
	DROP TABLE [EDOC].[AcbdebBusinessPointHeader]
	PRINT 'AcbdebBusinessPointHeader DELETED'
END

-------------------------DROP DisputeLettersInfo-------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'DisputeLettersInfo')
BEGIN
	DROP TABLE [EDOC].[DisputeLettersInfo]
	PRINT 'DisputeLettersInfo DELETED'
END

-------------------------DROP DinersCorporateVFPREDetails-------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'DinersCorporateVFPREDetails')
BEGIN
	DROP TABLE [EDOC].[DinersCorporateVFPREDetails]
	PRINT 'DinersCorporateVFPREDetails DELETED'
END

-------------------------DROP DinersCorporateVFPREHeader-------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'DinersCorporateVFPREHeader')
BEGIN
	DROP TABLE [EDOC].[DinersCorporateVFPREHeader]
	PRINT 'DinersCorporateVFPREHeader DELETED'
END

-------------------------DROP AblBillingSupplementaryDetails-------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'AblBillingSupplementaryDetails')
BEGIN
	DROP TABLE [EDOC].[AblBillingSupplementaryDetails]
	PRINT 'AblBillingSupplementaryDetails DELETED'
END

-------------------------DROP AblBillingSupplementaryHeader-------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'AblBillingSupplementaryHeader')
BEGIN
	DROP TABLE [EDOC].[AblBillingSupplementaryHeader]
	PRINT 'AblBillingSupplementaryHeader DELETED'
END

-------------------------DROP AlphaBankRewardsLtr-------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'AlphaBankRewardsLtr')
BEGIN
	DROP TABLE [EDOC].[AlphaBankRewardsLtr]
	PRINT 'AlphaBankRewardsLtr DELETED'
END

-------------------------DROP EmailInfoMain-------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'EmailInfoMain')
BEGIN
	DROP TABLE [EDOC].[EmailInfoMain]
	PRINT 'EmailInfoMain DELETED'
END

-------------------------DROP EmailInfoInset1-------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'EmailInfoInset1')
BEGIN
	DROP TABLE [EDOC].[EmailInfoInset1]
	PRINT 'EmailInfoInset1 DELETED'
END

-------------------------DROP EmailInfoInset2-------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'EmailInfoInset2')
BEGIN
	DROP TABLE [EDOC].[EmailInfoInset2]
	PRINT 'EmailInfoInset2 DELETED'
END

-------------------------DROP EmailInfoInset3-------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'EmailInfoInset3')
BEGIN
	DROP TABLE [EDOC].[EmailInfoInset3]
	PRINT 'EmailInfoInset3 DELETED'
END

-------------------------DROP EmailBINS-------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'EmailBINS')
BEGIN
	DROP TABLE [EDOC].[EmailBINS]
	PRINT 'EmailBINS DELETED'
END


-------------------------DROP AcbstLemonisCards-------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'AcbstLemonisCards')
BEGIN
	DROP TABLE [EDOC].[AcbstLemonisCards]
	PRINT 'AcbstLemonisCards DELETED'
END


-----ATTICA BANK ATM Transactions-----
---------------------------------------------Details--------------------------------------------------------
CREATE TABLE [EDOC].[AttDebAtmTrxDetails](
	[AttDebAtmTrxDetailsID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[CardNumber] [nvarchar](20) NOT NULL,
	[TrxDate] DATETIME NOT NULL,
	[ProcDate] DATETIME NOT NULL,
	[RetRefNo] [nvarchar](20) NOT NULL,
	[TrxDesc] [nvarchar](40) NOT NULL,
	[TrxAmt] [nvarchar](20) NOT NULL,
	[RefNo] [nvarchar](20) NOT NULL,
	[ProcFlag] NVARCHAR(1) NOT NULL, --Yes OR No (Y or N) NOT NULL,
	[ImportDateTime] DATETIME NOT NULL,
CONSTRAINT [PK_Details] PRIMARY KEY CLUSTERED 
(
	[AttDebAtmTrxDetailsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [Details] CREATED'
---------------------------------------------DetailsHeader--------------------------------------------------------
CREATE TABLE [EDOC].[AttDebAtmTrxDetailsHeader](
	[DetailsHeaderID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[DateFrom] DATETIME NOT NULL,
	[DateTo] DATETIME NOT NULL,
	[UploadFileName] [nvarchar](120) NOT NULL,
	[UploadDateTime] DATETIME NOT NULL,
	[UploadedRecordsCounter] INT NOT NULL,
CONSTRAINT [PK_DetailsHeader] PRIMARY KEY CLUSTERED 
(
	[DetailsHeaderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [DetailsHeader] CREATED'
---------------------------------------------------------------------------------------------------------
-----END ATTICA BANK-----

--Alpha Bank Albania VPSV51--

CREATE TABLE [EDOC].[AlphaBankAlbaniaVPSV51](
	[VPSV51ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[CustomerCode] [nvarchar](10) NOT NULL,
	[Name] [nvarchar](40),
	[Surname] [nvarchar](40),
	[FatherName] [nvarchar](40),
	[BirthDate] DATETIME,
	[HomePhone] [nvarchar](10),
	[MobilePhone] [nvarchar](10),
	[HomeAddress] [nvarchar](30),
	[HomeDetail] [nvarchar](20),
	[HomePoBox] [nvarchar](6),
	[HomePostCode] [nvarchar](5),
	[HomeCoutry] [nvarchar](20),
	[NetSalesTrxAmt] [nvarchar](20),
	[RefundAmt] [nvarchar](20),
	[SourceCode] [nvarchar](10),
	[SourcePOS] [nvarchar](10),
	[UploadedFileDateTime] DATETIME,
	[UploadedFileName] [nvarchar](250),
CONSTRAINT [PK_AlphaBankAlbaniaVPSV51] PRIMARY KEY CLUSTERED 
(
	[VPSV51ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [AlphaBankAlbaniaVPSV51] CREATED'
-----END Alpha Bank Albania VPSV51--


--AcbdebBusinessPointHeader--
CREATE TABLE [EDOC].[AcbdebBusinessPointHeader](
	[AcbdebBusinessPointsHeaderID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ProjectID] [nvarchar](20),
	[UploadedFileDateTime] DATETIME,
	[UploadedFileName] [nvarchar](250),
CONSTRAINT [PK_AcbdebBusinessPointsHeader] PRIMARY KEY CLUSTERED 
(
	[AcbdebBusinessPointsHeaderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [AcbdebBusinessPointHeader] CREATED'
-----END AcbdebBusinessPointHeader--

--AcbdebBusinessPointDetails--
CREATE TABLE [EDOC].[AcbdebBusinessPointDetails](
	[AcbdebBusinessPointsDetailID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[AccountNumber] [nvarchar](20),
	[BonusDate] DATETIME,
	[TotalPoints] [nvarchar](20),
	[AcbdebBusinessPointsHeaderID] [int] NOT NULL
CONSTRAINT [PK_AcbdebBusinessPointsDetail] PRIMARY KEY CLUSTERED 
(
	[AcbdebBusinessPointsDetailID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

ALTER TABLE [EDOC].[AcbdebBusinessPointDetails]  WITH CHECK ADD CONSTRAINT [FK_AcbdebBusinessPointDetails_AcbdebBusinessPointHeader] FOREIGN KEY([AcbdebBusinessPointsHeaderID])
REFERENCES [EDOC].[AcbdebBusinessPointHeader] ([AcbdebBusinessPointsHeaderID])

ALTER TABLE [EDOC].[AcbdebBusinessPointDetails] CHECK CONSTRAINT [FK_AcbdebBusinessPointDetails_AcbdebBusinessPointHeader]


PRINT 'TABLE [AcbdebBusinessPointDetails] CREATED'
-----END AcbdebBusinessPointDetails--

--DisputeLettersInfo--
CREATE TABLE [EDOC].[DisputeLettersInfo](
	[DisputeLettersInfoID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[delta_reference] [nvarchar](20),
	[card_reference] [nvarchar](20),
	[card_number] [nvarchar](20),
	[branch] [nvarchar](50),
	[package] [nvarchar](20),
	[merchant_c] [nvarchar](50),
	[from] [nvarchar](20),
	[pdf_name] [nvarchar](50),
	[doc_date] DATETIME,
	[run_date] DATETIME,
	[send_via] [nvarchar](10),
	[fax] [nvarchar](30),
	[email_to] [nvarchar](120),
	[email_cc] [nvarchar](120),
	[email_bcc] [nvarchar](120),
	[sent] [nvarchar](1),
	[error] [nvarchar](1),
	[info_structure] [nvarchar](1),
	[info_consistency] [nvarchar](1),
	[no_inf_file] [nvarchar](1),
	[Invalid_pdf_file] [nvarchar](1),
	[pdf_doesnt_exists] [nvarchar](1),
	[doesnt_have_fax] [nvarchar](1),
	[doesnt_have_email_to] [nvarchar](1),
	[doesnt_have_valid_email_to] [nvarchar](1),
	[doesnt_have_valid_email_cc] [nvarchar](1),
	[doesnt_have_valid_email_bcc] [nvarchar](1),
	[error_from_runtime] [nvarchar](101),
CONSTRAINT [PK_DisputeLettersInfo] PRIMARY KEY CLUSTERED 
(
	[DisputeLettersInfoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [DisputeLettersInfo] CREATED'
-----END DisputeLettersInfo--


--DinersCorporateVFPREHeader--
CREATE TABLE [EDOC].[DinersCorporateVFPREHeader](
	[DinersCorporateVFPREHeaderID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[RecordNumber] [nvarchar](10),
	[Version] [nvarchar](1),
	[ProdDateTime] DATETIME,
	[FileType] [nvarchar](10),
	[FSN] [nvarchar](10),
	[IssuerID] [nvarchar](10),
	[RecordCount] [nvarchar](10),
	[UploadFileName] [nvarchar](250),
	[UploadDateTime] DATETIME,
CONSTRAINT [PK_DinersCorporateVFPREHeader] PRIMARY KEY CLUSTERED 
(
	[DinersCorporateVFPREHeaderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [DinersCorporateVFPREHeader] CREATED'
-----END DinersCorporateVFPREHeader--

--DinersCorporateVFPREDetails--
CREATE TABLE [EDOC].[DinersCorporateVFPREDetails](
	[DinersCorporateVFPREDetailsID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[RecordNumber] [nvarchar](10),
	[CardNumber] [nvarchar](20),
	[MobileNumber] [nvarchar](10),
	[VodaphoneCustomerNumber] [nvarchar](10),
	[UAID] [nvarchar](30),
	[NumberOfPoints] [nvarchar](10),
	[DateOfPointsDeletion] DATETIME,
	[ReferenceNo] [nvarchar](20),
	[ImportDateTime] DATETIME,
	[DinersCorporateVFPREHeaderID] INT NOT NULL,
CONSTRAINT [PK_DinersCorporateVFPREDetails] PRIMARY KEY CLUSTERED 
(
	[DinersCorporateVFPREDetailsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

ALTER TABLE [EDOC].[DinersCorporateVFPREDetails]  WITH CHECK ADD CONSTRAINT [FK_DinersCorporateVFPREDetails_DinersCorporateVFPREHeader] FOREIGN KEY([DinersCorporateVFPREHeaderID])
REFERENCES [EDOC].[DinersCorporateVFPREHeader] ([DinersCorporateVFPREHeaderID])

ALTER TABLE [EDOC].[DinersCorporateVFPREDetails] CHECK CONSTRAINT [FK_DinersCorporateVFPREDetails_DinersCorporateVFPREHeader]

PRINT 'TABLE [DinersCorporateVFPREDetails] CREATED'
-----END DinersCorporateVFPREDetails--


--AblBillingSupplementaryHeader--
CREATE TABLE [EDOC].[AblBillingSupplementaryHeader](
	[AblBillingSupplementaryHeaderID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Origination] [nvarchar](20),
	[LiteralDescription] [nvarchar](20),
	[BillingDate] DATETIME,
	[UploadFileName] [nvarchar](250),
	[UploadDateTime] DATETIME,
	[TrailerPart] [nvarchar](max),
CONSTRAINT [PK_AblBillingSupplementaryHeader] PRIMARY KEY CLUSTERED 
(
	[AblBillingSupplementaryHeaderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [AblBillingSupplementaryHeader] CREATED'
-----END AblBillingSupplementaryHeader--


--AblBillingSupplementaryDetails--
CREATE TABLE [EDOC].[AblBillingSupplementaryDetails](
	[AblBillingSupplementaryDetailsID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Origination] [nvarchar](255),
	[CardNumber] [nvarchar](20),
	[ApplicationReference] [nvarchar](20),
	[IBAN] [nvarchar](30),
	[MemberID] [nvarchar](10),
	[MilesBalanceStart] [nvarchar](20),
	[MilesBalanceEnd] [nvarchar](20),
	[MilesBalanceDiff] [nvarchar](20),
	[ImportDateTime] DATETIME,
	[AblBillingSupplementaryHeaderID] INT NOT NULL,
CONSTRAINT [PK_AblBillingSupplementaryDetails] PRIMARY KEY CLUSTERED 
(
	[AblBillingSupplementaryDetailsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

ALTER TABLE [EDOC].[AblBillingSupplementaryDetails]  WITH CHECK ADD CONSTRAINT [FK_AblBillingSupplementaryDetails_AblBillingSupplementaryHeader] FOREIGN KEY([AblBillingSupplementaryHeaderID])
REFERENCES [EDOC].[AblBillingSupplementaryHeader] ([AblBillingSupplementaryHeaderID])

ALTER TABLE [EDOC].[AblBillingSupplementaryDetails] CHECK CONSTRAINT [FK_AblBillingSupplementaryDetails_AblBillingSupplementaryHeader]

PRINT 'TABLE [AblBillingSupplementaryDetails] CREATED'
-----END AblBillingSupplementaryDetails--



--AlphaBankRewardsLtr--
CREATE TABLE [EDOC].[AlphaBankRewardsLtr](
	[AlphaBankRewardsLtrID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ImportDateTime] DATETIME,
	[FieldA] [nvarchar](255),
	[FieldB] [nvarchar](255),
	[FieldC] [nvarchar](255),
	[FieldD] [nvarchar](255),
	[FieldE] [nvarchar](255),
	[FieldF] [nvarchar](255),
	[FieldG] [nvarchar](255),
	[FieldH] [nvarchar](255),
	[FieldI] [nvarchar](255),
	[FieldJ] [nvarchar](255),
	[FieldK] [nvarchar](255),
	[FieldL] [nvarchar](255),
	[FieldM] [nvarchar](255),
	[FieldN] [nvarchar](255),
	[FieldO] [nvarchar](255),
	[FieldP] [nvarchar](255),
	[FieldQ] [nvarchar](255),
	[FieldR] [nvarchar](255),
	[FieldS] [nvarchar](255),
	[FieldT] [nvarchar](255),
	[FieldU] [nvarchar](255),
	[FieldV] [nvarchar](255),
	[FieldW] [nvarchar](255),
	[FieldX] [nvarchar](255),
	[FieldZ] [nvarchar](255),
	[FieldY] [nvarchar](255),
	[FieldAA] [nvarchar](255),
	[FieldAB] [nvarchar](255),
	[FieldAC] [nvarchar](255),
	[FieldAD] [nvarchar](255),
	[FieldAE] [nvarchar](255),
	[FieldAF] [nvarchar](255),
	[FieldAG] [nvarchar](255),
	[FieldAH] [nvarchar](255),
	[FieldAI] [nvarchar](255),
	[FieldAJ] [nvarchar](255),
	[FieldAK] [nvarchar](255),
	[FieldAL] [nvarchar](255),
	[FieldAM] [nvarchar](255),
	[FieldAN] [nvarchar](255),
	[FieldAO] [nvarchar](255),
	[FieldAP] [nvarchar](255),
	[FieldAQ] [nvarchar](255),
	[FieldAR] [nvarchar](255),
	[FieldAS] [nvarchar](255),
CONSTRAINT [PK_AlphaBankRewardsLtr] PRIMARY KEY CLUSTERED 
(
	[AlphaBankRewardsLtrID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [AlphaBankRewardsLtr] CREATED'
-----END AlphaBankRewardsLtr--


--EmailInfoMain--
CREATE TABLE [EDOC].[EmailInfoMain](
	[EmailInfoMainID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[MENU] [nvarchar](20),
	[MENU_ID] [nvarchar](2),
	[PRODUCT_DESCRIPTION] [nvarchar](50),
	[PRODUCT_CODE] [nvarchar](2),
	[ISSUE_DATE] DATETIME,
	[PRIORITY] [nvarchar](2),
	[MRK_IMAGE_NAME_1] [nvarchar](50),
	[MRK_URL_1] [nvarchar](255),
	[MRK_IMAGE_NAME_2] [nvarchar](50),
	[MRK_URL_2] [nvarchar](255),
	[MRK_IMAGE_NAME_3] [nvarchar](50),
	[MRK_URL_3] [nvarchar](255),
	[ZIP_FROM] [nvarchar](5),
	[ZIP_TO] [nvarchar](5),
	[SOURCE_CODE_FROM] [nvarchar](7),
	[SOURCE_CODE_TO] [nvarchar](7),
	[SOURCE_POS_FROM] [nvarchar](5),
	[SOURCE_POS_TO] [nvarchar](5),
	[BUCKET] [nvarchar](2),
	[BLACK_LIST_CODE] [nvarchar](2),
	[LEGAL] [nvarchar](1),
	[ACCOUNT_STATUS] [nvarchar](2),
	[OCCUPATION_CODE] [nvarchar](4),
	[BIRTH_DATE] DATETIME,
	[GENDER] [nvarchar](1),
	[EXPIRATION_DATE] DATETIME,
	[EXTRA_PAGE_L1] [nvarchar](1),
	[EXTRA_PAGE_L1_IMG_NAME] [nvarchar](255),
	[EXTRA_PAGE_L2] [nvarchar](1),
	[EXTRA_PAGE_L2_IMG_NAME] [nvarchar](255),
	[EXTRA_PAGE_L3] [nvarchar](1),
	[EXTRA_PAGE_L3_IMG_NAME] [nvarchar](255),
	[GENERIC] [nvarchar](1),
	[TEXT_MESSAGE_1] [nvarchar](255),
	[TEMPLATE_ID] [nvarchar](255),
	[ImportDate] DATETIME,
CONSTRAINT [PK_EmailInfoMain] PRIMARY KEY CLUSTERED 
(
	[EmailInfoMainID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [EmailInfoMain] CREATED'
-----END EmailInfoMain--


--EmailInfoInset1--
CREATE TABLE [EDOC].[EmailInfoInset1](
	[EmailInfoInset1ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[CARD_NUMBER] [nvarchar](16),
	[REFERENCE_NUMBER] [nvarchar](16),
CONSTRAINT [PK_EmailInfoInset1] PRIMARY KEY CLUSTERED 
(
	[EmailInfoInset1ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [EmailInfoInset1] CREATED'
-----END EmailInfoInset1--


--EmailInfoInset2--
CREATE TABLE [EDOC].[EmailInfoInset2](
	[EmailInfoInset2ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[CARD_NUMBER] [nvarchar](16),
	[REFERENCE_NUMBER] [nvarchar](16),
CONSTRAINT [PK_EmailInfoInset2] PRIMARY KEY CLUSTERED 
(
	[EmailInfoInset2ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [EmailInfoInset2] CREATED'
-----END EmailInfoInset2--


--EmailInfoInset3--
CREATE TABLE [EDOC].[EmailInfoInset3](
	[EmailInfoInset3ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[CARD_NUMBER] [nvarchar](16),
	[REFERENCE_NUMBER] [nvarchar](16),
CONSTRAINT [PK_EmailInfoInset3] PRIMARY KEY CLUSTERED 
(
	[EmailInfoInset3ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [EmailInfoInset3] CREATED'
-----END EmailInfoInset3--

--EmailBINS--
CREATE TABLE [EDOC].[EmailBINS](
	[BINSID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[MENU] [nvarchar](20),
	[MENU_ID] [int],
	[PRODUCT_DESCRIPTION] [nvarchar](50),
	[PRODUCT_CODE] [int],
CONSTRAINT [PK_BINS] PRIMARY KEY CLUSTERED 
(
	[BINSID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [EmailBINS] CREATED'
-----END EmailBINS--


--AcbstLemonisCards--
CREATE TABLE [EDOC].[AcbstLemonisCards](
	[CardID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[CardNumber] [nvarchar](20),
	[BillingDate] DATETIME,
	[ReadCount] INT,
	[CardData] [nvarchar](MAX),
CONSTRAINT [PK_AcbstLemonisCards] PRIMARY KEY CLUSTERED 
(
	[CardID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [AcbstLemonisCards] CREATED'
-----END AcbstLemonisCards--

----------AbcFactors--------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'AbcFactorsDebtors')
BEGIN
	DROP TABLE [EDOC].[AbcFactorsDebtors];PRINT 'AbcFactorsDebtors DELETED'
END

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'AbcFactorsPayments')
BEGIN
	DROP TABLE [EDOC].[AbcFactorsPayments];PRINT 'AbcFactorsPayments DELETED'
END

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'AbcFactorsTransactions')
BEGIN
	DROP TABLE [EDOC].[AbcFactorsTransactions];PRINT 'AbcFactorsTransactions DELETED'
END

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'AbcFactorsCustomers')
BEGIN
	DROP TABLE [EDOC].[AbcFactorsCustomers];PRINT 'AbcFactorsCustomers DELETED'
END


--AbcFactorsCustomers--
CREATE TABLE [EDOC].[AbcFactorsCustomers](
	[AbcFactorsCustomersID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[CLIENTNO] [nvarchar](10) UNIQUE NOT NULL,
	[CLNAME] [nvarchar](30),
	[CLADDR1] [nvarchar](30),
	[CLADDR2] [nvarchar](30),
	[CLADDR3] [nvarchar](30),
	[CLADDR4] [nvarchar](30),
	[FULL_LINE] [nvarchar](255),
CONSTRAINT [PK_AbcFactorsCustomers] PRIMARY KEY CLUSTERED 
(
	[AbcFactorsCustomersID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [AbcFactorsCustomers] CREATED'
-----END AbcFactorsCustomers--

--AbcFactorsDebtors--
CREATE TABLE [EDOC].[AbcFactorsDebtors](
	[AbcFactorsDebtorsID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[CLDEBID] [nvarchar](10),
	[CLIENTNO] [nvarchar](10),
	[CLTYPE] [nvarchar](3),
	[CLCURCD] [nvarchar](3),
	[DBAFC] [nvarchar](10),
	[DEBTORNO] [nvarchar](10),
	[DRCURCD] [nvarchar](3),
	[CURRABBR] [nvarchar](3),
	[DRNAME] [nvarchar](40),
	[DRADDR1] [nvarchar](60),
	[DRADDR2] [nvarchar](30),
	[DRADDR3] [nvarchar](30),
	[DRADDR4] [nvarchar](30),
	[CONTACT] [nvarchar](30),
	[DRPHONE] [nvarchar](20),
	[DRSCHKEY] [nvarchar](15),
	[DRCURBAL] [nvarchar](16),
	[DRNOTBAL] [nvarchar](16),
	[FULL_LINE_1] [nvarchar](255),
	[FULL_LINE_2] [nvarchar](255),
CONSTRAINT [PK_AbcFactorsDebtors] PRIMARY KEY CLUSTERED 
(
	[AbcFactorsDebtorsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

ALTER TABLE [EDOC].[AbcFactorsDebtors]  WITH CHECK ADD CONSTRAINT [FK_AbcFactorsDebtors_AbcFactorsCustomers] FOREIGN KEY([CLIENTNO])
REFERENCES [EDOC].[AbcFactorsCustomers] ([CLIENTNO])

ALTER TABLE [EDOC].[AbcFactorsDebtors] CHECK CONSTRAINT [FK_AbcFactorsDebtors_AbcFactorsCustomers]

PRINT 'TABLE [AbcFactorsDebtors] CREATED'
-----END AbcFactorsDebtors--

--AbcFactorsPayments--
CREATE TABLE [EDOC].[AbcFactorsPayments](
	[AbcFactorsPaymentsID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[CLIENTBID] [nvarchar](10),
	[CLIENTNO] [nvarchar](10),
	[CLTYPE] [nvarchar](3),
	[CLCURCD] [nvarchar](3),
	[DBAFC] [nvarchar](10),
	[DEBTORNO] [nvarchar](10),
	[DRCURCD] [nvarchar](3),
	[CURRABBR] [nvarchar](3),
	[DOCNO] [nvarchar](15),
	[DOCDESC] [nvarchar](10),
	[DOCDATE] DATETIME,
	[ACTBAL] [nvarchar](16),
	[DOCBAL] [nvarchar](16),
	[PAYTYPE] [nvarchar](6),
	[CREDDATE] DATETIME,
	[PAYAMT] [nvarchar](16),
	[ALLOCAMT] [nvarchar](16),
	[DOCBAL2] [nvarchar](16),
	[FULL_LINE] [nvarchar](255),
CONSTRAINT [PK_AbcFactorsPayments] PRIMARY KEY CLUSTERED 
(
	[AbcFactorsPaymentsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

ALTER TABLE [EDOC].[AbcFactorsPayments]  WITH CHECK ADD CONSTRAINT [FK_AbcFactorsPayments_AbcFactorsCustomers] FOREIGN KEY([CLIENTNO])
REFERENCES [EDOC].[AbcFactorsCustomers] ([CLIENTNO])

ALTER TABLE [EDOC].[AbcFactorsPayments] CHECK CONSTRAINT [FK_AbcFactorsPayments_AbcFactorsCustomers]

PRINT 'TABLE [AbcFactorsPayments] CREATED'
-----END AbcFactorsPayments--

--AbcFactorsTransactions--
CREATE TABLE [EDOC].[AbcFactorsTransactions](
	[AbcFactorsTransactionsID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[CLIDEBID] [nvarchar](10),
	[CLIENTNO] [nvarchar](10),
	[CLTYPE] [nvarchar](3),
	[CLCURCD] [nvarchar](3),
	[DBAFC] [nvarchar](10),
	[DEBTORNO] [nvarchar](10),
	[DRCURCD] [nvarchar](3),
	[CURRABBR] [nvarchar](3),
	[DOCNO] [nvarchar](15),
	[DOCDESC] [nvarchar](10),
	[DOCDATE] DATETIME,
	[DUEDATE] DATETIME,
	[DOCBAL] [nvarchar](16),
	[ACTBAL] [nvarchar](16),
	[FULL_DOCDATE] DATETIME,
	[FULL_LINE] [nvarchar](255),
CONSTRAINT [PK_AbcFactorsTransactions] PRIMARY KEY CLUSTERED 
(
	[AbcFactorsTransactionsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

ALTER TABLE [EDOC].[AbcFactorsTransactions]  WITH CHECK ADD CONSTRAINT [FK_AbcFactorsTransactions_AAbcFactorsCustomers] FOREIGN KEY([CLIENTNO])
REFERENCES [EDOC].[AbcFactorsCustomers] ([CLIENTNO])

ALTER TABLE [EDOC].[AbcFactorsTransactions] CHECK CONSTRAINT [FK_AbcFactorsTransactions_AAbcFactorsCustomers]

PRINT 'TABLE [AbcFactorsTransactions] CREATED'
-----END AbcFactorsTransactions--
----------------------------------------------


----------Invoicing--------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'InvoicingServicesPerPolicy')
BEGIN
	DROP TABLE [EDOC].[InvoicingServicesPerPolicy]; PRINT 'InvoicingServicesPerPolicy DELETED'
END

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'InvoicingJobsDetails')
BEGIN
	DROP TABLE [EDOC].[InvoicingJobsDetails]; PRINT 'InvoicingJobsDetails DELETED'
END

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'InvoicingJobs')
BEGIN
	DROP TABLE [EDOC].[InvoicingJobs]; PRINT 'InvoicingJobs DELETED'
END

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'InvoicingServices')
BEGIN
	DROP TABLE [EDOC].[InvoicingServices]; PRINT 'InvoicingServices DELETED'
END

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'InvoicingProjects')
BEGIN
	DROP TABLE [EDOC].[InvoicingProjects]; PRINT 'InvoicingProjects DELETED'
END

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'InvoicingPolicies')
BEGIN
	DROP TABLE [EDOC].[InvoicingPolicies]; PRINT 'InvoicingPolicies DELETED'
END

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'InvoicingProducts')
BEGIN
	DROP TABLE [EDOC].[InvoicingProducts]; PRINT 'InvoicingProducts DELETED'
END

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'InvoicingCustomers')
BEGIN
	DROP TABLE [EDOC].[InvoicingCustomers]; PRINT 'InvoicingCustomers DELETED'
END

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'InvoicingAlgorithm')
BEGIN
	DROP TABLE [EDOC].[InvoicingAlgorithm]; PRINT 'InvoicingAlgorithm DELETED'
END

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'InvoicingStatFile')
BEGIN
	DROP TABLE [EDOC].[InvoicingStatFile]; PRINT 'InvoicingStatFile DELETED'
END


--InvoicingAlgorithm--
CREATE TABLE [EDOC].[InvoicingAlgorithm](
	[InvoicingAlgorithmID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ALGORITHM_CODE] INT,
	[ALGORITHM_DESCR] [nvarchar](50),
CONSTRAINT [PK_InvoicingAlgorithm] PRIMARY KEY CLUSTERED 
(
	[InvoicingAlgorithmID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [InvoicingAlgorithm] CREATED'
-----END InvoicingAlgorithm--


--InvoicingCustomers--
CREATE TABLE [EDOC].[InvoicingCustomers](
	[CUST_CODE_DM] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[CUST_NAME_DM] [nvarchar](50),
	[CUST_DESCR] [nvarchar](40),
	[CUST_CODE_L] [nvarchar](10),
	[CUST_NON_EXPORT] INT,
CONSTRAINT [PK_InvoicingCustomers] PRIMARY KEY CLUSTERED 
(
	[CUST_CODE_DM] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [InvoicingCustomers] CREATED'
-----END InvoicingCustomers--

--InvoicingServices--
CREATE TABLE [EDOC].[InvoicingServices](
	[SERVICE_CODE] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[SERVICE_DESCR] [nvarchar](50),
	[SERVICE_CODE_L] [nvarchar](10),
	[SERVICE_ALGORITHM_CODE] [int],
	[SERVICE_NON_EXPORT] [int],
CONSTRAINT [PK_InvoicingServices] PRIMARY KEY CLUSTERED 
(
	[SERVICE_CODE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [InvoicingServices] CREATED'
-----END InvoicingServices--

--InvoicingPolicies--
CREATE TABLE [EDOC].[InvoicingPolicies](
	[POLICY_CODE] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[POLICY_DESCR] [nvarchar](50),
CONSTRAINT [PK_InvoicingPolicies] PRIMARY KEY CLUSTERED 
(
	[POLICY_CODE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [InvoicingPolicies] CREATED'
-----END InvoicingPolicies--

--InvoicingProducts--
CREATE TABLE [EDOC].[InvoicingProducts](
	[PROD_CODE_DM] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[PROD_NAME_DM] [nvarchar](20),
	[PROD_CUST_CODE_DM] [int],
	[PROD_DESCR] [nvarchar](40),
	[PROD_CODE_L] [nvarchar](10),
	[PROD_NON_EXPORT] [int],
CONSTRAINT [PK_InvoicingProducts] PRIMARY KEY CLUSTERED 
(
	[PROD_CODE_DM] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

ALTER TABLE [EDOC].[InvoicingProducts]  WITH CHECK ADD CONSTRAINT [FK_InvoicingProducts_InvoicingCustomers] FOREIGN KEY([PROD_CUST_CODE_DM])
REFERENCES [EDOC].[InvoicingCustomers] ([CUST_CODE_DM])

ALTER TABLE [EDOC].[InvoicingProducts] CHECK CONSTRAINT [FK_InvoicingProducts_InvoicingCustomers]

PRINT 'TABLE [InvoicingProducts] CREATED'
-----END InvoicingProducts--


--InvoicingProjects--
CREATE TABLE [EDOC].[InvoicingProjects](
	[PROJ_CODE_DM] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[PROJ_NAME_DM] [nvarchar](50),
	[PROJ_PROD_CODE_DM] [int],
	[PROJ_DESCR] [nvarchar](50),
	[PROJ_CODE_L] [nvarchar](10),
	[PROJ_POLICY] [int],
	[PROJ_NON_EXPORT] [int],
CONSTRAINT [PK_InvoicingProjects] PRIMARY KEY CLUSTERED 
(
	[PROJ_CODE_DM] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

ALTER TABLE [EDOC].[InvoicingProjects]  WITH CHECK ADD CONSTRAINT [FK_InvoicingProjects_InvoicingProducts] FOREIGN KEY([PROJ_CODE_DM])
REFERENCES [EDOC].[InvoicingProducts] ([PROD_CODE_DM])

ALTER TABLE [EDOC].[InvoicingProjects] CHECK CONSTRAINT [FK_InvoicingProjects_InvoicingProducts]

ALTER TABLE [EDOC].[InvoicingProjects]  WITH CHECK ADD CONSTRAINT [FK_InvoicingProjects_InvoicingPolicies] FOREIGN KEY([PROJ_POLICY])
REFERENCES [EDOC].[InvoicingPolicies] ([POLICY_CODE])

ALTER TABLE [EDOC].[InvoicingProjects] CHECK CONSTRAINT [FK_InvoicingProjects_InvoicingPolicies]

PRINT 'TABLE [InvoicingProjects] CREATED'
-----END InvoicingProjects--



--InvoicingJobs--
CREATE TABLE [EDOC].[InvoicingJobs](
	[JOB_NUM] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[JOB_PROJ_CODE_DM] INT,
	[RUN_DATE] DATE,
	[TIME_STAMP] [nvarchar](50),
	[IN_FILE_NAME] [nvarchar](255),
	[SPOOL_FILE_NAME] [nvarchar](50),
	[GROUP_DISTINCT_HEADER] [nvarchar](50),
	[GROUP_IN_ACCNTS] INT,
	[PRINT_FLAG] BIT,
	[ENVEL_FLAG] BIT,
	[PRINT_TIMES] INT,
	[ENVEL_TIMES] INT,
	[PDF_FLAG] BIT,
	[ACTIVE_FLAG] BIT,
CONSTRAINT [PK_InvoicingJobs] PRIMARY KEY CLUSTERED 
(
	[JOB_NUM] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

ALTER TABLE [EDOC].[InvoicingJobs]  WITH CHECK ADD CONSTRAINT [FK_InvoicingJobs_InvoicingProjects] FOREIGN KEY([JOB_PROJ_CODE_DM])
REFERENCES [EDOC].[InvoicingProjects] ([PROJ_CODE_DM])

ALTER TABLE [EDOC].[InvoicingJobs] CHECK CONSTRAINT [FK_InvoicingJobs_InvoicingProjects]

PRINT 'TABLE [InvoicingJobs] CREATED'
-----END InvoicingJobs--


--InvoicingJobsDetails--
CREATE TABLE [EDOC].[InvoicingJobsDetails](
	[InvoicingJobsDetailsID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[JOB_D_NUM] INT,
	[JOB_D_JOB_NUM] INT,
	[SERVICE_CODE] INT,
	[QUANTITY] INT,
CONSTRAINT [PK_InvoicingJobsDetails] PRIMARY KEY CLUSTERED 
(
	[InvoicingJobsDetailsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

ALTER TABLE [EDOC].[InvoicingJobsDetails]  WITH CHECK ADD CONSTRAINT [FK_InvoicingJobsDetails_InvoicingJobs] FOREIGN KEY([JOB_D_JOB_NUM])
REFERENCES [EDOC].[InvoicingJobs] ([JOB_NUM])

ALTER TABLE [EDOC].[InvoicingJobsDetails] CHECK CONSTRAINT [FK_InvoicingJobsDetails_InvoicingJobs]

ALTER TABLE [EDOC].[InvoicingJobsDetails]  WITH CHECK ADD CONSTRAINT [FK_InvoicingJobsDetails_InvoicingServices] FOREIGN KEY([SERVICE_CODE])
REFERENCES [EDOC].[InvoicingServices] ([SERVICE_CODE])

ALTER TABLE [EDOC].[InvoicingJobsDetails] CHECK CONSTRAINT [FK_InvoicingJobsDetails_InvoicingServices]

PRINT 'TABLE [InvoicingJobsDetails] CREATED'
-----END InvoicingJobsDetails--



--InvoicingServicesPerPolicy--
CREATE TABLE [EDOC].[InvoicingServicesPerPolicy](
	[InvoicingServicesPerPolicyID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[POLICY_CODE] [int],
	[SERVICE_CODE] [int],
CONSTRAINT [PK_InvoicingServicesPerPolicy] PRIMARY KEY CLUSTERED 
(
	[InvoicingServicesPerPolicyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

ALTER TABLE [EDOC].[InvoicingServicesPerPolicy]  WITH CHECK ADD CONSTRAINT [FK_InvoicingServicesPerPolicy_InvoicingPolicies] FOREIGN KEY([POLICY_CODE])
REFERENCES [EDOC].[InvoicingPolicies] ([POLICY_CODE])

ALTER TABLE [EDOC].[InvoicingServicesPerPolicy] CHECK CONSTRAINT [FK_InvoicingServicesPerPolicy_InvoicingPolicies]


ALTER TABLE [EDOC].[InvoicingServicesPerPolicy]  WITH CHECK ADD CONSTRAINT [FK_InvoicingServicesPerPolicy_InvoicingServices] FOREIGN KEY([SERVICE_CODE])
REFERENCES [EDOC].[InvoicingServices] ([SERVICE_CODE])

ALTER TABLE [EDOC].[InvoicingServicesPerPolicy] CHECK CONSTRAINT [FK_InvoicingServicesPerPolicy_InvoicingServices]

PRINT 'TABLE [InvoicingServicesPerPolicy] CREATED'
-----END InvoicingServicesPerPolicy--


--InvoicingStatFile--
CREATE TABLE [EDOC].[InvoicingStatFile](
	[InvoicingStatFileID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[TOT_SHEETS] INT,
	[TOT_SHEETS_SIMPLEX] INT,
	[TOT_SHEETS_DUPLEX] INT,
	[TOT_PAGES] INT,
	[ENTYPO_1] INT,
	[ENTYPO_2] INT,
	[ENTYPO_3] INT,
	[ENTYPO_4] INT,
	[TOT_ENTHETA] INT,
	[ENTHETO_1] INT,
	[ENTHETO_2] INT,
	[ENTHETO_3] INT,
	[ENTHETO_4] INT,
	[COLOR_TYPE_1] INT,
	[COLOR_TYPE_2] INT,
	[COLOR_TYPE_3] INT,
	[CUTX_FLAG] INT,
	[CUTX_SHEETS] INT,
	[CUTX_PAGES] INT,
	[SPECIAL_QTY0] INT,
	[SPECIAL_QTY1] INT,
	[SPECIAL_QTY2] INT,
	[SPECIAL_QTY3] INT,
	[SPECIAL_QTY4] INT,
	[SPECIAL_QTY5] INT,
	[SPECIAL_QTY6] INT,
	[SPECIAL_QTY7] INT,
	[SPECIAL_QTY8] INT,
	[SPECIAL_QTY9] INT,
CONSTRAINT [PK_InvoicingStatFile] PRIMARY KEY CLUSTERED 
(
	[InvoicingStatFileID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [InvoicingStatFile] CREATED'
-----END InvoicingStatFile--


----------------------------------------------
