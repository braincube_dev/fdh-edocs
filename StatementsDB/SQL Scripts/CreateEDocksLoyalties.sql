
-------------------------DROP BankAdjustments-------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LoyaltyBankAdjustments')
BEGIN
	DROP TABLE [EDOC].[LoyaltyBankAdjustments]
	PRINT 'BankAdjustments DELETED'
END
-----------------------------------------------------------
-------------------------DROP CorporateData-------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LoyaltyCorporateData')
BEGIN
	DROP TABLE [EDOC].[LoyaltyCorporateData]
	PRINT 'CorporateData DELETED'
END
-----------------------------------------------------------
-------------------------DROP CorporateNames-------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LoyaltyCorporateNames')
BEGIN
	DROP TABLE [EDOC].[LoyaltyCorporateNames]
	PRINT 'CorporateNames DELETED'
END
-----------------------------------------------------------
-------------------------DROP CurrentDay-------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LoyaltyCurrentDay')
BEGIN
	DROP TABLE [EDOC].[LoyaltyCurrentDay]
	PRINT 'CurrentDay DELETED'
END
-----------------------------------------------------------
-------------------------DROP LoyaltyDates-------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LoyaltyDates')
BEGIN
	DROP TABLE [EDOC].[LoyaltyDates]
	PRINT 'LoyaltyDates DELETED'
END
-----------------------------------------------------------
-------------------------DROP ExcludedCards-------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LoyaltyExcludedCards')
BEGIN
	DROP TABLE [EDOC].[LoyaltyExcludedCards]
	PRINT 'ExcludedCards DELETED'
END
-----------------------------------------------------------
-------------------------DROP ExtraPoints-------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LoyaltyExtraPoints')
BEGIN
	DROP TABLE [EDOC].[LoyaltyExtraPoints]
	PRINT 'ExtraPoints DELETED'
END
-----------------------------------------------------------
-------------------------DROP LoyaltyHeader-------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LoyaltyHeader')
BEGIN
	DROP TABLE [EDOC].[LoyaltyHeader]
	PRINT 'LoyaltyHeader DELETED'
END
-----------------------------------------------------------
-------------------------DROP TotalPerCard-------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LoyaltyTotalPerCard')
BEGIN
	DROP TABLE [EDOC].[LoyaltyTotalPerCard]
	PRINT 'TotalPerCard DELETED'
END
-----------------------------------------------------------

-------------------------DROP Card-------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LoyaltyCard')
BEGIN
	DROP TABLE [EDOC].[LoyaltyCard]
	PRINT 'Card DELETED'
END
-----------------------------------------------------------

-------------------------DROP Schemes-------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'LoyaltySchemes')
BEGIN
	DROP TABLE [EDOC].[LoyaltySchemes]
	PRINT 'Schemes DELETED'
END
-----------------------------------------------------------


---------------------------------------------Schemes--------------------------------------------------------
CREATE TABLE [EDOC].[LoyaltySchemes](
	[SchemeID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[SchemeName] [nvarchar](100) NOT NULL,

CONSTRAINT [PK_Schemes] PRIMARY KEY CLUSTERED 
(
	[SchemeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [LoyaltySchemes] CREATED'
---------------------------------------------------------------------------------------------------------
---------------------------------------------CorporateNames--------------------------------------------------------
CREATE TABLE [EDOC].[LoyaltyCorporateNames](
	[CorporateCodeID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[CorporateCode] [nvarchar](10) NOT NULL,
	[weight] BIGINT NOT NULL,
	[CorporateName] [nvarchar](50) NOT NULL,

CONSTRAINT [PK_CorporateNames] PRIMARY KEY CLUSTERED 
(
	[CorporateCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [LoyaltyCorporateNames] CREATED'
---------------------------------------------------------------------------------------------------------
---------------------------------------------Card--------------------------------------------------------
CREATE TABLE [EDOC].[LoyaltyCard](
	[CardID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[SchemeID] INT NOT NULL,
	[ProcessDate] DATETIME NOT NULL,
	[PAN] [nvarchar](20) NOT NULL,
	[TemplateNumber] INT NOT NULL,
	[LetterID] INT NOT NULL,
	[InsertType] [nvarchar](4) NOT NULL,
	[PrevTPts] [nvarchar](20) NOT NULL,
	[TEPts] [nvarchar](20) NOT NULL,
	[TRPts] [nvarchar](20) NOT NULL,
	[NewTPts] [nvarchar](20) NOT NULL,
	[ExtraPoints] [nvarchar](20) NOT NULL,
	[PointsExpired] [nvarchar](20) NOT NULL,
	[PointsToBeExpired] [nvarchar](20) NOT NULL,
	[BonusPtsAdjustements] [nvarchar](20) NOT NULL,
	[ListOfOffers] [nvarchar](50) NOT NULL,
	[CorporateDataRecords] INT NOT NULL,
	[ExtraPointsDataRecords] INT NOT NULL,
	[XLSAccountID] [nvarchar](30) NOT NULL,
	[TotalPerCardRecords] INT NOT NULL,
	[ActiveCardFlag] INT NOT NULL,
	[TagRFU] [nvarchar](20) NOT NULL,
	[UpdateFlag] [nvarchar](1) NOT NULL,
	[BIN] [nvarchar](10) NOT NULL,
	[ProductCode] [nvarchar](2) NOT NULL,
	[PrintFlag] NVARCHAR(1) NOT NULL, --Yes OR No (Y or )
	[Found] NVARCHAR(1) NOT NULL, --Yes OR No (Y or )

	CONSTRAINT [PK_Card] PRIMARY KEY CLUSTERED 
(
	[PAN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

ALTER TABLE [EDOC].[LoyaltyCard]  WITH CHECK ADD CONSTRAINT [FK_Card_Scheme] FOREIGN KEY([SchemeID])
REFERENCES [EDOC].[LoyaltySchemes] ([SchemeID])
PRINT 'TABLE [LoyaltyCard] CREATED'

---------------------------------------------------------------------------------------------------------

---------------------------------------------BankAdjustments--------------------------------------------------------
CREATE TABLE [EDOC].[LoyaltyBankAdjustments](
	[BankAdjustmentsID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[SchemeID] INT NOT NULL,
	[ProcDate] DATETIME NOT NULL,
	[PAN] [nvarchar](20) NOT NULL,
	[BankAdjustmentRecordNumber] INT NOT NULL,
	[PAN2] [nvarchar](20) NOT NULL,
	[PointsEarned] [nvarchar](10) NOT NULL,
	[CorporateNumber] [nvarchar](10) NOT NULL,
	[DisplayCorporateName] [nvarchar](50) NOT NULL,

CONSTRAINT [PK_BankAdjustments] PRIMARY KEY CLUSTERED 
(
	[BankAdjustmentsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

ALTER TABLE [EDOC].[LoyaltyBankAdjustments]  WITH CHECK ADD CONSTRAINT [FK_BankAdjustments_PAN] FOREIGN KEY([PAN])
REFERENCES [EDOC].[LoyaltyCard] ([PAN])

ALTER TABLE [EDOC].[LoyaltyBankAdjustments]  WITH CHECK ADD CONSTRAINT [FK_BankAdjustments_CorporateNumber] FOREIGN KEY([CorporateNumber])
REFERENCES [EDOC].[LoyaltyCorporateNames] ([CorporateCode])

ALTER TABLE [EDOC].[LoyaltyBankAdjustments]  WITH CHECK ADD CONSTRAINT [FK_BankAdjustments_Scheme] FOREIGN KEY([SchemeID])
REFERENCES [EDOC].[LoyaltySchemes] ([SchemeID])

PRINT 'TABLE [LoyaltyBankAdjustments] CREATED'
---------------------------------------------------------------------------------------------------------


---------------------------------------------CorporateData--------------------------------------------------------
CREATE TABLE [EDOC].[LoyaltyCorporateData](
	[CorporateDataID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[SchemeID] INT NOT NULL,
	[ProcDate] DATETIME NOT NULL,
	[PAN] [nvarchar](20) NOT NULL,
	[CorporateRecords] INT NOT NULL,
	[CorporateCode] [nvarchar](10) NOT NULL,
	[DisplayCorporateName] [nvarchar](50) NOT NULL,
	[PointsEarned] [nvarchar](10) NOT NULL,

CONSTRAINT [PK_CorporateData] PRIMARY KEY CLUSTERED 
(
	[CorporateDataID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

ALTER TABLE [EDOC].[LoyaltyCorporateData]  WITH CHECK ADD CONSTRAINT [FK_CorporateData_PAN] FOREIGN KEY([PAN])
REFERENCES [EDOC].[LoyaltyCard] ([PAN])

ALTER TABLE [EDOC].[LoyaltyCorporateData]  WITH CHECK ADD CONSTRAINT [FK_CorporateData_CorporateNumber] FOREIGN KEY([CorporateCode])
REFERENCES [EDOC].[LoyaltyCorporateNames] ([CorporateCode])

ALTER TABLE [EDOC].[LoyaltyCorporateData]  WITH CHECK ADD CONSTRAINT [FK_CorporateData_Scheme] FOREIGN KEY([SchemeID])
REFERENCES [EDOC].[LoyaltySchemes] ([SchemeID])

PRINT 'TABLE [LoyaltyCorporateData] CREATED'
---------------------------------------------------------------------------------------------------------



---------------------------------------------TotalPerCard--------------------------------------------------------
CREATE TABLE [EDOC].[LoyaltyTotalPerCard](
	[TotalPerCardID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[SchemeID] INT NOT NULL,
	[ProcDate] DATETIME NOT NULL,
	[PAN] [nvarchar](20) NOT NULL,
	[TotalPerCardNumber] INT NOT NULL,
	[PAN2] [nvarchar](20) NOT NULL,
	[UAID] [nvarchar](30) NOT NULL,
	[PointsEarned] [nvarchar](10) NOT NULL,
	[PointsRedeemed] [nvarchar](10) NOT NULL,
	[AccountActiveFlag] [nvarchar](1) NOT NULL,
	[BankAdjustmentsRecNumber] INT NOT NULL,

CONSTRAINT [PK_TotalPerCard] PRIMARY KEY CLUSTERED 
(
	[TotalPerCardID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

ALTER TABLE [EDOC].[LoyaltyTotalPerCard]  WITH CHECK ADD CONSTRAINT [FK_TotalPerCard_PAN] FOREIGN KEY([PAN])
REFERENCES [EDOC].[LoyaltyCard] ([PAN])

ALTER TABLE [EDOC].[LoyaltyTotalPerCard]  WITH CHECK ADD CONSTRAINT [FK_TotalPerCard_Scheme] FOREIGN KEY([SchemeID])
REFERENCES [EDOC].[LoyaltySchemes] ([SchemeID])

PRINT 'TABLE [LoyaltyTotalPerCard] CREATED'
---------------------------------------------------------------------------------------------------------

---------------------------------------------LoyaltyHeader--------------------------------------------------------
CREATE TABLE [EDOC].[LoyaltyHeader](
	[LoyaltyHeaderID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[SchemeID] INT NOT NULL,
	[HeaderDate] [DATETIME] NOT NULL,
	[FileName] [nvarchar](120) NOT NULL,

CONSTRAINT [PK_LoyaltyHeader] PRIMARY KEY CLUSTERED 
(
	[LoyaltyHeaderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

ALTER TABLE [EDOC].[LoyaltyHeader]  WITH CHECK ADD CONSTRAINT [FK_LoyaltyHeader_Scheme] FOREIGN KEY([SchemeID])
REFERENCES [EDOC].[LoyaltySchemes] ([SchemeID])

PRINT 'TABLE [LoyaltyHeader] CREATED'
---------------------------------------------------------------------------------------------------------
---------------------------------------------ExtraPoints--------------------------------------------------------
CREATE TABLE [EDOC].[LoyaltyExtraPoints](
	[ExtraPointsID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[SchemeID] INT NOT NULL,
	[ProcDate] DATETIME NOT NULL,
	[PAN] [nvarchar](20) NOT NULL,
	[ExtraPointsRecordNumber] INT NOT NULL,
	[CampaignName] [nvarchar](40) NOT NULL,
	[ExtraPoints] [nvarchar](10) NOT NULL,

CONSTRAINT [PK_ExtraPoints] PRIMARY KEY CLUSTERED 
(
	[ExtraPointsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

ALTER TABLE [EDOC].[LoyaltyExtraPoints]  WITH CHECK ADD CONSTRAINT [FK_ExtraPoints_PAN] FOREIGN KEY([PAN])
REFERENCES [EDOC].[LoyaltyCard] ([PAN])

ALTER TABLE [EDOC].[LoyaltyExtraPoints]  WITH CHECK ADD CONSTRAINT [FK_ExtraPoints_Scheme] FOREIGN KEY([SchemeID])
REFERENCES [EDOC].[LoyaltySchemes] ([SchemeID])

PRINT 'TABLE [[LoyaltyExtraPoints]] CREATED'
---------------------------------------------------------------------------------------------------------

---------------------------------------------ExcludedCards--------------------------------------------------------
CREATE TABLE [EDOC].[LoyaltyExcludedCards](
	[ExcludedCardsID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ExclCard] [nvarchar](20) NOT NULL,

CONSTRAINT [PK_Dates] PRIMARY KEY CLUSTERED 
(
	[ExcludedCardsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [LoyaltyExcludedCards] CREATED'
---------------------------------------------------------------------------------------------------------
---------------------------------------------Dates--------------------------------------------------------
CREATE TABLE [EDOC].[LoyaltyDates](
	[LoyaltyDatesID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[SchemeID] INT NOT NULL,
	[ProcDate] DATETIME NOT NULL,
	[DateEpistoli] DATETIME NOT NULL,
	[DatePtsBegin] DATETIME NOT NULL,
	[DatePtsEnd] DATETIME NOT NULL,
	[DatePtsExpired] DATETIME NOT NULL,
	[DatePtsToExpire] DATETIME NOT NULL,

CONSTRAINT [PK_LoyaltyDates] PRIMARY KEY CLUSTERED 
(
	[LoyaltyDatesID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

ALTER TABLE [EDOC].[LoyaltyDates]  WITH CHECK ADD CONSTRAINT [FK_LoyaltyDates_Scheme] FOREIGN KEY([SchemeID])
REFERENCES [EDOC].[LoyaltySchemes] ([SchemeID])

PRINT 'TABLE [LoyaltyDates] CREATED'
---------------------------------------------------------------------------------------------------------
---------------------------------------------CurrentDay--------------------------------------------------------
CREATE TABLE [EDOC].[LoyaltyCurrentDay](
	[CurrentDayID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[SchemeID] INT NOT NULL,
	[ProcDate] DATETIME NOT NULL,
	[Single] [nvarchar](10) NOT NULL,


CONSTRAINT [PK_CurrentDay] PRIMARY KEY CLUSTERED 
(
	[CurrentDayID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

ALTER TABLE [EDOC].[LoyaltyCurrentDay]  WITH CHECK ADD CONSTRAINT [FK_CurrentDay_Scheme] FOREIGN KEY([SchemeID])
REFERENCES [EDOC].[LoyaltySchemes] ([SchemeID])

PRINT 'TABLE [LoyaltyCurrentDay] CREATED'
---------------------------------------------------------------------------------------------------------

