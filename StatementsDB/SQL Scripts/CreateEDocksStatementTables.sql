
-------------------------DROP ConditionCriteria-------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'StmtConditionCriteria')
BEGIN
	DROP TABLE [EDOC].[StmtConditionCriteria]
	PRINT 'ConditionCriteria DELETED'
END
-----------------------------------------------------------

-------------------------DROP ConditionCriteriaValidations-------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'StmtConditionCriteriaValidations')
BEGIN
	DROP TABLE [EDOC].[StmtConditionCriteriaValidations]
	PRINT 'ConditionCriteriaValidations DELETED'
END
-----------------------------------------------------------


-------------------------DROP Conditions-------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'StmtConditions')
BEGIN
	DROP TABLE [EDOC].[StmtConditions]
	PRINT 'Conditions DELETED'
END
-----------------------------------------------------------

-------------------------DROP BonusImages-------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'StmtBonusImages')
BEGIN
	DROP TABLE [EDOC].[StmtBonusImages]
	PRINT 'BonusImages DELETED'
END
-----------------------------------------------------------

-------------------------DROP Backgrounds------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'StmtBackgrounds')
BEGIN
	DROP TABLE [EDOC].[StmtBackgrounds]
	PRINT 'Backgrounds DELETED'
END
-----------------------------------------------------------

-------------------------DROP JobLogs-------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'JobLogs')
BEGIN
	DROP TABLE EDOC.JobLogs
	PRINT 'JobLogs DELETED'
END
-----------------------------------------------------------

-------------------------DROP MarketingImages-------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'StmtMarketingImages')
BEGIN
	DROP TABLE [EDOC].[StmtMarketingImages]
	PRINT 'MarketingImages DELETED'
END
-----------------------------------------------------------

-------------------------DROP Jobs-------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'Jobs')
BEGIN
	DROP TABLE EDOC.Jobs
	PRINT 'Jobs DELETED'
END
-----------------------------------------------------------

-------------------------DROP Programms-------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'Programms')
BEGIN
	DROP TABLE EDOC.Programms
	PRINT 'Programms DELETED'
END
-----------------------------------------------------------

-------------------------DROP Clients-------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'Clients')
BEGIN
	DROP TABLE EDOC.Clients
	PRINT 'Clients DELETED'
END
-----------------------------------------------------------

-------------------------DROP ExtraPages-------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'StmtExtraPages')
BEGIN
	DROP TABLE [EDOC].[StmtExtraPages]
	PRINT 'ExtraPages DELETED'
END
-----------------------------------------------------------

-------------------------DROP Criteria-------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'EDOC' AND TABLE_NAME = 'StmtCriteria')
BEGIN
	DROP TABLE [EDOC].[StmtCriteria]
	PRINT 'Criteria DELETED'
END
-----------------------------------------------------------

PRINT ''
PRINT '-----------------------------------------'
PRINT ''

---------------------------------------------Clients--------------------------------------------------------
CREATE TABLE [EDOC].[Clients](
	[ClientID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ClientName] [nvarchar](120) NOT NULL,
	[ClientDescription] [nvarchar](MAX) NULL,
CONSTRAINT [PK_Clients] PRIMARY KEY CLUSTERED 
(
	[ClientID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [Clients] CREATED'
---------------------------------------------------------------------------------------------------------

--------------------------------------------Programms-----------------------------------------------------
CREATE TABLE [EDOC].[Programms](
	[ProgrammID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ProgrammName] [nvarchar](120) NOT NULL,
	[ProgrammDescription] [nvarchar](MAX) NULL,
	[ClientID] INT NOT NULL,
CONSTRAINT [PK_Programms] PRIMARY KEY CLUSTERED 
(
	[ProgrammID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

ALTER TABLE [EDOC].[Programms]  WITH CHECK ADD CONSTRAINT [FK_Programms_Owner] FOREIGN KEY([ClientID])
REFERENCES [EDOC].[Clients] ([ClientID])

ALTER TABLE [EDOC].[Programms] CHECK CONSTRAINT [FK_Programms_Owner]

PRINT 'TABLE [Programms] CREATED'
---------------------------------------------------------------------------------------------------------

--------------------------------------------Jobs-----------------------------------------------------
CREATE TABLE [EDOC].[Jobs](
	[JobID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[JobName] [nvarchar](120) NOT NULL,
	[ProgrammID] INT NOT NULL,
CONSTRAINT [PK_Jobs] PRIMARY KEY CLUSTERED 
(
	[JobID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

ALTER TABLE [EDOC].[Jobs]  WITH CHECK ADD CONSTRAINT [FK_Job_Programms] FOREIGN KEY([ProgrammID])
REFERENCES [EDOC].[Programms] ([ProgrammID])

ALTER TABLE [EDOC].[Jobs] CHECK CONSTRAINT [FK_Job_Programms]

PRINT 'TABLE [Jobs] CREATED'
---------------------------------------------------------------------------------------------------------

--------------------------------------------JobLogs-----------------------------------------------------
CREATE TABLE [EDOC].[JobLogs](
	[LogID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[LogDate] DATETIME NOT NULL,
	[LogDescription] [nvarchar](MAX) NULL,
	[JobID] INT NOT NULL,
CONSTRAINT [PK_JobLogs] PRIMARY KEY CLUSTERED 
(
	[LogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

ALTER TABLE [EDOC].[JobLogs] WITH CHECK ADD CONSTRAINT [FK_Job_Logs] FOREIGN KEY([JobID])
REFERENCES [EDOC].[Jobs] ([JobID])

ALTER TABLE [EDOC].[JobLogs] CHECK CONSTRAINT [FK_Job_Logs]

PRINT 'TABLE [JobLogs] CREATED'
---------------------------------------------------------------------------------------------------------


--------------------------------------------Backgrounds-----------------------------------------------------
CREATE TABLE [EDOC].[StmtBackgrounds](
	[FrameID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[DateFrom] DATETIME NOT NULL,
	[Position] INT NOT NULL,
	[ImageName] [nvarchar](120) NOT NULL,
	[Product] [nvarchar](max) NOT NULL,
	[JobID] INT NOT NULL,
CONSTRAINT [PK_Backgrounds] PRIMARY KEY CLUSTERED 
(
	[FrameID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

ALTER TABLE [EDOC].[StmtBackgrounds] WITH CHECK ADD CONSTRAINT [FK_Job_Backgrounds] FOREIGN KEY([JobID])
REFERENCES [EDOC].[Jobs] ([JobID])

ALTER TABLE [EDOC].[StmtBackgrounds] CHECK CONSTRAINT [FK_Job_Backgrounds]

PRINT 'TABLE [StmtBackgrounds] CREATED'
---------------------------------------------------------------------------------------------------------

--------------------------------------------BonusImages-----------------------------------------------------
CREATE TABLE [EDOC].[StmtBonusImages](
	[BonusImageID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[BonusImageName] [nvarchar](120) NOT NULL,
	[BonusImageType] [nvarchar](50) NULL,
	[JobID] INT NOT NULL,
CONSTRAINT [PK_BonusImages] PRIMARY KEY CLUSTERED 
(
	[BonusImageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

ALTER TABLE [EDOC].[StmtBonusImages] WITH CHECK ADD CONSTRAINT [FK_Job_BonusImages] FOREIGN KEY([JobID])
REFERENCES [EDOC].[Jobs] ([JobID])

ALTER TABLE [EDOC].[StmtBonusImages] CHECK CONSTRAINT [FK_Job_BonusImages]

PRINT 'TABLE [StmtBonusImages] CREATED'
---------------------------------------------------------------------------------------------------------

--------------------------------------------MarketingImages-----------------------------------------------------
CREATE TABLE [EDOC].[StmtMarketingImages](
	[MarketingImageID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[MarketingImageName] [nvarchar](120) NULL,
	[MessageText] [nvarchar](max) NULL,
	[OutputInformationType] NVARCHAR(1) NOT NULL, --Image OR Message (I or M)
	[JobID] INT NOT NULL,
CONSTRAINT [PK_MarketingImages] PRIMARY KEY CLUSTERED 
(
	[MarketingImageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

ALTER TABLE [EDOC].[StmtMarketingImages] WITH CHECK ADD CONSTRAINT [FK_Job_MarketingImages] FOREIGN KEY([JobID])
REFERENCES [EDOC].[Jobs] ([JobID])

ALTER TABLE [EDOC].[StmtMarketingImages] CHECK CONSTRAINT [FK_Job_MarketingImages]

PRINT 'TABLE [StmtConditions] CREATED'
---------------------------------------------------------------------------------------------------------

--------------------------------------------Conditions-----------------------------------------------------
CREATE TABLE [EDOC].[StmtConditions](
	[ConditionID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[DateFrom] DATETIME NOT NULL,
	[Priority] INT NOT NULL,
	[Position] INT NOT NULL,
	[MarketingImageID] INT NOT NULL,
	[OutputInformationType] NVARCHAR(1) NOT NULL, --Image OR Message (I or M)
	[JobID] INT NOT NULL,
CONSTRAINT [PK_Conditions] PRIMARY KEY CLUSTERED 
(
	[ConditionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

ALTER TABLE [EDOC].[StmtConditions] WITH CHECK ADD CONSTRAINT [FK_Job_Condition] FOREIGN KEY([JobID])
REFERENCES [EDOC].[Jobs] ([JobID])

ALTER TABLE [EDOC].[StmtConditions] CHECK CONSTRAINT [FK_Job_Condition]

ALTER TABLE [EDOC].[StmtConditions] WITH CHECK ADD CONSTRAINT [FK_Condition_Marketing_Images] FOREIGN KEY([MarketingImageID])
REFERENCES [EDOC].[StmtMarketingImages] ([MarketingImageID])

ALTER TABLE [EDOC].[StmtConditions] CHECK CONSTRAINT [FK_Condition_Marketing_Images]

PRINT 'TABLE [StmtConditions] CREATED'
---------------------------------------------------------------------------------------------------------

--------------------------------------------ConditionCriteriaValidations-----------------------------------------------------
CREATE TABLE [EDOC].[StmtConditionCriteriaValidations](
	[ConditionCriteriaValidationID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[From] NVARCHAR(120) NOT NULL,
	[To] NVARCHAR(120) NOT NULL,
	[InternalExternal] NVARCHAR(1) NOT NULL, --I or E
CONSTRAINT [PK_ConditionCriteriaValidations] PRIMARY KEY CLUSTERED 
(
	[ConditionCriteriaValidationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [StmtConditionCriteriaValidations] CREATED'
---------------------------------------------------------------------------------------------------------


--------------------------------------------Criteria-----------------------------------------------------
CREATE TABLE [EDOC].[StmtCriteria](
	[CriteriaID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[CriteriaName] nvarchar(120) NOT NULL,
	[CriteriaSize] INT NOT NULL,
	[CriteriaType] NVARCHAR(1) NULL, -- L = List, N= Numeric(1,0)
CONSTRAINT [PK_Criteria] PRIMARY KEY CLUSTERED 
(
	[CriteriaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

PRINT 'TABLE [StmtCriteria] CREATED'
---------------------------------------------------------------------------------------------------------

--------------------------------------------ExtraPages-----------------------------------------------------
CREATE TABLE [EDOC].[StmtExtraPages](
	[ExtraListID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[CriteriaID] INT NOT NULL,
	[ExtraCriteria] NVARCHAR(120) NOT NULL,
	[DateUploaded] DATETIME NULL,
	[FileNameUploaded] NVARCHAR(120) NULL,
CONSTRAINT [PK_ExtraPages] PRIMARY KEY CLUSTERED 
(
	[ExtraListID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

ALTER TABLE [EDOC].[StmtExtraPages] WITH CHECK ADD CONSTRAINT [FK_CriteriaID_ExtraList] FOREIGN KEY([CriteriaID])
REFERENCES [EDOC].[StmtCriteria] ([CriteriaID])

ALTER TABLE [EDOC].[StmtExtraPages] CHECK CONSTRAINT [FK_CriteriaID_ExtraList]

PRINT 'TABLE [StmtExtraPages] CREATED'
---------------------------------------------------------------------------------------------------------


--------------------------------------------ConditionCriteria-----------------------------------------------------
CREATE TABLE [EDOC].[StmtConditionCriteria](
	[ConditionCriteriaID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ConditionID] INT NOT NULL,
	[ConditionCriteriaValidationID] INT NOT NULL,
	[CriteriaID] INT NOT NULL, 
CONSTRAINT [PK_ConditionCriteria] PRIMARY KEY CLUSTERED 
(
	[ConditionCriteriaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
) ON [DATA]

ALTER TABLE [EDOC].[StmtConditionCriteria] WITH CHECK ADD CONSTRAINT [FK_ConditionCriteria_Condition] FOREIGN KEY([ConditionID])
REFERENCES [EDOC].[StmtConditions] ([ConditionID])

ALTER TABLE [EDOC].[StmtConditionCriteria] CHECK CONSTRAINT [FK_ConditionCriteria_Condition]

ALTER TABLE [EDOC].[StmtConditionCriteria] WITH CHECK ADD CONSTRAINT [FK_ConditionCriteriaValidations_Condition] FOREIGN KEY([ConditionCriteriaValidationID])
REFERENCES [EDOC].[StmtConditionCriteriaValidations] ([ConditionCriteriaValidationID])

ALTER TABLE [EDOC].[StmtConditionCriteria] CHECK CONSTRAINT [FK_ConditionCriteriaValidations_Condition]

ALTER TABLE [EDOC].[StmtConditionCriteria] WITH CHECK ADD CONSTRAINT [FK_ConditionCriteriaValidations_Criteria] FOREIGN KEY([CriteriaID])
REFERENCES [EDOC].[StmtCriteria] ([CriteriaID])

ALTER TABLE [EDOC].[StmtConditionCriteria] CHECK CONSTRAINT [FK_ConditionCriteriaValidations_Criteria]

PRINT 'TABLE [StmtConditionCriteria] CREATED'
---------------------------------------------------------------------------------------------------------

